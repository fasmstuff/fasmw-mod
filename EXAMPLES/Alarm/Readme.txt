Alarm clock / Reminder by ATV - askovuori(a)hotmail.com
Open Source

Alarm stays on system tray and shows popup message
on user specify time/date with or without sound.

Notes:
  - Alarm.ini is loaded when program is started and saved after every change
    In [Alarms] section line format is:
      Alarm001=hh:mm dd.mm.yyyy msg
      Sound001=c:\fasmw\alarm\alarm.mp3
    char between time and date is repeat interval
        space = none
        d = daily
        w = weekly
        m = monthly
        y = yearly
        D = weekdays
        E = weekends
        t = 2 weeks
        f = 4 weeks
        s = 6 weeks
        e = 8 weeks
   - Sound path and filename can be over 80 chars,
     if you edit ini file don't break lines
   - If Sound data first char is ascii number it means that it is
     sound type/count info
     ex. "Sound001=02" is alarm1 has type 0=Alarm.wav repeat count 2
         "Sound002=13" is alarm2 has type 1=Beep repeat count 3
         "Sound004=26" is alarm4 has type 2=siren repeat count 6
  - on exit show warning if there is alarms left
  - maximum 100 different alarms in chronological order
  - maximum length of user message is 79 character
  - repeat alarm sound in 1 min intervals in next 10 minutes
  - you can modify your countrys bank holidays in setup window
    format: dd.mm dd.mm dd.mm
    It's saved in alarm.ini [Init] section
      Holidays=1.1 24.12 25.12 26.12
    used only by weekdays/weekends repeat intervals
  - to save used alarms in Alarm.log use checkbox in setup window.
  - to load at startup, use checkbox in setup window.
    remember uncheck it before you delete program
    used key: HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\Alarm
