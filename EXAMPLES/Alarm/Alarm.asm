; Alarm clock / Reminder by ATV - askovuori(a)hotmail.com
; Open Source
;
; Alarm stays on system tray and shows popup message
; on user specify time/date with or without sound.
;
; Compile with FASMW
; http://flatassembler.net
;
; Notes:
;   - Alarm.ini is loaded when program is started and saved after every change
;     In [Alarms] section line format is:
;       Alarm001=hh:mm dd.mm.yyyy msg
;       Sound001=c:\fasmw\alarm\alarm.mp3
;     char between time and date is repeat interval
;     (space = none,
;      d = daily, w = weekly, m = monthly, y = yearly,
;      D = weekdays, E = weekends,
;      t = 2 weeks, f = 4 weeks, s = 6 weeks, e = 8 weeks)
;   - Sound path and filename can be over 80 chars,
;     if you edit ini file don't break lines
;   - If Sound data first char is ascii number it means that it is
;     sound type/count info
;     ex. "Sound001=02" is alarm1 has type 0=Alarm.wav repeat count 2
;         "Sound002=13" is alarm2 has type 1=Beep repeat count 3
;         "Sound004=26" is alarm4 has type 2=siren repeat count 6
;   - on exit show warning if there is alarms left
;   - maximum 100 different alarms in chronological order
;   - maximum length of user message is 79 character
;   - repeat alarm sound in 1 min intervals in next 10 minutes
;   - you can modify your countrys bank holidays in setup window
;     format: dd.mm dd.mm dd.mm
;     used only by weekdays/weekends repeat intervals
;     It's saved in alarm.ini [Init] section
;       Holidays=1.1 24.12 25.12 26.12
;   - to save used alarms in Alarm.log use checkbox in setup window.
;   - to load at startup, use checkbox in setup window.
;     remember uncheck it before you delete program
;     used key: HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\Alarm
;
; ToDo:
;   - listbox click/double-click select, second click unselect
;   - more API error handlers
;
; Version history:
;   0.01 - 11.apr.2006 by ATV - askovuori(a)hotmail.com
;   0.02 - 17.apr.2006 by ATV - ordered alarm list, ini handling rewritten,
;                               choice sound type, date picker
;   0.03 - 28.jul.2006 by ATV - save to LogFile,counter for repeat sound,
;                               added daily/weekly/monthly/yearly/weekdays repeat
;   0.04 - 28.aug.2006 by ATV - added settopindex,added 2/4/6/8 weeks repeat,
;                               added edit_dialog,next alarm in tray tooltip
;   0.05 - 03.oct.2006 by ATV - added notes dialog,added MP3
;   0.06 - 06.oct.2006 by ATV - every alarm with own sound effect
;   0.07 - 11.oct.2006 by ATV - edit alarm change sound file bug fixed,
;                               alarms direct ini write (faster),
;                               exit program only on systray menu
;   0.08 - 15.jan.2007 by ATV - added holidays list,setup dialog,window sizing
;   0.09 - 05.feb.2007 by ATV - save window pos/size,some tooltips,listbox colors
;   0.10 - 09.may.2007 by ATV - added handler for TaskbarCreated,sound test
;   0.11 - 26.jul.2007 by ATV - added write2log,MB_TOPMOST,choose colors,about,
;                               day of week,tabstops in ownerdrawn listbox
;   0.12 - 25.aug.2007 by ATV - FindWindow,show same day alarms in edit dialog,
;                               confirm when delete alarm,few bugs fixed

format PE GUI 4.0
entry start

include 'win32a.inc'

; constants
DLGCAPTION      equ 'Alarm / Reminder'
STR_VERSION     equ '0.12'

WM_SHELLNOTIFY  = WM_USER+5
IDI_TRAY        = 100
IDT_TIMER1      = 200                   ; main timer
IDT_TIMER2      = 201                   ; timer to hide window
; tray menu items
IDM_SHOWHIDE    = 301                   ; show/hide window
IDM_SETUP       = 302                   ; setup dialog
IDM_ABOUT       = 303                   ; about dialog
IDM_EXIT        = 399                   ; exit program
; main dialog items
IDD_MAIN        = 400                   ; main dialog
ID_CURRENT_TIME = 401                   ; current time and date
ID_ALARM_LIST   = 402                   ; alarm listbox
ID_COUNT        = 403                   ; total alarm count
IDB_ADD         = 411                   ; add/edit button
IDB_NOTES       = 413                   ; notes button
IDB_WRITE2LOG   = 414
; edit dialog items
IDD_EDIT        = 500                   ; edit dialog
ID_NEW_ALARM    = 501                   ; time editbox
ID_DATE         = 502                   ; DateTimePicker
ID_MESSAGE      = 503                   ; message editbox
ID_SND          = 504                   ; sound type (wav,beep,siren) combobox
ID_SNDCOUNT     = 505                   ; times to repeat sound editbox
ID_REPEAT       = 506                   ; daily..yearly repeat combobox
ID_FILENAME     = 508                   ; MP3 filename / editbox
ID_DAY_LIST     = 509                   ; day alarm listbox
IDB_DELETE      = 511                   ; delete button
IDB_OPENF       = 513                   ; open button
IDB_TESTSND     = 514                   ; test sound button
; notes dialog items
IDD_NOTES       = 600                   ; notes dialog
IDE_NOTES       = 601                   ; editbox
; write2log dialog items
IDD_WRITE2LOG   = 650                   ; write2log dialog
; about dialog items
IDD_ABOUT       = 660                   ; about dialog
; setup dialog items
IDD_SETUP       = 700                   ; setup dialog
IDB_LOGFILE     = 701                   ; use log file checkbox
IDB_STARTUP     = 702                   ; load at startup checkbox
IDE_HOLIDAYS    = 703                   ; bank holidays editbox
ID_COLORS       = 710                   ; colors listbox
IDB_TODAY_TEXT    = 711                 ; note: color pos = id - IDB_TODAY_TEXT
IDB_TOMORROW_TEXT = 712
IDB_FUTURE_TEXT   = 713
IDB_ACTIVE_TEXT   = 714
IDB_TODAY_BACK    = 715
IDB_TOMORROW_BACK = 716
IDB_FUTURE_BACK   = 717
IDB_ACTIVE_BACK   = 718
IDB_FONT          = 730

IDI_MAIN        = 801                   ; main icon

LIST_WIDTH      = 180                   ; resize: listbox
LIST_HEIGHT     = 80                    ; move: buttons,statics

ALARM_MSG_SIZE  = 80
MAX_ALARM       = 100                   ; maximum 3 digit = 999
MAX_HOLIDAY     = 100
NOTES_SIZE      = 8192
FNAME_SIZE      = 260
BUFFER_SIZE     = NOTES_SIZE+2048
TEST_PER_MIN    = 4                     ; timer update interval
WAV_IN_MEMORY   = 0                     ; 0=wav in file,1=wav in memory

SND_SYNC        = 00000000h
SND_ASYNC       = 00000001h
SND_NODEFAULT   = 00000002h
SND_MEMORY      = 00000004h
SND_ALIAS       = 00010000h
SND_FILENAME    = 00020000h
SND_RESOURCE    = 00040004h
SND_ALIAS_ID    = 00110000h
SND_ALIAS_START = 00000000h
SND_LOOP        = 00000008h
SND_NOSTOP      = 00000010h
SND_VALID       = 0000001Fh
SND_NOWAIT      = 00002000h
SND_VALIDFLAGS  = 0017201Fh
SND_RESERVED    = 0FF000000h
SND_TYPE_MASK   = 00170007h

TTS_ALWAYSTIP   = 01h
TTS_NOPREFIX    = 02h
TTS_NOANIMATE   = 10h
TTS_NOFADE      = 20h
TTS_BALLOON     = 40h
TTS_CLOSE       = 80h

LOCALE_USER_DEFAULT = 0400h

struct ALARM_TYPE
  systime       SYSTEMTIME
  flag          rb 1            ; space=none, d=daily, w=weekly, y=yearly
  msg           rb ALARM_MSG_SIZE
  sound_type    rb 1
  sound_count   rb 1
  sound_file    rb FNAME_SIZE
ends

struct HOLIDAY_TYPE
  day           rb 1
  month         rb 1
ends

section '.code' code readable executable

start:
        mov     [repeat_alarm],0
        mov     [alarm_count],0
        mov     [notes_buffer],0

        invoke  FindWindow,0,szTitle
        or      eax,eax
        jnz     .activate_window

        invoke  RegisterWindowMessageA,szTaskBarCreated
        mov     [taskbarcreated],eax
        invoke  GetModuleHandle,0
        or      eax,eax
        jz      .error
        mov     [hInstance],eax

        invoke  LoadIcon,[hInstance],IDI_MAIN
        or      eax,eax
        jz      .error
        mov     [hIcon],eax

        ; prepare common control structure
        mov     [icex.dwSize],sizeof.INITCOMMONCONTROLSEX
        mov     [icex.dwICC],ICC_DATE_CLASSES
        ; initialize common controls for DateTimePicker
        invoke  InitCommonControlsEx,icex

        invoke  DialogBoxParam,[hInstance],IDD_MAIN,HWND_DESKTOP,DialogProc,0
  .exit:
        invoke  ExitProcess,0

  .activate_window:
        invoke  PostMessage,eax,WM_SHELLNOTIFY,IDI_TRAY,WM_LBUTTONDBLCLK
        jmp     .exit

  .error:
        invoke  MessageBox,NULL,szCreateError,NULL,MB_ICONERROR or MB_OK
        jmp     .exit

proc DialogProc hWnd,wMsg,wParam,lParam
local szListBuffer[256]:BYTE
local pt:POINT
local Tm:TEXTMETRIC
        push    ebx esi edi
        mov     eax,[wMsg]
        cmp     eax,WM_TIMER
        je      .wmTimer
        cmp     eax,WM_COMMAND
        je      .wmcommand
        cmp     eax,WM_CLOSE
        je      .showhide
        cmp     eax,WM_DESTROY
        je      .wmdestroy
        cmp     eax,WM_SHELLNOTIFY
        je      .wmshellnotify
        cmp     eax,WM_ENDSESSION
        je      .wmendsession
        cmp     eax,WM_GETMINMAXINFO
        je      .wmgetminmaxinfo
        cmp     eax,WM_SIZE
        je      .wmsize
        cmp     eax,WM_MOVE
        je      .wmmove
        cmp     eax,WM_DRAWITEM
        je      .wmdrawitem
        cmp     eax,WM_CTLCOLORSTATIC
        je      .wmctlcolorstatic
        cmp     eax,WM_INITDIALOG
        je      .initdlg
        cmp     eax,[taskbarcreated]
        je      .wmtaskbarcreated
        xor     eax,eax
        jmp     .finish

  .initdlg:
        mov     [showflag],1
        invoke  SendMessage,[hWnd],WM_SETICON,ICON_BIG,[hIcon]

        ; create tray icon, fill NOTIFYICONDATA structure
        mov     [node.cbSize],sizeof.NOTIFYICONDATA
        mov     eax,[hWnd]
        mov     [node.hWnd],eax
        mov     [node.uID],IDI_TRAY
        mov     [node.uFlags],NIF_ICON or NIF_MESSAGE or NIF_TIP
        mov     [node.uCallbackMessage],WM_SHELLNOTIFY
        mov     eax,[hIcon]
        mov     [node.hIcon],eax
        mov     dword [node.szTip],'Alar'
        mov     dword [node.szTip+4],'ms: '
        mov     word [node.szTip+8],'0'
        invoke  Shell_NotifyIcon,NIM_ADD,node
        invoke  CreatePopupMenu
        mov     [hTrayMenu],eax
        or      eax,eax
        jz      .tray_error
        invoke  AppendMenu,[hTrayMenu],MF_STRING,IDM_ABOUT,szAbout
        invoke  AppendMenu,[hTrayMenu],MF_STRING,IDM_SETUP,szSetup
        invoke  AppendMenu,[hTrayMenu],MF_SEPARATOR,0,0
        invoke  AppendMenu,[hTrayMenu],MF_STRING,IDM_SHOWHIDE,szShowHide
        invoke  AppendMenu,[hTrayMenu],MF_STRING,IDM_EXIT,szExit
      .tray_error:

        invoke  GetDlgItem,[hWnd],ID_ALARM_LIST
        mov     [hList],eax
        invoke  GetDlgItem,[hWnd],ID_COUNT
        mov     [hCount],eax
        invoke  GetDlgItem,[hWnd],ID_CURRENT_TIME
        mov     [hCurrentTime],eax

        call    get_weekday_names
        call    get_ini_fname
        stdcall load_alarm_list,[hWnd]

        mov     esi,[saved_rect.top]
        mov     edi,[saved_rect.left]
        mov     eax,[saved_rect.bottom]
        mov     ebx,[saved_rect.right]
        sub     eax,esi         ; height=bottom-top
        sub     ebx,edi         ; width=right-left
        invoke  SetWindowPos,[hWnd],HWND_TOP,edi,esi,ebx,eax,0

        invoke  CreateSolidBrush,[0*4+back_colors]
        mov     [hTodayBrush],eax

        stdcall show_current_time_date,[hWnd]
        invoke  SetTimer,[hWnd],IDT_TIMER2,600,NULL
        jmp     .processed

  .wmgetminmaxinfo:
        mov     ebx,[lParam]
        mov     [ebx+MINMAXINFO.ptMinTrackSize.x],290
        mov     [ebx+MINMAXINFO.ptMinTrackSize.y],190
        jmp     .processed

  .wmsize:
        invoke  GetClientRect,[hWnd],rect
        invoke  GetDlgItem,[hWnd],IDB_NOTES
        mov     ecx,[rect.right]
        mov     edx,[rect.top]
        sub     ecx,80
        add     edx,20
        invoke  MoveWindow,eax,ecx,edx,60,26,TRUE
        mov     ecx,[rect.right]
        mov     edx,[rect.top]
        sub     ecx,100
        add     edx,52
        invoke  MoveWindow,[hCount],ecx,edx,80,20,TRUE
        mov     ecx,[rect.right]
        mov     edx,[rect.bottom]
        sub     ecx,40
        sub     edx,130
        invoke  MoveWindow,[hList],20,80,ecx,edx,TRUE
        invoke  GetDlgItem,[hWnd],IDB_ADD
        mov     ecx,[rect.right]
        mov     edx,[rect.bottom]
        sub     ecx,100
        sub     edx,40
        invoke  MoveWindow,eax,ecx,edx,80,26,TRUE
        invoke  GetDlgItem,[hWnd],IDB_WRITE2LOG
        mov     edx,[rect.bottom]
        sub     edx,40
        invoke  MoveWindow,eax,20,edx,100,26,TRUE
  .wmmove:
        invoke  GetWindowRect,[hWnd],rect
        mov     esi,rect
        mov     edi,saved_rect
        mov     ecx,sizeof.RECT
        cld
        repe    cmpsb
        je      .not_moved
        call    write_window_pos
        mov     esi,rect
        mov     edi,saved_rect
        mov     ecx,sizeof.RECT
        rep     movsb
      .not_moved:
        jmp     .processed

  .wmctlcolorstatic:
        mov     eax,[lParam]
        cmp     eax,[hCurrentTime]
        jne     .processed
        invoke  SetTextColor,[wParam],[0*4+text_colors]
        invoke  SetBkColor,[wParam],[0*4+back_colors]
        mov     eax,[hTodayBrush]
        jmp     .finish

  .wmdrawitem:
        cmp     [wParam],ID_ALARM_LIST
        jne     .draw_default
        mov     esi,[lParam]
        test    [esi+DRAWITEMSTRUCT.itemAction],ODA_FOCUS
        jne     .has_focus
        cmp     [esi+DRAWITEMSTRUCT.itemID],-1
        je      .draw_default
        lea     eax,[Tm]
        invoke  GetTextMetrics,[esi+DRAWITEMSTRUCT.hDC],eax
        mov     ebx,3
        test    [esi+DRAWITEMSTRUCT.itemState],ODS_SELECTED
        jne     .is_selected
        mov     ebx,[esi+DRAWITEMSTRUCT.itemData]
      .is_selected:
        invoke  CreateSolidBrush,[ebx*4+back_colors]
        mov     edi,eax
        lea     edx,[esi+DRAWITEMSTRUCT.rcItem]
        invoke  FillRect,[esi+DRAWITEMSTRUCT.hDC],edx,edi
        invoke  DeleteObject,edi
        invoke  SetTextColor,[esi+DRAWITEMSTRUCT.hDC],[ebx*4+text_colors]
        invoke  SetBkColor,[esi+DRAWITEMSTRUCT.hDC],[ebx*4+back_colors]
        lea     ebx,[szListBuffer]
        invoke  SendMessage,[hList],LB_GETTEXT,[esi+DRAWITEMSTRUCT.itemID],ebx
        mov     edx,eax
        mov     ecx,[esi+DRAWITEMSTRUCT.rcItem.left]
        add     ecx,4
        mov     eax,[esi+DRAWITEMSTRUCT.rcItem.bottom]
        add     eax,[esi+DRAWITEMSTRUCT.rcItem.top]
        sub     eax,[Tm+TEXTMETRIC.tmHeight]
        shr     eax,1
        stdcall TabStop_TextOut,[esi+DRAWITEMSTRUCT.hDC],ecx,eax,ebx,edx,tab_count
        mov     eax,TRUE
        jmp     .finish
    .has_focus:
        lea     eax,[esi+DRAWITEMSTRUCT.rcItem]
        invoke  DrawFocusRect,[esi+DRAWITEMSTRUCT.hDC],eax
        mov     eax,TRUE
        jmp     .finish
    .draw_default:
        invoke  DefWindowProc,[hWnd],[wMsg],[wParam],[lParam]
        jmp     .finish

  .wmtaskbarcreated:
        ; When Explorer crashes and restarts.
        invoke  Shell_NotifyIcon,NIM_ADD,node   ; show icon to system tray
        jmp     .finish

  .wmendsession:
        cmp     [wParam],TRUE
        jne     .processed
        ;call    save_alarm_list
        jmp     .processed

  .wmTimer:
        cmp     [wParam],IDT_TIMER2     ; quick and dirty way to hide window at start
        je      .remove_timer2
        cmp     [wParam],IDT_TIMER1
        jne     .processed
        stdcall show_current_time_date,[hWnd]
        call    check_alarm
        dec     eax
        jz      .start_alarm            ; =1 start alarm
        dec     eax
        jnz     .processed              ; =2 repeat sound

    .repeat_alarm:
        mov     edi,repeat_alarm
        mov     ebx,alarm_table
        call    do_sound
        jmp     .processed

    .start_alarm:
        stdcall do_alarm,[hWnd],ecx
        jmp     .processed

  .wmshellnotify:
        ; WM_SHELLNOTIFY handler - we handle actions like clicking on our icon
        cmp     [wParam],IDI_TRAY
        jne     .processed
        cmp     [lParam],WM_LBUTTONDBLCLK
        je      .showhide
        cmp     [lParam],WM_RBUTTONUP
        je      .show_tray_popup
        jmp     .processed

    .remove_timer2:
        invoke  KillTimer,[hWnd],IDT_TIMER2
        invoke  SetTimer,[hWnd],IDT_TIMER1,60000/TEST_PER_MIN,NULL

    .showhide:
        test    [showflag],1
        je      .show
      .hide:
        invoke  ShowWindow,[hWnd],SW_HIDE
        and     byte [showflag],11111110b
        jmp     .processed
      .show:
        or      [showflag],1
        stdcall show_current_time_date,[hWnd]
        invoke  SendMessage,[hList],LB_SETCURSEL,0,0
        invoke  SendMessage,[hList],LB_SETCURSEL,-1,0
        invoke  SendMessage,[hList],LB_SETTOPINDEX,0,0
        invoke  SetForegroundWindow,[hWnd]
        invoke  ShowWindow,[hWnd],SW_SHOW
    .focus:
        invoke  GetDlgItem,[hWnd],IDB_ADD
        invoke  SetFocus,eax
        jmp     .processed

    .show_tray_popup:
        lea     eax,[pt]
        invoke  GetCursorPos,eax
        invoke  SetForegroundWindow,[hWnd]
        invoke  TrackPopupMenu,[hTrayMenu],TPM_RIGHTALIGN,\
                [pt.x],[pt.y],NULL,[hWnd],NULL
        invoke  PostMessage,[hWnd],WM_NULL,0,0
        jmp     .processed

  .wmcommand:
        cmp     [wParam],BN_CLICKED shl 16 + IDCANCEL
        je      .hide
        cmp     [wParam],BN_CLICKED shl 16 + IDB_ADD
        je      .add_item
        cmp     [wParam],BN_CLICKED shl 16 + IDB_WRITE2LOG
        je      .write2log
        cmp     [wParam],BN_CLICKED shl 16 + IDB_NOTES
        je      .notes
        cmp     [wParam],LBN_DBLCLK shl 16 + ID_ALARM_LIST
        je      .unselect_list
        cmp     [lParam],0
        jne     .processed
        cmp     [wParam],IDM_SHOWHIDE
        je      .showhide
        cmp     [wParam],IDM_ABOUT
        je      .about
        cmp     [wParam],IDM_SETUP
        je      .setup
        cmp     [wParam],IDM_EXIT
        je      .close
        xor     eax,eax
        jmp     .finish

    .add_item:
        invoke  DialogBoxParam,[hInstance],IDD_EDIT,[hWnd],EditDialog,0
      .unselect_list:
        invoke  SendMessage,[hList],LB_SETCURSEL,-1,0
        jmp     .focus

    .write2log:
        invoke  DialogBoxParam,[hInstance],IDD_WRITE2LOG,[hWnd],Write2LogDialog,0
        jmp     .focus

    .notes:
        invoke  DialogBoxParam,[hInstance],IDD_NOTES,[hWnd],NotesDialog,0
        jmp     .focus

    .about:
        invoke  DialogBoxParam,[hInstance],IDD_ABOUT,0,AboutDialog,0
        jmp     .focus

    .setup:
        invoke  GetKeyState,VK_LSHIFT
        or      eax,eax
        js      .anti_easter_egg
        test    [showflag],8
        jne     .close_setup
        invoke  DialogBoxParam,[hInstance],IDD_SETUP,0,SetupDialog,0
        jmp     .focus
      .close_setup:
        invoke  SendMessage,[hSetup],WM_CLOSE,0,0
        jmp     .focus

      .anti_easter_egg:         ; this don't show any cool stuff
        invoke  Sleep,500
        invoke  SendMessage,[hWnd],WM_SYSCOMMAND,SC_MONITORPOWER,2
        jmp     .processed

    .close:
        cmp     [repeat_alarm],0
        jne     .processed
        ;invoke  MessageBox,[hWnd],szMemory,szCaption,MB_OK
        ;mov     eax,1
        ;jmp     .finish

        cmp     [alarm_count],0
        je      .wmclose

        invoke  MessageBox,[hWnd],szAreYouSure,szCaption,\
                MB_ICONQUESTION or MB_OKCANCEL
        cmp     eax,IDOK
        je      .wmclose
        mov     eax,1
        jmp     .finish

  .wmclose:
        test    [showflag],2
        jz      .not_edit_dialog
        invoke  SendMessage,[hEdit],WM_CLOSE,0,0
      .not_edit_dialog:
        test    [showflag],4
        jz      .not_note_dialog
        invoke  SendMessage,[hNotes],WM_CLOSE,0,0
      .not_note_dialog:
        test    [showflag],8
        jz      .not_setup_dialog
        invoke  SendMessage,[hSetup],WM_CLOSE,0,0
      .not_setup_dialog:
        test    [showflag],16
        jz      .not_write2log_dialog
        invoke  SendMessage,[hLog],WM_CLOSE,0,0
      .not_write2log_dialog:
        test    [showflag],32
        jz      .not_about_dialog
        invoke  SendMessage,[hAbout],WM_CLOSE,0,0
      .not_about_dialog:
        ;call    save_alarm_list
        invoke  EndDialog,[hWnd],0
        jmp     .processed

  .wmdestroy:
        invoke  KillTimer,[hWnd],IDT_TIMER1
        invoke  DeleteObject,[hTodayBrush]
        invoke  Shell_NotifyIcon,NIM_DELETE,node
        invoke  DestroyMenu,[hTrayMenu]
        invoke  PostQuitMessage,0

  .processed:
        xor     eax,eax
  .finish:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; TabStop_TextOut - writes string with tab stops
; param:  hdc     - device handle
;         x       - x-coordinate
;         y       - y-coordinate
;         str     - string
;         len     - string length
;         tabs    - tab chars array (first is count of tabs,next are columns width)
; notes: write ends if extra tab chars found
proc TabStop_TextOut hdc:DWORD,x:DWORD,y:DWORD,str:DWORD,len:DWORD,tabs:DWORD
        push    ebx esi edi
        mov     esi,[str]
        mov     edi,[tabs]
        xor     ebx,ebx                 ; char position
        mov     edx,esi                 ; current string start
    .new_char:
        cmp     ebx,[len]
        jnb     .str_end
        cmp     byte [esi+ebx],9
        je      .is_tab
        inc     ebx
        jmp     .new_char
    .is_tab:
        lea     ecx,[esi+ebx]
        sub     ecx,edx                 ; count of chars
        jz      .empty_str
        invoke  TextOut,[hdc],[x],[y],edx,ecx
    .empty_str:
        inc     ebx
        lea     edx,[esi+ebx]           ; new start of string
        add     edi,4
        mov     eax,[edi]               ; get column width
        add     [x],eax
        mov     ecx,[tabs]
        mov     eax,edi
        sub     eax,ecx
        shr     eax,2
        cmp     eax,[ecx]               ; is tab array end?
        jbe     .new_char
    .str_end:
        lea     ecx,[esi+ebx]
        sub     ecx,edx
        jz      .done
        invoke  TextOut,[hdc],[x],[y],edx,ecx
    .done:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; do_alarm - plays user specify sound and show alarm message
; param:  hWnd     - window handle
;         alarm_no - alarm number (0=first)
proc do_alarm hWnd:DWORD,alarm_no:DWORD
local szAlarmBuffer[256]:BYTE
        push    ebx esi edi
        mov     edi,repeat_alarm
        mov     ebx,alarm_table
        call    do_sound
        mov     [per_min],0
        mov     [min_counter],0
        or      [repeat_alarm],1        ; flag for repeat sound

        imul    esi,[alarm_no],sizeof.ALARM_TYPE
        add     esi,alarm_table
        mov     edi,alarm_time
        mov     ecx,sizeof.ALARM_TYPE
        cld
        rep     movsb                   ; copy alarm in temp area
        lea     esi,[alarm_time.systime]
        lea     edi,[szAlarmBuffer]
        call    get_dayofweek
        mov     eax,dword [eax*4+weekdays4]
        mov     [edi],eax
        add     edi,[weekday_wide]
        inc     edi
        mov     byte [edi],' '
        inc     edi
        call    put_time
        mov     word [edi],'  '
        add     edi,2
        call    put_date
        mov     dword [edi],0a0d0a0dh
        add     edi,4
        mov     byte [edi],0
        lea     esi,[alarm_time.msg]
        cmp     byte [esi],0
        jnz     .user_msg
        mov     esi,szWakeUp
    .user_msg:
        call    copyz
        invoke  GetForegroundWindow
        push    eax
        lea     edi,[szAlarmBuffer]
        invoke  MessageBox,[hWnd],edi,szCaption,MB_OK or MB_SETFOREGROUND or MB_TOPMOST
        test    [repeat_alarm],2
        jz      .no_mp3
        invoke  mciSendString,szClose,0,0,0
      .no_mp3:
        pop     eax
        invoke  SetForegroundWindow,eax
        cmp     [logflag],0
        je      .dont_log
        mov     ebx,alarm_time
        call    save_to_log_file
    .dont_log:
        mov     [repeat_alarm],0
        mov     ecx,[alarm_no]
        call    remove_alarm
        cmp     [alarm_time.flag],' '
        je      .done
        mov     ebx,alarm_time
        call    add_alarm
        cmp     eax,1
        ja      .done
        call    save_alarm_list
    .done:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; do_sound - start sound effect, mp3 is started and stay playing
;            others sound effect is played to the end
; param:  ebx -> ALARM_TYPE structure
;         edi -> bit 1 is set if mp3 is played (don't start again if already played)
proc do_sound
local szMCIBuffer[768]:BYTE
        test    byte [edi],2            ; is mp3 flag?
        jnz     .done
        movzx   eax,[ebx+ALARM_TYPE.sound_type]
        cmp     eax,3                   ; 3=mp3 file
        je      .mp3
        movzx   ecx,[ebx+ALARM_TYPE.sound_count]
        or      ecx,ecx
        jz      .done
        cmp     ecx,9
        jbe     .count_ok
        mov     ecx,1
    .count_ok:
    .again:
        push    eax ecx
        cmp     eax,1
        ja      .siren                  ; 2=Siren
        je      .beep                   ; 1=Beep
    .wav:                               ; 0=wav file
if WAV_IN_MEMORY = 1
        invoke  PlaySound,sound_in_mem,NULL,SND_NODEFAULT or SND_MEMORY
else
        invoke  PlaySound,wav_file_name,NULL,SND_NODEFAULT or SND_FILENAME
end if
        or      eax,eax
        jnz     .next                   ; if play fail try message beep
    .beep:
        invoke  MessageBeep,-1
        invoke  Sleep,1000
        jmp     .next
    .siren:
        invoke  Beep,700,600
        invoke  Beep,1000,600
    .next:
        pop     ecx eax
        loop    .again
    .done:
        ret
    .mp3:
        lea     eax,[ebx+ALARM_TYPE.sound_file]
        lea     ecx,[szMCIBuffer]
        cinvoke wsprintf,ecx,szOpen,eax
        lea     ecx,[szMCIBuffer]
        invoke  mciSendString,ecx,0,0,0
        or      eax,eax
        mov     eax,0
        mov     ecx,2
        jnz     .again                  ; if mp3 is missing try wav
    .play:
        invoke  mciSendString,szPlay,0,0,0
        or      byte [edi],2            ; flag for mp3
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; show_current_time_date - update ID_CURRENT_TIME with current time
; param:  hWnd - window handle
; called when window is shown
proc show_current_time_date hWnd:DWORD
local szTimeBuffer[64]:BYTE
        push    ebx esi edi
        invoke  GetLocalTime,current_time
        mov     esi,current_time
        lea     edi,[szTimeBuffer]

        call    get_dayofweek
        mov     eax,dword [eax*4+weekdays4]
        mov     [edi],eax
        add     edi,[weekday_wide]
        inc     edi
        mov     byte [edi],' '
        inc     edi

        call    put_time
        mov     word [edi],'  '
        add     edi,2
        call    put_date
        lea     edi,[szTimeBuffer]
        invoke  SetDlgItemText,[hWnd],ID_CURRENT_TIME,edi
        mov     ax,[current_time.wDay]          ; has day changed
        cmp     ax,[today_day]
        je      .done
        mov     [today_day],ax
        call    highlight_alarms
        test    [showflag],1                    ; is window visible
        je      .done
        invoke  InvalidateRect,[hList],NULL,TRUE
    .done:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; load_alarm_list - loads Alarm.ini and add alarms in alarm list
; param:  hWnd - window handle
; change: eax,ebx,ecx,edx,esi,edi
; if old alarms are found they are shown and changed list is saved
; In [Alarms] section line format is:
;    Alarm001=hh:mm dd.mm.yyyy msg
;    Sound001=c:\fasmw\alarm\alarm.mp3
; char between time and date is repeat interval
; (space = none,
;  d = daily, w = weekly, m = monthly, y = yearly,
;  D = weekdays, E = weekends,
;  t = 2 weeks, f = 4 weeks, s = 6 weeks, e = 8 weeks)
proc load_alarm_list hWnd:DWORD
local skipped:DWORD                     ; flag for save ini file
        mov     [skipped],0
        invoke  GetPrivateProfileInt,szSectionInit,szSoundType,0,ini_file_name
        cmp     eax,4
        jb      .type_ok
        mov     eax,0
    .type_ok:
        mov     [sound_type],eax
        invoke  GetPrivateProfileInt,szSectionInit,szSoundCount,1,ini_file_name
        cmp     eax,9
        jbe     .count_ok
        mov     eax,1
    .count_ok:
        mov     [sound_count],eax
        invoke  GetPrivateProfileString,szSectionInit,szSoundFile,\
                szEmpty,mp3_file_name,FNAME_SIZE,ini_file_name

        ; get alarm list colors
        stdcall GetIniColor,ini_file_name,szSectionColors,szTodayText,text_colors+0
        stdcall GetIniColor,ini_file_name,szSectionColors,szTomorrowText,text_colors+4
        stdcall GetIniColor,ini_file_name,szSectionColors,szFutureText,text_colors+8
        stdcall GetIniColor,ini_file_name,szSectionColors,szActiveText,text_colors+12
        stdcall GetIniColor,ini_file_name,szSectionColors,szTodayBack,back_colors+0
        stdcall GetIniColor,ini_file_name,szSectionColors,szTomorrowBack,back_colors+4
        stdcall GetIniColor,ini_file_name,szSectionColors,szFutureBack,back_colors+8
        stdcall GetIniColor,ini_file_name,szSectionColors,szActiveBack,back_colors+12

        ; get old window position and size
        invoke  GetPrivateProfileInt,szSectionWindow,szLeft,20,ini_file_name
        mov     [saved_rect.left],eax
        invoke  GetPrivateProfileInt,szSectionWindow,szTop,20,ini_file_name
        mov     [saved_rect.top],eax
        invoke  GetPrivateProfileInt,szSectionWindow,szRight,410,ini_file_name
        mov     [saved_rect.right],eax
        invoke  GetPrivateProfileInt,szSectionWindow,szBottom,300,ini_file_name
        mov     [saved_rect.bottom],eax

        invoke  GetPrivateProfileInt,szSectionInit,szLogFile,0,ini_file_name
        mov     [logflag],eax
        invoke  GetPrivateProfileString,szSectionInit,szHolidays,\
                szEmpty,io_buffer,1024,ini_file_name
        stdcall get_holidays,[hWnd],io_buffer,eax
        xor     ecx,ecx
    .add_more:
        lea     eax,[ecx+1]
        call    alarm_number_str
        mov     dword [szAlarmNo],eax
        mov     dword [szSoundNo],eax
        push    ecx
        invoke  GetPrivateProfileString,szSectionAlarms,szAlarm,\
                szEmpty,io_buffer,256,ini_file_name
        pop     ecx
        cmp     byte [io_buffer],0
        je      .list_end
        push    ecx
        mov     esi,io_buffer
        mov     ebx,new_alarm
        call    ascii_to_alarm
        mov     eax,1
        jc      .error
        invoke  GetPrivateProfileString,szSectionAlarms,szSound,\
                szEmpty,new_alarm.sound_file,FNAME_SIZE,ini_file_name
        cmp     [new_alarm.sound_file],0
        je      .set_default
        mov     eax,1 shl 8 + 3
        cmp     byte [new_alarm.sound_file],'0'
        jb      .set_snd_data
        cmp     byte [new_alarm.sound_file],'9'
        ja      .set_snd_data
        mov     eax,dword [new_alarm.sound_file]
        and     eax,00000f0fh
        mov     [new_alarm.sound_file],0
        jmp     .set_snd_data
    .set_default:
        cmp     [sound_type],3
        jne     .not_mp3
        mov     esi,mp3_file_name
        mov     edi,new_alarm.sound_file
        call    copyz
    .not_mp3:
        mov     al,byte [sound_type]
        mov     ah,byte [sound_count]
    .set_snd_data:
        mov     [new_alarm.sound_type],al
        mov     [new_alarm.sound_count],ah
        call    add_alarm
    .error:
        test    eax,eax                 ; show only if old date flag
        je      .next_char              ; or updated repeat alarm
        mov     [skipped],1
        invoke  MessageBox,[hWnd],io_buffer,szSkipped,MB_OK or MB_SETFOREGROUND or MB_TOPMOST
    .next_char:
        pop     ecx
        inc     ecx
        cmp     ecx,MAX_ALARM
        jbe     .add_more
    .list_end:
        call    load_notes
        cmp     [skipped],0
        je      .done
        call    save_alarm_list
    .done:
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; change_startup_key
; change registry run key and return new status of run key
; param:  none
; return: eax = BST_UNCHECKED or BST_CHECKED
; change: ebx,ecx,edx,esi
; note: change HKEY_LOCAL_MACHINE to HKEY_CURRENT_USER if necessary
proc change_startup_key
local startup_key:DWORD
        call    get_run_key_state
        mov     [startup_key],eax
        cmp     eax,BST_UNCHECKED
        jne     .remove_run_key
    .add_run_key:
        invoke  GetModuleFileName,NULL,program_name,500 ; get the file name
        test    eax,eax
        jz      .done                   ; can't get name???
        ; make sure it is enclosed in quotes in case of spaces
        mov     word [program_name+eax],'"'
        lea     esi,[eax+3]             ; total length
        push    ebx                     ; ebx will get return value
        invoke  RegOpenKeyEx,HKEY_LOCAL_MACHINE,RegRunKey,0,KEY_WRITE,esp
        pop     ebx
        test    eax,eax
        jnz     .done
        invoke  RegSetValueEx,ebx,SubKey,0,REG_SZ,CurrentProgram,esi
        test    eax,eax
        jnz     .close_key
        mov     [startup_key],BST_CHECKED
        jmp     .close_key
    .remove_run_key:
        push    ebx                     ; ebx will get return value
        invoke  RegOpenKeyEx,HKEY_LOCAL_MACHINE,RegRunKey,0,KEY_WRITE,esp
        pop     ebx
        test    eax,eax
        jnz     .done
        invoke  RegDeleteValue,ebx,SubKey
        test    eax,eax
        jnz     .close_key
        mov     [startup_key],BST_UNCHECKED
    .close_key:
        invoke  RegCloseKey,ebx
    .done:
        mov     eax,[startup_key]
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_run_key_state
; return current status of run key
; param:  none
; return: eax = BST_UNCHECKED or BST_CHECKED
; change: ebx,ecx,edx
proc get_run_key_state
local startup_key:DWORD
        mov     [startup_key],BST_UNCHECKED
        push    ebx                     ; ebx will get return value
        invoke  RegOpenKeyEx,HKEY_LOCAL_MACHINE,RegRunKey,0,KEY_QUERY_VALUE,esp
        pop     ebx
        test    eax,eax
        jnz     .done
        push    ecx                     ; ecx will get return value
        mov     ecx,esp
        invoke  RegQueryValueEx,ebx,SubKey,NULL,ecx,NULL,NULL
        pop     ecx
        test    eax,eax
        jnz     .close_key
        cmp     ecx,REG_SZ
        jne     .close_key
        mov     [startup_key],BST_CHECKED
    .close_key:
        invoke  RegCloseKey,ebx
    .done:
        mov     eax,[startup_key]
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; update_tray_info
; change alarm count value and show next alarm in systray tooltip
; change: eax,ecx,edx
proc update_tray_info
local szTrayBuffer[128]:BYTE
        push    esi edi
        lea     edi,[szTrayBuffer]
        mov     eax,[alarm_count]
        mov     cl,1
        call    put_longint
        mov     byte [edi],'/'
        inc     edi
        mov     eax,MAX_ALARM
        mov     cl,1
        call    put_longint
        lea     edi,[szTrayBuffer]
        invoke  SendMessage,[hCount],WM_SETTEXT,0,edi

        lea     edi,[node.szTip]
        mov     dword [edi],'Alar'
        mov     dword [edi+4],'ms: '
        mov     word [edi+8],'0'
        mov     eax,[alarm_count]
        or      eax,eax
        jz      .done
        mov     esi,alarm_table+ALARM_TYPE.systime
        call    get_dayofweek
        mov     eax,dword [eax*4+weekdays4]
        mov     [edi],eax
        add     edi,[weekday_wide]
        inc     edi
        call    put_time
        mov     byte [edi],' '
        inc     edi
        call    put_date
        mov     byte [edi],0dh
        inc     edi
        mov     esi,alarm_table+ALARM_TYPE.msg
        mov     ecx,20
        cld
        rep     movsb
        mov     byte [edi],0    ;tip length=4+5+1+10+1+20+1=42 bytes
    .done:
        pop     edi esi
        invoke  Shell_NotifyIcon,NIM_MODIFY,node
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_holidays - convert ascii holiday list into holiday_table
; param:  hWnd        - window handle
;         holiday_str - Holiday list in format: d.m d.m d.m ...
;         size        - bytes in buffer
; change: eax,ebx,ecx,edx,esi,edi
proc get_holidays hWnd:DWORD,holiday_str:DWORD,size:DWORD
local szHolidaysBuffer[1024]:BYTE
local buffer_end:DWORD
        mov     eax,[size]
        mov     esi,[holiday_str]
        add     eax,esi
        mov     [buffer_end],eax
        xor     ebx,ebx
    .new:
        cmp     byte [esi],0
        je      .list_end
        call    get_number
        or      eax,eax
        jz      .list_end
        cmp     eax,31
        ja      .list_end
        cmp     byte [esi-1],'.'
        jne     .list_end
        mov     [ebx*2+holiday_table+HOLIDAY_TYPE.day],al
        call    get_number
        or      eax,eax
        jz      .list_end
        cmp     eax,12
        ja      .list_end
        mov     [ebx*2+holiday_table+HOLIDAY_TYPE.month],al
        inc     ebx
        cmp     esi,[buffer_end]
        jb      .new
    .list_end:
        mov     [holiday_count],ebx
        cmp     esi,[buffer_end]
        jnb     .done

        xor     esi,esi
        lea     edi,[szHolidaysBuffer]
    .get_list:
        cmp     esi,[holiday_count]
        jnb     .show_list
        movzx   eax,[esi*2+holiday_table+HOLIDAY_TYPE.day]
        mov     cl,2
        call    put_longint
        mov     byte [edi],'.'
        inc     edi
        movzx   eax,[esi*2+holiday_table+HOLIDAY_TYPE.month]
        mov     cl,2
        call    put_longint
        mov     dword [edi],0a0dh
        add     edi,2
        inc     esi
        jmp     .get_list
    .show_list:
        mov     byte [edi],0
        lea     edi,[szHolidaysBuffer]
        invoke  MessageBox,[hWnd],edi,szHolidayList,MB_OK
    .done:
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; holidays2str - convert holiday_table into asciiz string
; param:  edi -> buffer for asciiz holiday list
; change: eax,ecx,esi,edi
proc holidays2str
        xor     esi,esi
    .get_list:
        cmp     esi,[holiday_count]
        jnb     .show_list
        test    esi,esi
        jz      .first
        mov     byte [edi],' '
        inc     edi
    .first:
        movzx   eax,[esi*2+holiday_table+HOLIDAY_TYPE.day]
        mov     cl,1
        call    put_longint
        mov     byte [edi],'.'
        inc     edi
        movzx   eax,[esi*2+holiday_table+HOLIDAY_TYPE.month]
        mov     cl,1
        call    put_longint
        inc     esi
        jmp     .get_list
    .show_list:
        mov     byte [edi],0
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; highlight_alarms - set highlited background in today/tomorrow alarms
proc highlight_alarms
local today:ALARM_TYPE
local tomorrow:ALARM_TYPE
        push    ebx
        lea     ebx,[today]
        invoke  GetLocalTime,ebx
        lea     ebx,[tomorrow]
        mov     ax,[today+SYSTEMTIME.wDay]
        mov     [ebx+ALARM_TYPE.systime.wDay],ax
        mov     ax,[today+SYSTEMTIME.wMonth]
        mov     [ebx+ALARM_TYPE.systime.wMonth],ax
        mov     ax,[today+SYSTEMTIME.wYear]
        mov     [ebx+ALARM_TYPE.systime.wYear],ax
        mov     [ebx+ALARM_TYPE.flag],'d'
        call    next_alarm              ; set tomorrow date
        xor     ecx,ecx
    .new:
        cmp     ecx,[alarm_count]
        jnb     .list_end
        push    ecx
        imul    ebx,ecx,sizeof.ALARM_TYPE
        add     ebx,alarm_table

        xor     edx,edx                 ; today
        mov     ax,[ebx+ALARM_TYPE.systime.wYear]
        cmp     ax,[today+SYSTEMTIME.wYear]
        jne     .no_today
        mov     ax,[ebx+ALARM_TYPE.systime.wMonth]
        cmp     ax,[today+SYSTEMTIME.wMonth]
        jne     .no_today
        mov     ax,[ebx+ALARM_TYPE.systime.wDay]
        cmp     ax,[today+SYSTEMTIME.wDay]
        je      .set_day_flag
    .no_today:
        inc     edx                     ; tomorrow
        mov     ax,[ebx+ALARM_TYPE.systime.wYear]
        cmp     ax,[tomorrow.systime.wYear]
        jne     .no_tomorrow
        mov     ax,[ebx+ALARM_TYPE.systime.wMonth]
        cmp     ax,[tomorrow.systime.wMonth]
        jne     .no_tomorrow
        mov     ax,[ebx+ALARM_TYPE.systime.wDay]
        cmp     ax,[tomorrow.systime.wDay]
        je      .set_day_flag
    .no_tomorrow:
        inc     edx                     ; future
    .set_day_flag:
        invoke  SendMessage,[hList],LB_SETITEMDATA,ecx,edx
        pop     ecx
        inc     ecx
        jmp     .new
    .list_end:
        pop     ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; add_alarm - copy ALARM_TYPE in right place
; param:  ebx -> ALARM_TYPE structure
; return: eax = 0 - ok
;               1 - ok with updated repeat alarm
;               2 - old time/date
;               3 - error (list full)
; change: ecx,edx,esi,edi
proc add_alarm
local szAddBuffer[256]:BYTE
local update_old_date:DWORD
        cmp     [alarm_count],MAX_ALARM
        jnb     .error

        mov     [update_old_date],0
        invoke  GetLocalTime,current_time
        call    is_right_weekday
        jnc     .test_again
        call    next_alarm

    .test_again:
        mov     edi,current_time
        call    compare_date_time       ; is alarm time in future?
        cmp     eax,1
        ja      .future
        cmp     [ebx+ALARM_TYPE.flag],' '
        je      .error_old_date
        mov     [update_old_date],1
        call    next_alarm
        jnc     .test_again
    .error_old_date:
        mov     eax,2
        ret

    .error:
        mov     eax,3
        ret

    .future:
        lea     esi,[ebx+ALARM_TYPE.systime]
        lea     edi,[szAddBuffer]
        call    get_weekday
        mov     eax,dword [eax*4+weekdays4]
        mov     [edi],eax
        add     edi,3
        mov     byte [edi],9
        inc     edi
        call    put_time
        mov     byte [edi],9
        add     edi,1
        call    put_date
        cmp     byte [ebx+ALARM_TYPE.msg],0
        je      .empty
        mov     byte [edi],9
        inc     edi
        lea     esi,[ebx+ALARM_TYPE.msg]
        call    copyz
    .empty:
        call    add_list_pos
        lea     edi,[szAddBuffer]
        invoke  SendMessage,[hList],LB_INSERTSTRING,eax,edi
        call    highlight_alarms
        call    update_tray_info
        mov     eax,[update_old_date]   ; 0 = ok , 1 = updated repeat date
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; next_alarm - will add day/week/month/year into date
; param:  ebx -> ALARM_TYPE structure
; return: CF is set on error (unknown repeat char)
; change: eax
proc next_alarm
        push    edx ecx
    .wrong_day:
        movzx   ecx,[ebx+ALARM_TYPE.systime.wDay]
        movzx   edx,[ebx+ALARM_TYPE.systime.wMonth]
        mov     eax,1
        cmp     [ebx+ALARM_TYPE.flag],'d'
        je      .add_days
        cmp     [ebx+ALARM_TYPE.flag],'D'
        je      .add_days
        cmp     [ebx+ALARM_TYPE.flag],'E'
        je      .add_days
        mov     eax,7
        cmp     [ebx+ALARM_TYPE.flag],'w'
        je      .add_days
        mov     eax,7*2
        cmp     [ebx+ALARM_TYPE.flag],'t'
        je      .add_days
        mov     eax,7*4
        cmp     [ebx+ALARM_TYPE.flag],'f'
        je      .add_days
        mov     eax,7*6
        cmp     [ebx+ALARM_TYPE.flag],'s'
        je      .add_days
        mov     eax,7*8
        cmp     [ebx+ALARM_TYPE.flag],'e'
        je      .add_days
        cmp     [ebx+ALARM_TYPE.flag],'y'
        je      .next_year
        cmp     [ebx+ALARM_TYPE.flag],'m'
        jne     .error
        inc     edx
        cmp     edx,13
        jb      .date_ok
        mov     edx,1
    .next_year:
        inc     [ebx+ALARM_TYPE.systime.wYear]
    .date_ok:
        mov     [ebx+ALARM_TYPE.systime.wDay],cx
        mov     [ebx+ALARM_TYPE.systime.wMonth],dx
        call    is_right_weekday
        jc      .wrong_day
        pop     ecx edx
        ret
    .add_days:
        add     ecx,eax
    .again:
        mov     eax,[edx*4+days_in_month-4]
        cmp     edx,2
        jne     .not_feb
        movzx   eax,[ebx+ALARM_TYPE.systime.wYear]
        call    is_leap_year
        add     eax,28
    .not_feb:
        cmp     ecx,eax
        jbe     .date_ok
        sub     ecx,eax
        inc     edx
        cmp     edx,13
        jb      .again
        mov     edx,1
        inc     [ebx+ALARM_TYPE.systime.wYear]
        jmp     .again
    .error:
        stc
        pop     ecx edx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; remove_alarm
; param:  ecx = alarm index (0=first)
; change: eax,ecx,edx
proc remove_alarm
        push    ebx esi edi
        cmp     [alarm_count],0
        je      .done
        push    ecx
        invoke  SendMessage,[hList],LB_DELETESTRING,ecx,0
        pop     ecx
        cmp     eax,LB_ERR
        je      .done
        dec     [alarm_count]
        mov     eax,[alarm_count]
        sub     eax,ecx
        jbe     .save
        imul    edi,ecx,sizeof.ALARM_TYPE
        add     edi,alarm_table
        lea     esi,[edi+sizeof.ALARM_TYPE]
        imul    ecx,eax,sizeof.ALARM_TYPE
        cld
        rep     movsb
    .save:
        call    update_tray_info
        call    save_alarm_list
    .done:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; save_alarm_list - save alarm list in Alarm.ini file
; param:  none
; change: eax,ebx,ecx,edx,esi,edi
; uses WritePrivateProfileString to clear [Alarms] section
; writes each alarms into file using WriteFile
; Note: WritePrivateProfileString is slow when there is many alarms in list
proc save_alarm_list
local ioBytes:DWORD
local hFile:DWORD
        push    ebx esi edi
        invoke  WritePrivateProfileString,szSectionAlarms,NULL,NULL,ini_file_name
        call    write_sound_type
        call    write_sound_count
        call    write_sound_file
        ; flush write cache before open file
        invoke  WritePrivateProfileString,NULL,NULL,NULL,ini_file_name
        invoke  CreateFile,ini_file_name,GENERIC_WRITE,FILE_SHARE_READ,\
                    NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_ARCHIVE,NULL
        cmp     eax,INVALID_HANDLE_VALUE
        je      .done
        mov     [hFile],eax
        invoke  SetFilePointer,[hFile],0,NULL,FILE_END
        lea     eax,[ioBytes]
        invoke  WriteFile,[hFile],szAlarmsHeader,SZALARMS_SIZE,eax,NULL
        xor     ecx,ecx
    .new:
        cmp     ecx,[alarm_count]
        jnb     .list_end

        push    ecx
        lea     eax,[ecx+1]
        call    alarm_number_str
        or      eax,'=' shl 24
        mov     dword [szAlarmNo],eax
        mov     dword [szSoundNo],eax
        mov     edi,io_buffer
        mov     esi,szAlarm
        call    copyz
        imul    ebx,ecx,sizeof.ALARM_TYPE
        add     ebx,alarm_table
        lea     esi,[ebx+ALARM_TYPE.systime]
        call    put_time
        mov     al,[ebx+ALARM_TYPE.flag]
        mov     [edi],al
        inc     edi
        call    put_date
        lea     esi,[ebx+ALARM_TYPE.msg]
        cmp     byte [esi],0
        jz      .empty
        mov     byte [edi],' '
        inc     edi
        call    copyz
    .empty:
        mov     esi,szCrLfSound
        call    copyz
        mov     eax,00003030h
        or      al,[ebx+ALARM_TYPE.sound_type]
        or      ah,[ebx+ALARM_TYPE.sound_count]
        mov     [edi],eax
        add     edi,2
        cmp     al,'3'
        jne     .not_mp3
        sub     edi,2
        lea     esi,[ebx+ALARM_TYPE.sound_file]
        call    copyz
    .not_mp3:
        mov     word [edi],0a0dh
        add     edi,2
        mov     esi,io_buffer
        sub     edi,esi
        lea     eax,[ioBytes]
        invoke  WriteFile,[hFile],esi,edi,eax,NULL
        pop     ecx
        inc     ecx
        jmp     .new

    .list_end:
        invoke  CloseHandle,[hFile]
    .done:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; write_window_pos - write current window position and size into alarm.ini
; change: eax,ecx,edx
proc write_window_pos
local szBuffer[64]:BYTE
        push    edi
        lea     edi,[szBuffer]
        mov     eax,[rect.left]
        mov     cl,1
        call    put_longint
        lea     edi,[szBuffer]
        invoke  WritePrivateProfileString,szSectionWindow,szLeft,edi,ini_file_name
        mov     eax,[rect.top]
        mov     cl,1
        call    put_longint
        lea     edi,[szBuffer]
        invoke  WritePrivateProfileString,szSectionWindow,szTop,edi,ini_file_name
        mov     eax,[rect.right]
        mov     cl,1
        call    put_longint
        lea     edi,[szBuffer]
        invoke  WritePrivateProfileString,szSectionWindow,szRight,edi,ini_file_name
        mov     eax,[rect.bottom]
        mov     cl,1
        call    put_longint
        lea     edi,[szBuffer]
        invoke  WritePrivateProfileString,szSectionWindow,szBottom,edi,ini_file_name
        pop     edi
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc write_colors
        stdcall WriteIniColor,ini_file_name,szSectionColors,szTodayText,[text_colors+0]
        stdcall WriteIniColor,ini_file_name,szSectionColors,szTomorrowText,[text_colors+4]
        stdcall WriteIniColor,ini_file_name,szSectionColors,szFutureText,[text_colors+8]
        stdcall WriteIniColor,ini_file_name,szSectionColors,szActiveText,[text_colors+12]
        stdcall WriteIniColor,ini_file_name,szSectionColors,szTodayBack,[back_colors+0]
        stdcall WriteIniColor,ini_file_name,szSectionColors,szTomorrowBack,[back_colors+4]
        stdcall WriteIniColor,ini_file_name,szSectionColors,szFutureBack,[back_colors+8]
        stdcall WriteIniColor,ini_file_name,szSectionColors,szActiveBack,[back_colors+12]
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc write_sound_type
        mov     eax,[sound_type]
        and     eax,3
        or      al,'0'
        mov     dword [io_buffer],eax
        invoke  WritePrivateProfileString,szSectionInit,szSoundType,\
                io_buffer,ini_file_name
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc write_sound_count
        mov     eax,[sound_count]
        and     eax,0fh
        or      al,'0'
        mov     dword [io_buffer],eax
        invoke  WritePrivateProfileString,szSectionInit,szSoundCount,\
                io_buffer,ini_file_name
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc write_sound_file
        invoke  WritePrivateProfileString,szSectionInit,szSoundFile,\
                mp3_file_name,ini_file_name
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc write_log_type
        mov     eax,[logflag]
        and     eax,1
        or      al,'0'
        mov     dword [io_buffer],eax
        invoke  WritePrivateProfileString,szSectionInit,szLogFile,\
                io_buffer,ini_file_name
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc write_holidays
        push    edi
        mov     edi,io_buffer
        call    holidays2str
        invoke  WritePrivateProfileString,szSectionInit,szHolidays,\
                io_buffer,ini_file_name
        pop     edi
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; write_notes - writes user notes into Alarm.ini file section [Notes]
; change: eax,ecx,edx
proc write_notes
        push    ebx esi edi
        mov     esi,notes_buffer
    .line_start:
        mov     edi,esi
    .find_zero:
        mov     al,[esi]
        cmp     al,0
        je      .set_zero
        inc     esi
        cmp     al,0ah
        je      .find_zero
        cmp     al,0dh
        je      .find_zero
        jmp     .line_start
    .set_zero:
        mov     byte [edi],0
        mov     esi,notes_buffer
        mov     edi,io_buffer
    .new_line:
        cmp     byte [esi],0
        je      .buffer_end
        push    esi
        mov     esi,szLine
        call    copyz
        mov     byte [edi],'='
        inc     edi
        pop     esi
    .new_char:
        cmp     esi,notes_buffer+NOTES_SIZE
        jnb     .buffer_end
        mov     al,[esi]
        cmp     al,0
        je      .buffer_end
        inc     esi
        cmp     al,0ah
        je      .new_char
        cmp     al,0dh
        je      .line_end
        mov     [edi],al
        inc     edi
        jmp     .new_char
    .line_end:
        mov     word [edi],0a0dh
        add     edi,2
        jmp     .new_line
    .buffer_end:
        mov     dword [edi],0
        invoke  WritePrivateProfileSection,szSectionNotes,io_buffer,ini_file_name
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; load_notes - loads user notes from Alarm.ini file section [Notes]
; change: eax,ecx,edx
proc load_notes
local buffer_end:DWORD                  ; Bytes in [Notes] section
        push    ebx esi edi
        mov     esi,io_buffer
        invoke  GetPrivateProfileSection,szSectionNotes,esi,BUFFER_SIZE,ini_file_name
        add     eax,esi
        mov     [buffer_end],eax
        mov     edi,notes_buffer
    .new_note_line:
        cmp     byte [esi],'='
        jnz     .next_note_char
        inc     esi
    .copy_note_line:
        call    copyz
        cmp     esi,[buffer_end]
        jnb     .note_end
        cmp     edi,notes_buffer+NOTES_SIZE-4
        jnb     .note_end
        mov     word [edi],0a0dh
        add     edi,2
    .next_note_char:
        inc     esi
        cmp     byte [esi],0
        je      .note_end
        cmp     esi,[buffer_end]
        jb      .new_note_line
    .note_end:
        mov     byte [edi],0
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; save_to_log_file - save alarm in Alarm.log file
; param:  ebx -> ALARM_TYPE structure
; change: eax,ecx,edx
proc save_to_log_file
local szLogBuffer[256]:BYTE
local ioBytes:DWORD
local hFile:DWORD
        push    esi edi
        lea     esi,[ebx+ALARM_TYPE.systime]
        lea     edi,[szLogBuffer]
        call    get_weekday
        mov     eax,dword [eax*4+weekdays4]
        mov     [edi],eax
        add     edi,[weekday_wide]
        inc     edi
        call    put_time
        mov     byte [edi],' '
        inc     edi
        call    put_date
        mov     word [edi],' '
        inc     edi
        lea     esi,[ebx+ALARM_TYPE.msg]
        call    copyz
        mov     dword [edi],0a0dh
        add     edi,2
        lea     eax,[szLogBuffer]
        sub     edi,eax
        mov     [ioBytes],edi
        invoke  CreateFile,log_file_name,GENERIC_WRITE,FILE_SHARE_READ,\
                    NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_ARCHIVE,NULL
        cmp     eax,INVALID_HANDLE_VALUE
        jz      .done
        mov     [hFile],eax
        invoke  SetFilePointer,[hFile],0,NULL,FILE_END
        lea     eax,[ioBytes]
        lea     edi,[szLogBuffer]
        invoke  WriteFile,[hFile],edi,[ioBytes],eax,NULL
        invoke  CloseHandle,[hFile]
    .done:
        pop     edi esi
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; add_list_pos - add alarm in right position in alarm list
; param:  ebx -> ALARM_TYPE structure has new time/date/msg
; return: eax = alarm index (0=first)
; change: ecx
proc add_list_pos
        push    esi edi
        xor     eax,eax
        mov     edi,alarm_table
    .new_pos:
        cmp     eax,[alarm_count]
        jnb     .copy_it
        push    eax
        call    compare_date_time
        cmp     eax,1
        pop     eax
        jb      .make_room
        inc     eax
        add     edi,sizeof.ALARM_TYPE
        jmp     .new_pos
    .make_room:
        push    edi
        imul    ecx,[alarm_count],sizeof.ALARM_TYPE
        add     ecx,alarm_table
        lea     esi,[ecx-1]             ; last byte
        sub     ecx,edi                 ; count of bytes to move
        lea     edi,[esi+sizeof.ALARM_TYPE]
        std
        rep     movsb
        pop     edi
    .copy_it:
        inc     [alarm_count]
        mov     esi,ebx
        mov     ecx,sizeof.ALARM_TYPE
        cld
        rep     movsb                   ; copy alarm time/date/msg
        pop     edi esi
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; check_alarm
; return: eax = 0 - no alarm
;               1 - start alarm  (ecx=alarm number on table)
;               2 - repeat alarm
proc check_alarm
        cmp     [alarm_count],0
        jz      .no_alarm
        cmp     [repeat_alarm],0
        jne     .repeat_alarm
        xor     ecx,ecx

    .new:
        cmp     ecx,[alarm_count]
        jnb     .no_alarm

        push    ebx edi
        imul    ebx,ecx,sizeof.ALARM_TYPE
        add     ebx,alarm_table
        mov     edi,current_time
        call    compare_date_time
        pop     edi ebx

        cmp     eax,1
        jbe     .is_alarm
        ;inc     ecx                    ; no need to check any more
        ;jmp     .new                   ; list is ordered

    .no_alarm:
        xor     eax,eax                 ; 0=no alarm
        ret

    .is_alarm:
        mov     eax,1
        ret

    .stop_mp3:
        test    [repeat_alarm],2
        jz      .no_alarm
        and     [repeat_alarm],1
        invoke  mciSendString,szClose,0,0,0
        jmp     .no_alarm

    .repeat_alarm:
        cmp     [min_counter],10        ; if over 10min stop alarm sound
        jnb     .stop_mp3
        inc     [per_min]
        cmp     [per_min],TEST_PER_MIN
        jb      .no_alarm               ; counter mod TEST_PER_MIN
        mov     [per_min],0
        inc     [min_counter]           ; repeat sound in 1min period
        mov     eax,2
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; compare_date_time
; param:  ebx -> ALARM_TYPE structure
;         edi -> SYSTEMTIME structure
; return: eax = 0 - date has gone
;               1 - date and time is same
;               2 - date/time is in future
proc compare_date_time
        mov     ax,[ebx+ALARM_TYPE.systime.wYear]
        cmp     ax,[edi+SYSTEMTIME.wYear]
        jb      .gone
        ja      .future
        mov     ax,[ebx+ALARM_TYPE.systime.wMonth]
        cmp     ax,[edi+SYSTEMTIME.wMonth]
        jb      .gone
        ja      .future
        mov     ax,[ebx+ALARM_TYPE.systime.wDay]
        cmp     ax,[edi+SYSTEMTIME.wDay]
        jb      .gone
        ja      .future

        mov     ax,[ebx+ALARM_TYPE.systime.wHour]
        cmp     ax,[edi+SYSTEMTIME.wHour]
        jb      .gone
        ja      .future
        mov     ax,[ebx+ALARM_TYPE.systime.wMinute]
        cmp     ax,[edi+SYSTEMTIME.wMinute]
        jb      .gone
        ja      .future
        mov     eax,1
        ret

    .gone:
        xor     eax,eax
        ret

    .future:
        mov     eax,2
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; is_leap_year
; param:  eax = year
; return: eax = 0 - not leap year
;               1 - is leap year
proc is_leap_year
        test    al,3
        jnz     .not_leap
        push    ebx edx
        mov     ebx,100
        xor     edx,edx
        div     ebx
        test    edx,edx
        pop     edx ebx
        jnz     .is_leap
        test    al,3
        jnz     .not_leap
    .is_leap:
        mov     eax,1
        ret
    .not_leap:
        xor     eax,eax
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; is_right_weekday
; param:  ebx -> ALARM_TYPE structure
; return: CF clear on right day
proc is_right_weekday
        cmp     [ebx+ALARM_TYPE.flag],'D'
        jne     .not_weekdays
        call    is_holiday
        jc      .wrong_day
        call    get_weekday
        jnb     .wrong_day
    .right_day:
        clc
        ret
    .not_weekdays:
        cmp     [ebx+ALARM_TYPE.flag],'E'
        jne     .right_day
        call    is_holiday
        jc      .right_day
        call    get_weekday
        jnb     .right_day
    .wrong_day:
        stc
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; is_holiday - check if date found in local bank holiday list
; param:  ebx -> ALARM_TYPE structure
; return: CF set on bank holiday
; ToDo: add Easter formula
proc is_holiday
        push    esi
        xor     esi,esi
    .new:
        cmp     esi,[holiday_count]
        jnb     .not_holiday
        mov     ax,[ebx+ALARM_TYPE.systime.wDay]
        cmp     al,[esi*2+holiday_table+HOLIDAY_TYPE.day]
        jne     .next
        mov     ax,[ebx+ALARM_TYPE.systime.wMonth]
        cmp     al,[esi*2+holiday_table+HOLIDAY_TYPE.month]
        je      .is_holiday
    .next:
        inc     esi
        jmp     .new
    .not_holiday:
        pop     esi
        clc
        ret
    .is_holiday:
        pop     esi
        stc
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_weekday
; param:  ebx -> ALARM_TYPE structure
; return: eax = weekday (0=monday ... 6=sunday)
;         CF set on weekdays
proc get_weekday
        push    esi
        lea     esi,[ebx+ALARM_TYPE.systime]
        call    get_dayofweek
        pop     esi
        cmp     eax,5
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_dayofweek
; param:  esi -> SYSTEMTIME structure
; return: eax = weekday (0=monday ... 6=sunday)
proc get_dayofweek
        push    ebx ecx edx
        movzx   eax,[esi+SYSTEMTIME.wMonth]
        movzx   ecx,[esi+SYSTEMTIME.wYear]
        movzx   ebx,[esi+SYSTEMTIME.wDay]
        sub     eax,2
        ja      .above_feb
        dec     ecx
        add     eax,12
    .above_feb:
        push    ecx
        mov     ecx,100
        mov     edx,260
        mul     edx
        sub     eax,19
        div     ecx
        add     ebx,eax
        pop     eax
        xor     edx,edx
        div     ecx
        add     ebx,edx
        shr     edx,2
        add     ebx,edx
        mov     edx,eax
        shr     edx,2
        add     ebx,edx
        add     eax,eax
        inc     eax
        xor     edx,edx
        mov     ecx,7
        div     ecx
        mov     eax,ebx
        sub     eax,edx
        xor     edx,edx
        div     ecx
        mov     eax,edx
        pop     edx ecx ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
LOCALE_SABBREVDAYNAME1 = 31h

proc get_weekday_names
        push    ebx edi
        mov     edi,weekdays4
        mov     ebx,LOCALE_SABBREVDAYNAME1
      .new_day:
        invoke  GetLocaleInfo,LOCALE_USER_DEFAULT,ebx,edi,8
        or      eax,eax
        jz      .done
        mov     dword [edi+eax-1],'    '
        add     edi,4
        inc     ebx
        cmp     ebx,LOCALE_SABBREVDAYNAME1+7    ; loop until full week is done
        jb      .new_day
        mov     byte [edi],0
        mov     edx,3
        mov     eax,36
        cmp     byte [weekdays4+2],' '  ; is 2 char weekdays
        jne     .set_wide
        dec     edx
        sub     eax,8
        cmp     byte [weekdays4+1],' '  ; is 1 char weekdays
        jne     .set_wide
        dec     edx
        sub     eax,8
      .set_wide:
        mov     [weekday_wide],edx      ; set count of chars
        mov     [tab_stops],eax         ; change first column wide
      .done:
        pop     edi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; ascii_to_alarm
; param:  esi -> asciiz string (format: "hh:mm dd.mm.yyyy msg")
;         ebx -> ALARM_TYPE structure
; return: CF on error
; change: eax,ecx,edx
proc ascii_to_alarm
        ; get time
        call    get_number
        cmp     eax,24
        jnb     .error
        mov     ecx,eax
        call    get_number
        cmp     eax,60
        jnb     .error
        mov     [ebx+ALARM_TYPE.systime.wMinute],ax
        mov     [ebx+ALARM_TYPE.systime.wHour],cx

        ; get repeat char
    .skip:
        mov     al,[esi]
        mov     ah,' '
        cmp     al,0
        je      .error
        cmp     al,20h
        ja      .new
        inc     esi
        jmp     .skip
    .new:
        cmp     al,'9'
        jbe     .no_repeat
        inc     esi
        push    edi
        mov     edi,repeat_chars
        mov     ecx,REPEAT_CHAR_COUNT
        cld
        repne   scasb
        pop     edi
        jne     .no_repeat
        mov     ah,al
    .no_repeat:
        mov     [ebx+ALARM_TYPE.flag],ah

        ; get date
        call    get_number
        test    eax,eax
        jz      .error
        cmp     eax,31
        ja      .error
        mov     ecx,eax
        call    get_number
        test    eax,eax
        jz      .error
        cmp     eax,12
        ja      .error
        mov     edx,eax
        call    get_number
        cmp     eax,1900
        jb      .error
        cmp     eax,3000
        ja      .error
        mov     [ebx+ALARM_TYPE.systime.wYear],ax
        mov     [ebx+ALARM_TYPE.systime.wMonth],dx
        mov     [ebx+ALARM_TYPE.systime.wDay],cx
        call    get_str
        clc
        ret
    .error:
        stc
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc skip_file_path
        push    ebx edx
        mov     ebx,eax
    .path:
        mov     edx,ebx         ; start of filename
    .new:
        mov     al,[ebx]
        inc     ebx
        cmp     al,':'
        jz      .path
        cmp     al,'\'
        jz      .path
        cmp     al,'/'
        jz      .path
        cmp     al,0
        jnz     .new
        mov     eax,edx
        pop     edx ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; put_time
; param:  esi -> SYSTEMTIME structure
;         edi -> buffer for asciiz
proc put_time
        push    ecx
        movzx   eax,[esi+SYSTEMTIME.wHour]
        mov     cl,2
        call    put_longint
        mov     byte [edi],':'
        inc     edi
        movzx   eax,[esi+SYSTEMTIME.wMinute]
        mov     cl,2
        call    put_longint
        pop     ecx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; put_date
; param:  esi -> SYSTEMTIME structure
;         edi -> buffer for asciiz
proc put_date
        push    ecx
        movzx   eax,[esi+SYSTEMTIME.wDay]
        mov     cl,2
        call    put_longint
        mov     byte [edi],'.'
        inc     edi
        movzx   eax,[esi+SYSTEMTIME.wMonth]
        mov     cl,2
        call    put_longint
        mov     byte [edi],'.'
        inc     edi
        movzx   eax,[esi+SYSTEMTIME.wYear]
        mov     cl,4
        call    put_longint
        pop     ecx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc alarm_number_str
local str_buffer[16]:BYTE
        push    ecx edi
        mov     cl,3
        lea     edi,[str_buffer]
        call    put_longint
        mov     eax,dword [str_buffer]
        and     eax,00ffffffh
        pop     edi ecx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; put_longint
; param:  eax - longint
;         cl  - count of digits to display
;         edi -> buffer for Asciiz string
proc put_longint
        push    ebx edx
        xor     ch,ch
        mov     ebx,10
        or      eax,eax
        jns     .push_loop
        mov     byte [edi],'-'
        inc     edi
        neg     eax
    .push_loop:
        xor     edx,edx
        div     ebx
        push    edx
        inc     ch
        or      eax,eax
        jnz     .push_loop
        cmp     ch,cl
        jb      .push_loop
    .pop_loop:
        pop     eax
        or      al,'0'
        mov     [edi],al
        inc     edi
        dec     ch
        jnz     .pop_loop
        pop     edx ebx
        mov     byte [edi],0
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_number
; param:  esi -> asciiz string
; return: eax = number
proc get_number
        push    edx
        xor     edx,edx
        xor     eax,eax
    .skip:
        mov     al,[esi]
        cmp     al,0
        je      .done
        cmp     al,20h
        ja      .new
        inc     esi
        jmp     .skip
    .new:
        mov     al,[esi]
        cmp     al,0
        je      .done
        cmp     al,'A'
        jnb     .done
        inc     esi
        cmp     al,'9'
        ja      .done
        sub     al,'0'
        jb      .done
        imul    edx,10
        add     edx,eax
        jmp     .new
    .done:
        mov     eax,edx
        pop     edx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc get_str
        xor     edi,edi
    .new:
        mov     al,[esi]
        cmp     al,20h
        jb      .end
        jnz     .copy
        inc     esi
        jmp     .new
    .copy:
        mov     al,[esi]
        cmp     al,20h
        jb      .end
        inc     esi
        cmp     edi,ALARM_MSG_SIZE-1
        jnb     .copy
        mov     [ebx+edi+ALARM_TYPE.msg],al
        inc     edi
        jmp     .copy
    .end:
        mov     byte [ebx+edi+ALARM_TYPE.msg],0
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc copyz
    .new_char:
        mov     al,[esi]
        mov     [edi],al
        cmp     al,0
        jz      .zero
        inc     esi
        inc     edi
        jmp     .new_char
    .zero:
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_ini_fname - get full path of executable file
; and copy it for ini/wav/log file
proc get_ini_fname
        push    ebx esi edi
        invoke  GetModuleFileName,[hInstance],ini_file_name,500
        cld
        mov     ecx,500/4
        mov     esi,ini_file_name
        mov     edi,wav_file_name
        rep     movsd
        mov     ecx,500/4
        mov     esi,ini_file_name
        mov     edi,log_file_name
        rep     movsd
    .back:
        dec     eax
        js      .copy_name
        cmp     byte [eax+ini_file_name],'\'
        je      .copy_name
        cmp     byte [eax+ini_file_name],'/'
        jne     .back
    .copy_name:
        push    eax
        lea     edi,[eax+ini_file_name+1]
        mov     esi,ini_fname
        call    copyz
        pop     eax
        push    eax
        lea     edi,[eax+wav_file_name+1]
        mov     esi,wav_fname
        call    copyz
        pop     eax
        lea     edi,[eax+log_file_name+1]
        mov     esi,log_fname
        call    copyz
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc GetIniColor ini,sec,key,lpcolor
local szBuffer[256]:BYTE
        push    ebx esi edi
        lea     esi,[szBuffer]
        mov     byte [esi],0
        invoke  GetPrivateProfileString,[sec],[key],esi,esi,255,[ini]
        cmp     byte [esi],0
        je      .done
        call    get_number
        cmp     byte [esi],' '
        jbe     .num_ok
        mov     edi,eax
        call    get_number
        cmp     eax,0FFh
        ja      .done
        cmp     byte [esi],' '
        jbe     .done
        shl     eax,8
        or      edi,eax
        call    get_number
        cmp     eax,0FFh
        ja      .done
        shl     eax,16
        or      eax,edi
      .num_ok:
        mov     ebx,[lpcolor]
        mov     [ebx],eax
      .done:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc WriteIniColor ini,sec,key,color
local szBuffer[256]:BYTE
        push    ebx
        movzx   eax,byte [color]
        movzx   edx,byte [color+1]
        movzx   ecx,byte [color+2]
        lea     ebx,[szBuffer]
        cinvoke wsprintf,ebx,_color,eax,edx,ecx
        invoke  WritePrivateProfileString,[sec],[key],ebx,[ini]
        pop     ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; add_string_list - add list of asciiz strings to ComboBox
; param:  hWnd    - window handle
;         control - identifier of control
;         list    - list of asciiz strings (extra 0 = end of list)
proc add_string_list hWnd:DWORD,control:DWORD,list:DWORD
        push    edi
        mov     edi,[list]
      .new:
        invoke  SendDlgItemMessage,[hWnd],[control],CB_ADDSTRING,0,edi
        xor     al,al
        cld
      .char_loop:
        scasb
        jne     .char_loop
        cmp     al,[edi]
        jne     .new
        pop     edi
        ret
endp

;-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; center_window - move window into center of screen
; param:  hWnd - window handle
proc center_window hWnd:DWORD
local rect:RECT
        push    ebx esi edi
        invoke  GetSystemMetrics,SM_CXSCREEN
        mov     edi,eax
        invoke  GetSystemMetrics,SM_CYSCREEN
        mov     esi,eax
        lea     eax,[rect]
        invoke  GetWindowRect,[hWnd],eax
        mov     eax,[rect.right]
        sub     eax,[rect.left]
        sub     edi,eax
        mov     edx,[rect.bottom]
        sub     edx,[rect.top]
        sub     esi,edx
        shr     edi,1
        shr     esi,1
        invoke  SetWindowPos,[hWnd],HWND_TOP,edi,esi,eax,edx,NULL
        pop     edi esi ebx
        ret
endp

;-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc EditDialog hWnd,wMsg,wParam,lParam
local szBuffer[256]:BYTE
local hToolTip:DWORD
        push    ebx esi edi
        cmp     [wMsg],WM_COMMAND
        je      .wmcommand
        cmp     [wMsg],WM_CLOSE
        je      .wmclose
        cmp     [wMsg],WM_NOTIFY
        je      .wmnotify
        cmp     [wMsg],WM_INITDIALOG
        jne     .processed

    .initdlg:
        or      [showflag],2
        mov     eax,[hWnd]
        mov     [hEdit],eax
        mov     [old_flag],0
        mov     [mp3_flag],0
        invoke  GetDlgItem,[hWnd],ID_DATE
        mov     [hDatePicker],eax
        invoke  GetDlgItem,[hWnd],ID_DAY_LIST
        mov     [hDayList],eax

        stdcall add_string_list,[hWnd],ID_SND,szWav
        invoke  SendDlgItemMessage,[hWnd],ID_SNDCOUNT,EM_LIMITTEXT,1,0

        ; limit user input fields
        invoke  SendDlgItemMessage,[hWnd],ID_MESSAGE,EM_LIMITTEXT,ALARM_MSG_SIZE,0

        stdcall add_string_list,[hWnd],ID_REPEAT,szNone
        invoke  SendDlgItemMessage,[hWnd],ID_REPEAT,CB_SETCURSEL,0,0

        ; set format of DateTime control to string in DateFormat
        invoke  SendMessage,[hDatePicker],DTM_SETFORMAT,NULL,DateFormat

        ; set tooltips
        invoke  CreateWindowEx,WS_EX_TOPMOST,szToolTipClass,NULL,\
                WS_POPUP or TTS_NOPREFIX,0,0,0,0,[hWnd],NULL,[hInstance],NULL
        mov     [hToolTip],eax
        or      eax,eax
        jz      .tooltip_error
        mov     [ti.cbSize],sizeof.TOOLINFO
        mov     [ti.uFlags],TTF_SUBCLASS or TTF_IDISHWND
        push    [hWnd]
        pop     [ti.hwnd]
        push    [hInstance]
        pop     [ti.hInst]
        push    [hDatePicker]
        pop     [ti.uId]
        mov     [ti.lpszText],szDateTip
        invoke  SendMessage,[hToolTip],TTM_ADDTOOL,0,ti
        invoke  GetDlgItem,[hWnd],ID_NEW_ALARM
        mov     [ti.uId],eax
        mov     [ti.lpszText],szTimeTip
        invoke  SendMessage,[hToolTip],TTM_ADDTOOL,0,ti
      .tooltip_error:
        mov     eax,[sound_type]
        mov     [snd_type],eax
        mov     eax,[sound_count]
        mov     [snd_count],eax
        mov     [snd_file],mp3_file_name
        invoke  SendMessage,[hList],LB_GETCURSEL,0,0
        cmp     eax,LB_ERR
        je      .fill_days_alarm_list
        cmp     eax,[alarm_count]
        jnb     .fill_days_alarm_list
        ; get alarm info and show it
        mov     [old_flag],1
        imul    ebx,eax,sizeof.ALARM_TYPE
        add     ebx,alarm_table
        mov     edi,old_alarm
        mov     esi,ebx
        mov     ecx,sizeof.ALARM_TYPE
        cld
        rep     movsb                   ; copy alarm time/date/msg
        movzx   eax,[ebx+ALARM_TYPE.sound_type]
        mov     [snd_type],eax
        movzx   eax,[ebx+ALARM_TYPE.sound_count]
        mov     [snd_count],eax
        cmp     [snd_type],3            ; 3=mp3 file
        jnz     .not_mp3
        lea     esi,[ebx+ALARM_TYPE.sound_file]
        mov     [snd_file],esi
        mov     edi,mp3_file_name
        call    copyz
      .not_mp3:
        lea     esi,[ebx+ALARM_TYPE.systime]
        invoke  SendMessage,[hDatePicker],DTM_SETSYSTEMTIME,NULL,esi
        lea     edi,[szBuffer]
        call    put_time
        lea     edi,[szBuffer]
        invoke  SetDlgItemText,[hWnd],ID_NEW_ALARM,edi
        lea     eax,[ebx+ALARM_TYPE.msg]
        invoke  SetDlgItemText,[hWnd],ID_MESSAGE,eax
        mov     al,[ebx+ALARM_TYPE.flag]
        mov     edi,repeat_chars
        mov     ecx,REPEAT_CHAR_COUNT
        cld
        repne   scasb
        jne     .no_repeat
        neg     ecx
        add     ecx,REPEAT_CHAR_COUNT-1
      .no_repeat:
        invoke  SendDlgItemMessage,[hWnd],ID_REPEAT,CB_SETCURSEL,ecx,0
        invoke  GetDlgItem,[hWnd],IDB_DELETE
        invoke  EnableWindow,eax,TRUE
        invoke  SendMessage,[hWnd],WM_SETTEXT,0,szEditAlarm

    .fill_days_alarm_list:
        call    get_day_alarms

    .enable_disable:
        invoke  SendDlgItemMessage,[hWnd],ID_SND,CB_SETCURSEL,[snd_type],0
        invoke  SetDlgItemInt,[hWnd],ID_SNDCOUNT,[snd_count],FALSE
        mov     eax,[snd_file]
        call    skip_file_path
        invoke  SetDlgItemText,[hWnd],ID_FILENAME,eax
        stdcall enable_disable_snd,[hWnd]

    .focus:
        invoke  GetDlgItem,[hWnd],ID_NEW_ALARM
        invoke  SetFocus,eax
        jmp     .processed

  .wmnotify:
        mov     ebx,[lParam]
        mov     eax,[ebx+NMHDR.hwndFrom]
        cmp     eax,[hDatePicker]
        jne     .not_datepicker
        cmp     [ebx+NMHDR.code],DTN_CLOSEUP
        je      .focus
        cmp     [ebx+NMHDR.code],DTN_DATETIMECHANGE
        jne     .not_datepicker
        call    get_day_alarms
      .not_datepicker:
        jmp     .processed

    .wmcommand:
        cmp     [wParam],BN_CLICKED shl 16 + IDCANCEL
        je      .wmclose
        cmp     [wParam],BN_CLICKED shl 16 + IDB_DELETE
        je      .delete_item
        cmp     [wParam],BN_CLICKED shl 16 + IDB_OPENF
        je      .open
        cmp     [wParam],BN_CLICKED shl 16 + IDB_TESTSND
        je      .test_snd
        cmp     [wParam],BN_CLICKED shl 16 + IDOK
        je      .edit_item
        cmp     [wParam],CBN_CLOSEUP shl 16 + ID_REPEAT
        je      .focus
        cmp     [wParam],CBN_CLOSEUP shl 16 + ID_SND
        je      .focus
        cmp     [wParam],CBN_SELCHANGE shl 16 + ID_SND
        je      .snd_change
        cmp     word [wParam],ID_DAY_LIST
        je      .focus
        cmp     [wParam],EN_CHANGE shl 16 + ID_SNDCOUNT
        jne     .processed

    .snd_count_change:
        invoke  GetDlgItemInt,[hWnd],ID_SNDCOUNT,temp,FALSE
        cmp     [temp],TRUE
        jne     .processed
        cmp     eax,9
        ja      .processed
        cmp     eax,[snd_count]
        je      .snd_empty
        mov     [snd_count],eax
        mov     [sound_count],eax
        call    write_sound_count
        jmp     .focus

    .snd_change:
        invoke  SendDlgItemMessage,[hWnd],ID_SND,CB_GETCURSEL,0,0
        cmp     eax,CB_ERR
        je      .snd_empty
        cmp     eax,[snd_type]
        je      .snd_empty
        mov     [snd_type],eax
        mov     [sound_type],eax
        stdcall enable_disable_snd,[hWnd]
        call    write_sound_type
      .snd_empty:
        jmp     .focus

    .test_snd:
        cmp     [mp3_flag],0
        jne     .stop_mp3
        call    get_sound_info
        mov     ebx,new_alarm
        mov     edi,mp3_flag
        call    do_sound
        test    byte [edi],2    ; if mp3 is played, show stop button
        jz      .focus
        invoke  GetDlgItem,[hWnd],IDB_TESTSND
        invoke  SendMessage,eax,WM_SETTEXT,0,szStop
        jmp     .focus
    .stop_mp3:
        invoke  mciSendString,szClose,0,0,0
        invoke  GetDlgItem,[hWnd],IDB_TESTSND
        invoke  SendMessage,eax,WM_SETTEXT,0,szTest
        mov     [mp3_flag],0
        jmp     .focus

    .open:
        mov     [dOpenOfn.lStructSize],sizeof.OPENFILENAME
        push    [hInstance]
        pop     [dOpenOfn.hInstance]
        mov     [dOpenOfn.lpstrFilter],dOpenFilter
        mov     [dOpenOfn.lpstrFile],mp3_file_name
        mov     [dOpenOfn.nMaxFile],FNAME_SIZE
        mov     [dOpenOfn.Flags],OFN_FILEMUSTEXIST or OFN_PATHMUSTEXIST or\
                    OFN_LONGNAMES or OFN_HIDEREADONLY
        invoke  GetOpenFileName,dOpenOfn
        cmp     eax,TRUE
        jne     .old_mp3
        mov     eax,mp3_file_name
        call    skip_file_path
        invoke  SetDlgItemText,[hWnd],ID_FILENAME,eax
        call    write_sound_file
      .old_mp3:
        jmp     .focus

    .delete_item:
        invoke  MessageBox,[hWnd],szDeleteAlarm,szCaption,\
                MB_ICONQUESTION or MB_OKCANCEL
        cmp     eax,IDOK
        jne     .processed
        invoke  SendMessage,[hList],LB_GETCURSEL,0,0
        cmp     eax,LB_ERR
        je      .wmclose
        mov     ecx,eax
        call    remove_alarm
        jmp     .wmclose

    .edit_item:
        invoke  SendMessage,[hList],LB_GETCURSEL,0,0
        cmp     eax,LB_ERR
        je      .only_add
        mov     ecx,eax
        call    remove_alarm            ; if new alarm has error
        mov     [old_flag],2            ; flag for restore old alarm
      .only_add:
        stdcall set_alarm,[hWnd]
        test    eax,eax
        jnz     .focus
        mov     [old_flag],0

    .wmclose:
        cmp     [mp3_flag],0
        je      .close
        invoke  mciSendString,szClose,0,0,0
      .close:
        cmp     [old_flag],2            ; restore old alarm
        jne     .no_old_alarm
        mov     ebx,old_alarm
        call    add_alarm
      .no_old_alarm:
        and     byte [showflag],11111101b
        invoke  EndDialog,[hWnd],0
    .processed:
        xor     eax,eax
    .finish:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_day_alarms - get date from DatePicker and show that day alarms
proc get_day_alarms
local szTimeMsg[256]:BYTE
local szBuffer[64]:BYTE
local selected:DWORD
local current:DWORD
        invoke  SendMessage,[hDayList],LB_RESETCONTENT,0,0
        lea     esi,[szBuffer]
        invoke  SendMessage,[hDatePicker],WM_GETTEXT,32,esi
        cmp     byte [esi],0
        jz      .error
        mov     ecx,eax
    .skip_weekday:
        mov     al,[esi]
        inc     esi
        cmp     al,' '
        je      .weekday_skipped
        loop    .skip_weekday
    .error:
        ret

    .weekday_skipped:
        ; get date
        call    get_number
        test    eax,eax
        jz      .error
        cmp     eax,31
        ja      .error
        mov     ecx,eax
        call    get_number
        test    eax,eax
        jz      .error
        cmp     eax,12
        ja      .error
        mov     edx,eax
        call    get_number
        cmp     eax,1900
        jb      .error
        cmp     eax,3000
        ja      .error
        mov     [alarm_day.systime.wYear],ax
        mov     [alarm_day.systime.wMonth],dx
        mov     [alarm_day.systime.wDay],cx

        invoke  SendMessage,[hList],LB_GETCURSEL,0,0
        mov     [selected],eax

        xor     ecx,ecx
    .new:
        cmp     ecx,[alarm_count]
        jnb     .list_end
        push    ecx
        imul    ebx,ecx,sizeof.ALARM_TYPE
        add     ebx,alarm_table
        mov     [current],ecx
        call    is_same_day
        jnc     .next

        lea     edi,[szTimeMsg]
        lea     esi,[ebx+ALARM_TYPE.systime]
        call    put_time
        mov     byte [edi],' '
        inc     edi
        lea     esi,[ebx+ALARM_TYPE.msg]
        call    copyz

        lea     edi,[szTimeMsg]
        invoke  SendMessage,[hDayList],LB_ADDSTRING,0,edi
        mov     ecx,[current]
        cmp     ecx,[selected]
        jne     .next
        invoke  SendMessage,[hDayList],LB_SETCURSEL,eax,0
    .next:
        pop     ecx
        inc     ecx
        jmp     .new
    .list_end:
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; is_same_day - check if alarm date and DatePicker date will cross
; param:  ebx -> ALARM_TYPE structure (alarm list)
;         alarm_day -> ALARM_TYPE structure (DatePicker)
; return: CF set when alarm date is ok
proc is_same_day
local temp_date:ALARM_TYPE
        push    ebx
        mov     cx,[ebx+ALARM_TYPE.systime.wYear]
        cmp     cx,[alarm_day.systime.wYear]
        ja      .no
        jne     .test_repeat
        mov     cx,[ebx+ALARM_TYPE.systime.wMonth]
        cmp     cx,[alarm_day.systime.wMonth]
        ja      .no
        jne     .test_repeat
        mov     cx,[ebx+ALARM_TYPE.systime.wDay]
        cmp     cx,[alarm_day.systime.wDay]
        ja      .no
        je      .yes

    .test_repeat:
        mov     esi,ebx
        lea     edi,[temp_date]
        mov     ecx,sizeof.ALARM_TYPE
        cld
        rep     movsb
        mov     al,[ebx+ALARM_TYPE.flag]
        mov     [alarm_day.flag],al
        cmp     al,' '
        je      .no
        cmp     al,'d'
        je      .yes
        cmp     al,'E'
        je      .test_weekday
        cmp     al,'D'
        jne     .test_w
      .test_weekday:
        lea     ebx,[alarm_day]
        call    is_right_weekday
        jnc     .yes
        jmp     .no

    .test_w:
        cmp     al,'w'
        jne     .test_more
        lea     esi,[ebx+ALARM_TYPE.systime]
        call    get_dayofweek
        push    eax
        lea     esi,[alarm_day.systime]
        call    get_dayofweek
        pop     edx
        cmp     eax,edx
        je      .yes
    .no:
        pop     ebx
        clc
        ret

    .test_more:
        lea     ebx,[temp_date]
    .test_again:
        call    next_alarm
        jc      .no
        mov     ax,[ebx+ALARM_TYPE.systime.wYear]
        cmp     ax,[alarm_day.systime.wYear]
        jb      .test_again
        ja      .no
        mov     ax,[ebx+ALARM_TYPE.systime.wMonth]
        cmp     ax,[alarm_day.systime.wMonth]
        jb      .test_again
        ja      .no
        mov     ax,[ebx+ALARM_TYPE.systime.wDay]
        cmp     ax,[alarm_day.systime.wDay]
        jb      .test_again
        ja      .no
    .yes:
        pop     ebx
        stc
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; enable_disable_snd
; param:  hWnd - window handle
proc enable_disable_snd hWnd:DWORD
        push    ebx
        xor     ebx,ebx
        cmp     [snd_type],3
        jne     .disable_mp3
        xor     ebx,TRUE
      .disable_mp3:
        invoke  GetDlgItem,[hWnd],ID_FILENAME
        invoke  EnableWindow,eax,ebx
        xor     ebx,TRUE
        invoke  GetDlgItem,[hWnd],ID_SNDCOUNT
        invoke  EnableWindow,eax,ebx
        pop     ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; get_sound_info - get sound info from dialog items
; param:  none
; return: eax = 0 if successful
proc get_sound_info
        push    ebx esi edi
        invoke  SendDlgItemMessage,[hEdit],ID_SND,CB_GETCURSEL,0,0
        cmp     eax,CB_ERR
        jne     .type_error
        mov     eax,0
      .type_error:
        mov     [new_alarm.sound_type],al
        invoke  GetDlgItemInt,[hEdit],ID_SNDCOUNT,temp,FALSE
        cmp     [temp],TRUE
        jne     .default_count
        cmp     eax,9
        jbe     .count_ok
      .default_count:
        mov     eax,1
      .count_ok:
        mov     [new_alarm.sound_count],al
        mov     [new_alarm.sound_file],0

        cmp     [new_alarm.sound_type],3        ; 3=mp3 file
        jne     .not_mp3
        mov     esi,mp3_file_name
        cmp     byte [esi],0
        je      .error
        mov     edi,new_alarm.sound_file
        call    copyz
      .not_mp3:
        pop     edi esi ebx
        xor     eax,eax
        ret
    .error:
        pop     edi esi ebx
        mov     eax,1
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
; set_alarm - get time/date/msg from dialog items
; param:  hWnd - window handle
; return: eax = 0 if successful
proc set_alarm hWnd:DWORD
local szBuffer[256]:BYTE
local szAlarmLine[256]:BYTE
        push    ebx esi edi
        cmp     [alarm_count],MAX_ALARM
        jnb     .error

        lea     esi,[szBuffer]
        invoke  GetDlgItemText,[hWnd],ID_NEW_ALARM,esi,16
        cmp     byte [esi],0
        jz      .error
        lea     edi,[szAlarmLine]
        call    copyz
        invoke  SendDlgItemMessage,[hWnd],ID_REPEAT,CB_GETCURSEL,0,0
        mov     edx,' '
        cmp     eax,REPEAT_CHAR_COUNT
        jnb     .set_repeat
        mov     dl,[eax+repeat_chars]
    .set_repeat:
        mov     [edi],dx
        inc     edi

        lea     esi,[szBuffer]
        invoke  SendMessage,[hDatePicker],WM_GETTEXT,32,esi
        cmp     byte [esi],0
        jz      .error
        mov     ecx,eax
      .skip_weekday:
        mov     al,[esi]
        inc     esi
        cmp     al,' '
        je      .weekday_skipped
        loop    .skip_weekday
    .error:
        pop     edi esi ebx
        mov     eax,1
        ret

      .weekday_skipped:
        call    copyz
        mov     word [edi],' '
        inc     edi

        lea     esi,[szBuffer]
        invoke  GetDlgItemText,[hWnd],ID_MESSAGE,esi,ALARM_MSG_SIZE
        call    copyz

        lea     esi,[szAlarmLine]
        mov     ebx,new_alarm
        call    ascii_to_alarm
        jc      .error

        call    get_sound_info
        or      eax,eax
        jnz     .error

        call    add_alarm
        cmp     eax,1
        ja      .error
        call    save_alarm_list
        pop     edi esi ebx
        xor     eax,eax
        ret
endp

;-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc AboutDialog hWnd,wMsg,wParam,lParam
        push    ebx esi edi
        cmp     [wMsg],WM_COMMAND
        je      .wmcommand
        cmp     [wMsg],WM_CLOSE
        je      .wmclose
        cmp     [wMsg],WM_INITDIALOG
        jne     .processed

    .initdlg:
        or      [showflag],32
        mov     eax,[hWnd]
        mov     [hAbout],eax
        invoke  SendMessage,[hWnd],WM_SETICON,ICON_BIG,[hIcon]
        stdcall center_window,[hWnd]
        jmp     .processed

    .wmcommand:
        cmp     [wParam],BN_CLICKED shl 16 + IDCANCEL
        je      .wmclose
        cmp     [wParam],BN_CLICKED shl 16 + IDOK
        jne     .processed
    .wmclose:
        and     byte [showflag],11011111b
        invoke  EndDialog,[hWnd],0
    .processed:
        xor     eax,eax
    .finish:
        pop     edi esi ebx
        ret
endp

;-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc Write2LogDialog hWnd,wMsg,wParam,lParam
local log_alarm:ALARM_TYPE
        push    ebx esi edi
        cmp     [wMsg],WM_COMMAND
        je      .wmcommand
        cmp     [wMsg],WM_CLOSE
        je      .wmclose
        cmp     [wMsg],WM_INITDIALOG
        jne     .processed

    .initdlg:
        or      [showflag],16
        mov     eax,[hWnd]
        mov     [hLog],eax
        ; limit user input fields
        invoke  SendDlgItemMessage,[hWnd],ID_MESSAGE,EM_LIMITTEXT,ALARM_MSG_SIZE,0
        jmp     .processed

    .wmcommand:
        cmp     [wParam],BN_CLICKED shl 16 + IDCANCEL
        je      .wmclose
        cmp     [wParam],BN_CLICKED shl 16 + IDOK
        jne     .processed
        lea     eax,[log_alarm.msg]
        invoke  GetDlgItemText,[hWnd],ID_MESSAGE,eax,ALARM_MSG_SIZE
        cmp     byte [log_alarm.msg],0
        je      .wmclose
        lea     eax,[log_alarm.systime]
        invoke  GetLocalTime,eax
        lea     ebx,[log_alarm]
        call    save_to_log_file
    .wmclose:
        and     byte [showflag],11101111b
        invoke  EndDialog,[hWnd],0
    .processed:
        xor     eax,eax
    .finish:
        pop     edi esi ebx
        ret
endp

;-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc NotesDialog hWnd,wMsg,wParam,lParam
        push    ebx esi edi
        cmp     [wMsg],WM_COMMAND
        je      .wmcommand
        cmp     [wMsg],WM_CLOSE
        je      .wmclose
        cmp     [wMsg],WM_INITDIALOG
        jne     .processed

    .initdlg:
        or      [showflag],4
        mov     eax,[hWnd]
        mov     [hNotes],eax
        invoke  SetDlgItemText,[hWnd],IDE_NOTES,notes_buffer
        invoke  GetDlgItem,[hWnd],IDE_NOTES
        invoke  SetFocus,eax
        jmp     .processed

    .wmcommand:
        cmp     [wParam],BN_CLICKED shl 16 + IDCANCEL
        je      .wmclose
        cmp     [wParam],BN_CLICKED shl 16 + IDOK
        jne     .processed
        invoke  GetDlgItemText,[hWnd],IDE_NOTES,notes_buffer,NOTES_SIZE-4
        call    write_notes
    .wmclose:
        and     byte [showflag],11111011b
        invoke  EndDialog,[hWnd],0
    .processed:
        xor     eax,eax
    .finish:
        pop     edi esi ebx
        ret
endp

;-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
proc SetupDialog hWnd,wMsg,wParam,lParam
local szListBuffer[256]:BYTE
local cc:CHOOSECOLOR
local Tm:TEXTMETRIC
local hToolTip:DWORD
local hColorList:DWORD
        push    ebx esi edi
        cmp     [wMsg],WM_DRAWITEM
        je      .wmdrawitem
        cmp     [wMsg],WM_COMMAND
        je      .wmcommand
        cmp     [wMsg],WM_CLOSE
        je      .wmclose
        cmp     [wMsg],WM_INITDIALOG
        jne     .processed

    .initdlg:
        or      [showflag],8
        mov     eax,[hWnd]
        mov     [hSetup],eax
        invoke  SendMessage,[hWnd],WM_SETICON,ICON_BIG,[hIcon]
        mov     eax,BST_CHECKED
        cmp     [logflag],0
        jne     .set_log_flag
        mov     eax,BST_UNCHECKED
      .set_log_flag:
        invoke  CheckDlgButton,[hWnd],IDB_LOGFILE,eax
        call    get_run_key_state
        invoke  CheckDlgButton,[hWnd],IDB_STARTUP,eax
        mov     edi,szHolidayBuffer
        call    holidays2str
        invoke  SetDlgItemText,[hWnd],IDE_HOLIDAYS,szHolidayBuffer

        mov     esi,text_colors
        mov     edi,temp_colors
        mov     ecx,2*4
        cld
        rep     movsd
        invoke  GetDlgItem,[hWnd],ID_COLORS
        mov     [hColorList],eax
        invoke  SendMessage,[hColorList],LB_INSERTSTRING,-1,szTodayColor
        invoke  SendMessage,[hColorList],LB_INSERTSTRING,-1,szTomorrowColor
        invoke  SendMessage,[hColorList],LB_INSERTSTRING,-1,szFutureColor
        invoke  SendMessage,[hColorList],LB_INSERTSTRING,-1,szActiveColor

        ; set tooltips
        invoke  CreateWindowEx,WS_EX_TOPMOST,szToolTipClass,NULL,\
                WS_POPUP or TTS_NOPREFIX or TTS_BALLOON,0,0,0,0,[hWnd],NULL,[hInstance],NULL
        mov     [hToolTip],eax
        or      eax,eax
        jz      .tooltip_error
        mov     [ti.cbSize],sizeof.TOOLINFO
        mov     [ti.uFlags],TTF_SUBCLASS or TTF_IDISHWND
        push    [hWnd]
        pop     [ti.hwnd]
        push    [hInstance]
        pop     [ti.hInst]
        invoke  GetDlgItem,[hWnd],IDB_STARTUP
        mov     [ti.uId],eax
        mov     [ti.lpszText],szStartupTip
        invoke  SendMessage,[hToolTip],TTM_ADDTOOL,0,ti
        invoke  GetDlgItem,[hWnd],IDE_HOLIDAYS
        mov     [ti.uId],eax
        mov     [ti.lpszText],szHolidayTip
        invoke  SendMessage,[hToolTip],TTM_ADDTOOL,0,ti
      .tooltip_error:
        stdcall center_window,[hWnd]
        invoke  GetDlgItem,[hWnd],IDOK
        invoke  SetFocus,eax
        jmp     .processed

  .wmdrawitem:
        cmp     [wParam],ID_COLORS
        jne     .draw_default
        mov     esi,[lParam]
        test    [esi+DRAWITEMSTRUCT.itemAction],ODA_FOCUS
        jne     .draw_default
        cmp     [esi+DRAWITEMSTRUCT.itemID],-1
        je      .draw_default
        lea     eax,[Tm]
        invoke  GetTextMetrics,[esi+DRAWITEMSTRUCT.hDC],eax
        mov     ebx,[esi+DRAWITEMSTRUCT.itemID]
        cmp     ebx,3
        ja      .draw_default
        invoke  CreateSolidBrush,[ebx*4+4*4+temp_colors]
        mov     edi,eax
        lea     edx,[esi+DRAWITEMSTRUCT.rcItem]
        invoke  FillRect,[esi+DRAWITEMSTRUCT.hDC],edx,edi
        invoke  DeleteObject,edi
        invoke  SetTextColor,[esi+DRAWITEMSTRUCT.hDC],[ebx*4+temp_colors]
        invoke  SetBkColor,[esi+DRAWITEMSTRUCT.hDC],[ebx*4+4*4+temp_colors]
        mov     ebx,[esi+DRAWITEMSTRUCT.itemData]
        invoke  lstrlen,ebx
        mov     edx,eax
        mov     ecx,[esi+DRAWITEMSTRUCT.rcItem.left]
        add     ecx,4
        mov     eax,[esi+DRAWITEMSTRUCT.rcItem.bottom]
        add     eax,[esi+DRAWITEMSTRUCT.rcItem.top]
        sub     eax,[Tm+TEXTMETRIC.tmHeight]
        shr     eax,1
        invoke  TextOut,[esi+DRAWITEMSTRUCT.hDC],ecx,eax,ebx,edx
        cmp     [esi+DRAWITEMSTRUCT.itemID],3
        jne     .draw_done
        lea     eax,[esi+DRAWITEMSTRUCT.rcItem]
        invoke  DrawFocusRect,[esi+DRAWITEMSTRUCT.hDC],eax
    .draw_done:
        mov     eax,TRUE
        jmp     .finish
    .draw_default:
        invoke  DefWindowProc,[hWnd],[wMsg],[wParam],[lParam]
        jmp     .finish

    .wmcommand:
        mov     eax,[wParam]
        cmp     eax,BN_CLICKED shl 16 + IDCANCEL
        je      .wmclose
        cmp     eax,BN_CLICKED shl 16 + IDOK
        je      .ok
        cmp     eax,BN_CLICKED shl 16 + IDB_ACTIVE_BACK
        ja      .no_color_button
        sub     eax,BN_CLICKED shl 16 + IDB_TODAY_TEXT
        jb      .no_color_button
        lea     ebx,[eax*4+temp_colors]
        lea     edi,[cc]
        mov     ecx,sizeof.CHOOSECOLOR
        xor     eax,eax
        cld
        rep     stosb
        mov     [cc.lStructSize],sizeof.CHOOSECOLOR
        mov     eax,[hWnd]
        mov     [cc.hwndOwner],eax
        push    dword [ebx]
        pop     [cc.rgbResult]
        mov     [cc.lpCustColors],custColors
        mov     [cc.Flags],CC_RGBINIT or CC_FULLOPEN
        lea     eax,[cc]
        invoke  ChooseColor,eax
        or      eax,eax
        jz      .processed
        push    [cc.rgbResult]
        pop     dword [ebx]
        invoke  InvalidateRect,[hWnd],NULL,TRUE
      .no_color_button:
        jmp     .processed

      .ok:
        invoke  GetDlgItemText,[hWnd],IDE_HOLIDAYS,szHolidayBuffer,1024
        stdcall get_holidays,[hWnd],szHolidayBuffer,eax
        call    write_holidays
        invoke  IsDlgButtonChecked,[hWnd],IDB_LOGFILE
        cmp     eax,BST_CHECKED
        mov     eax,0
        jnz     .no_log
        inc     eax
      .no_log:
        cmp     eax,[logflag]
        je      .log_same
        mov     [logflag],eax
        call    write_log_type
      .log_same:
        call    get_run_key_state
        push    eax
        invoke  IsDlgButtonChecked,[hWnd],IDB_STARTUP
        pop     edx
        cmp     eax,edx
        je      .startup_same
        call    change_startup_key
      .startup_same:
        mov     esi,temp_colors
        mov     edi,text_colors
        mov     ecx,2*4
        cld
        repe    cmpsd
        je      .colors_same
        mov     esi,temp_colors
        mov     edi,text_colors
        mov     ecx,2*4
        rep     movsd
        call    write_colors
        invoke  DeleteObject,[hTodayBrush]
        or      eax,eax
        jz      .colors_same
        invoke  CreateSolidBrush,[0*4+back_colors]
        mov     [hTodayBrush],eax
        invoke  InvalidateRect,[hList],NULL,TRUE
        invoke  InvalidateRect,[hCurrentTime],NULL,TRUE
      .colors_same:
    .wmclose:
        and     byte [showflag],11110111b
        invoke  EndDialog,[hWnd],0
    .processed:
        xor     eax,eax
    .finish:
        pop     edi esi ebx
        ret
endp

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
section '.rsrc' resource data readable
directory RT_DIALOG,dialogs,\
          RT_ICON,icons,\
          RT_GROUP_ICON,group_icons,\
          RT_VERSION,versions

resource dialogs,\
         IDD_MAIN,LANG_NEUTRAL,diag,\
         IDD_EDIT,LANG_ENGLISH or SUBLANG_DEFAULT,edit_dialog,\
         IDD_NOTES,LANG_ENGLISH or SUBLANG_DEFAULT,notes_dialog,\
         IDD_WRITE2LOG,LANG_ENGLISH or SUBLANG_DEFAULT,write2log_dialog,\
         IDD_ABOUT,LANG_ENGLISH or SUBLANG_DEFAULT,about_dialog,\
         IDD_SETUP,LANG_ENGLISH or SUBLANG_DEFAULT,setup_dialog

resource icons,\
         11,LANG_NEUTRAL,icon1_data

resource group_icons,\
         IDI_MAIN,LANG_NEUTRAL,icon1

resource versions,\
         9,LANG_NEUTRAL,version

icon icon1, icon1_data, 'Alarm.ico'

versioninfo version,VOS__WINDOWS32,VFT_APP,VFT2_UNKNOWN,LANG_ENGLISH+SUBLANG_DEFAULT,0,\
            'CompanyName','ATV',\
            'FileDescription','Alarm Reminder',\
            'FileVersion',STR_VERSION,\
            'InternalName','Alarm Reminder',\
            'LegalCopyright','Open Source',\
            'OriginalFilename','ALARM.EXE',\
            'ProductName','Alarm Reminder',\
            'ProductVersion',STR_VERSION

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
dialog diag,DLGCAPTION,10,10,LIST_WIDTH+20,LIST_HEIGHT+60,\
       WS_CAPTION or WS_SYSMENU or WS_SIZEBOX,0,0,'MS Sans Serif Bold',10
    dialogitem 'STATIC','',ID_CURRENT_TIME,10,10,90,10,\
               WS_VISIBLE or WS_BORDER or SS_CENTER
    dialogitem 'STATIC','Alarm list:',-1,10,26,50,10,WS_VISIBLE
    dialogitem 'STATIC','',ID_COUNT,LIST_WIDTH-30,26,40,10,WS_VISIBLE or SS_RIGHT
    dialogitem 'LISTBOX','',ID_ALARM_LIST,10,40,LIST_WIDTH,LIST_HEIGHT,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP or WS_VSCROLL or\
               LBS_NOTIFY or LBS_OWNERDRAWFIXED or LBS_HASSTRINGS
    dialogitem 'BUTTON','Add / Edit',IDB_ADD,LIST_WIDTH-40,LIST_HEIGHT+40,50,13,\
               WS_VISIBLE or WS_TABSTOP or BS_DEFPUSHBUTTON
    dialogitem 'BUTTON','Notes',IDB_NOTES,LIST_WIDTH-20,10,30,13,\
               WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','Write to log',IDB_WRITE2LOG,20,LIST_HEIGHT+40,50,13,\
               WS_VISIBLE or WS_TABSTOP
enddialog

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
dialog edit_dialog,'Add alarm',0,20,260,160,\
               WS_CAPTION or WS_POPUP or WS_SYSMENU or DS_MODALFRAME,\
               0,0,'MS Sans Serif Bold',10
    dialogitem 'STATIC','Alarm date:',-1,10,10,50,10,WS_VISIBLE
    dialogitem 'STATIC','Repeat interval:',-1,10,30,70,10,WS_VISIBLE
    dialogitem 'STATIC','Alarm time:',-1,10,50,50,10,WS_VISIBLE
    dialogitem 'STATIC','Message:',-1,10,68,50,10,WS_VISIBLE

    dialogitem 'SysDateTimePick32','',ID_DATE,60,10,70,12,\
               WS_VISIBLE or WS_TABSTOP or DTS_RIGHTALIGN
    dialogitem 'COMBOBOX','',ID_REPEAT,80,30,50,110,\
               WS_VISIBLE or WS_TABSTOP or WS_VSCROLL or CBS_DROPDOWNLIST
    dialogitem 'EDIT','',ID_NEW_ALARM,60,50,40,12,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP
    dialogitem 'EDIT','',ID_MESSAGE,10,80,200,12,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP or ES_AUTOHSCROLL

    dialogitem 'BUTTON','Sound',-1,5,100,210,46,WS_VISIBLE or BS_GROUPBOX
    dialogitem 'COMBOBOX','',ID_SND,30,110,50,70,\
               WS_VISIBLE or WS_TABSTOP or WS_VSCROLL or CBS_DROPDOWNLIST
    dialogitem 'STATIC','*',-1,83,113,10,12,WS_VISIBLE
    dialogitem 'EDIT','',ID_SNDCOUNT,90,110,16,12,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP or ES_CENTER or ES_NUMBER
    dialogitem 'EDIT','',ID_FILENAME,30,130,180,10,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP or ES_AUTOHSCROLL
    dialogitem 'BUTTON','&File',IDB_OPENF,9,129,18,12,\
               WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','&Test',IDB_TESTSND,180,110,30,12,\
               WS_VISIBLE or WS_TABSTOP

    dialogitem 'BUTTON','&OK',IDOK,220,80,30,12,\
               WS_VISIBLE or WS_TABSTOP or BS_DEFPUSHBUTTON
    dialogitem 'BUTTON','&Delete',IDB_DELETE,220,100,30,12,\
               WS_VISIBLE or WS_TABSTOP or WS_DISABLED
    dialogitem 'BUTTON','&Cancel',IDCANCEL,220,120,30,12,WS_VISIBLE or WS_TABSTOP

    dialogitem 'LISTBOX','',ID_DAY_LIST,140,10,110,60,\
               WS_VISIBLE or WS_BORDER or WS_VSCROLL or \
               LBS_NOTIFY or LBS_SORT or LBS_HASSTRINGS
enddialog

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
dialog write2log_dialog,'Write message to log file',0,20,200,70,\
               WS_CAPTION or WS_POPUP or WS_SYSMENU or DS_MODALFRAME,\
               0,0,'MS Sans Serif Bold',10
    dialogitem 'STATIC','Message:',-1,10,10,60,10,WS_VISIBLE
    dialogitem 'EDIT','',ID_MESSAGE,10,25,180,12,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP or ES_AUTOHSCROLL
    dialogitem 'BUTTON','&OK',IDOK,10,50,40,14,\
               WS_VISIBLE or WS_TABSTOP or BS_DEFPUSHBUTTON
    dialogitem 'BUTTON','&Cancel',IDCANCEL,60,50,40,14,WS_VISIBLE or WS_TABSTOP
enddialog

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
dialog notes_dialog,'Notes',0,20,200,120,\
               WS_CAPTION or WS_POPUP or WS_SYSMENU or DS_MODALFRAME,\
               0,0,'MS Sans Serif Bold',10
    dialogitem 'EDIT','',IDE_NOTES,10,10,180,80,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP or WS_VSCROLL or \
               ES_MULTILINE or ES_AUTOHSCROLL or ES_WANTRETURN
    dialogitem 'BUTTON','&OK',IDOK,10,100,40,14,\
               WS_VISIBLE or WS_TABSTOP or BS_DEFPUSHBUTTON
    dialogitem 'BUTTON','&Cancel',IDCANCEL,60,100,40,14,WS_VISIBLE or WS_TABSTOP
enddialog

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
dialog setup_dialog,'Alarm setup',0,20,200,160,\
               WS_CAPTION or WS_POPUP or WS_SYSMENU or DS_MODALFRAME,\
               0,0,'MS Sans Serif Bold',10
    dialogitem 'BUTTON','Save used alarms into Alarm.log',IDB_LOGFILE,\
               10,10,180,15,WS_VISIBLE or BS_AUTOCHECKBOX or WS_TABSTOP
    dialogitem 'BUTTON','Load at startup',IDB_STARTUP,\
               10,30,180,15,WS_VISIBLE or BS_AUTOCHECKBOX or WS_TABSTOP
    dialogitem 'BUTTON','Bank holiday list:',-1,5,50,190,30,WS_VISIBLE or BS_GROUPBOX
    dialogitem 'EDIT','',IDE_HOLIDAYS,10,63,180,12,\
               WS_VISIBLE or WS_BORDER or WS_TABSTOP or ES_AUTOHSCROLL
    dialogitem 'BUTTON','Colors',-1,5,85,110,60,WS_VISIBLE or BS_GROUPBOX
    dialogitem 'LISTBOX','',ID_COLORS,35,99,50,40,\
               WS_VISIBLE or WS_BORDER or WS_VSCROLL or\
               LBS_NOTIFY or LBS_OWNERDRAWFIXED ;or LBS_HASSTRINGS
    dialogitem 'BUTTON','text',IDB_TODAY_TEXT,10,100,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','back',IDB_TODAY_BACK,90,100,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','text',IDB_TOMORROW_TEXT,10,108,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','back',IDB_TOMORROW_BACK,90,108,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','text',IDB_FUTURE_TEXT,10,116,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','back',IDB_FUTURE_BACK,90,116,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','text',IDB_ACTIVE_TEXT,10,124,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','back',IDB_ACTIVE_BACK,90,124,20,8,WS_VISIBLE or WS_TABSTOP
    dialogitem 'BUTTON','&OK',IDOK,150,100,40,14,\
               WS_VISIBLE or WS_TABSTOP or BS_DEFPUSHBUTTON
    dialogitem 'BUTTON','&Cancel',IDCANCEL,150,120,40,14,WS_VISIBLE or WS_TABSTOP
enddialog

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
dialog about_dialog,'About',0,20,140,80,\
               WS_CAPTION or WS_POPUP or WS_SYSMENU or DS_MODALFRAME
    dialogitem 'STATIC',IDI_MAIN,-1,8,8,32,32,WS_VISIBLE or SS_ICON
    dialogitem 'STATIC',<DLGCAPTION,' v',STR_VERSION,0dh,0ah,\
               'written by ATV',0dh,0ah,\
               'askovuori(a)hotmail.com'>,\
               -1,40,8,80,40,WS_VISIBLE or SS_CENTER
    dialogitem 'STATIC','',-1,5,45,130,10,WS_VISIBLE or SS_ETCHEDHORZ
    dialogitem 'STATIC',<'compiled with FASMW',0dh,0ah,\
                         'http://flatassembler.net'>,\
               -1,8,55,80,40,WS_VISIBLE
    dialogitem 'BUTTON','OK',IDOK,90,55,40,14,\
               WS_VISIBLE or WS_TABSTOP or BS_DEFPUSHBUTTON
enddialog

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
section '.idata' import data readable writeable

library kernel32,'kernel32.dll',\
        user32,'user32.dll',\
        shell32,'shell32.dll',\         ; for Shell_NotifyIcon
        advapi32,'advapi32.dll',\       ; for Win Registry
        comctl32,'comctl32.dll',\       ; for DateTimePicker
        comdlg32,'comdlg32.dll',\       ; for OpenFile
        gdi32,'gdi32.dll',\             ; for CreateSolidBrush
        winmm,'winmm.dll'               ; for PlaySound

import  winmm,\
        PlaySound,'PlaySound',\
        mciSendString,'mciSendStringA'

include 'api/kernel32.inc'
include 'api/user32.inc'
include 'api/shell32.inc'
include 'api/advapi32.inc'
include 'api/comctl32.inc'
include 'api/comdlg32.inc'
include 'api/gdi32.inc'

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
section '.data' data readable writeable
  szTaskBarCreated db "TaskbarCreated", 0
  szCaption     db 'Alarm',0
  szTitle       db DLGCAPTION,0
  szAbout       db '&About',0
  szSetup       db '&Setup',0
  szShowHide    db 'Show/&Hide',0
  szExit        db '&Exit',0
  szTest        db '&Test',0
  szStop        db '&Stop',0
  szCreateError db 'Can',27h,'t create window',0
  szAreYouSure  db 'There is alarm times left.',0dh,0ah
                db 'Do you really want to quit',0
  szDeleteAlarm db 'Delete alarm. Are you sure?',0
  ;szMemory      db 'You have memory problems, you',0dh,0ah
  ;              db 'don',27h,'t have rights to close this program',0
  szSkipped     db 'Skipped old alarm',0
  szWakeUp      db 'Wake up',0
  szEditAlarm   db 'Edit alarm',0
  szEmpty       db 0
  szSpace       db ' - ',0
  szHolidayList db 'Holiday list error',0
  ; Format for date in DateTimePicker
  DateFormat    db "ddd dd'.'MM'.'yyyy",0

  szToolTipClass db 'tooltips_class32',0
  szDateTip     db 'Date format: dd.mm.yyyy',0
  szTimeTip     db 'Time in 24h format: hh:mm',0
  szStartupTip  db 'Remember uncheck before you delete program',0
  szHolidayTip  db 'Holiday list format: d.m d.m d.m ...',0

  ; mci strings
  szOpen        db 'open "%s" type MPEGVideo alias alarmp3',0
  szPlay        db 'play alarmp3 from 0 repeat',0
  szStatus      db 'status alarmp3 mode',0
  szClose       db 'close alarmp3',0

  dOpenFilter   db 'Sound Files',0,'*.mp3;*.wav;*.mid',0
                db 0
  dDefExt       db 'mp3',0

  weekday_wide  dd 3
  weekdays4     db 'Mon Tue Wed Thu Fri Sat Sun     ',0 ; 4 extra bytes for read buffer
  today_day     dw -1
  days_in_month dd 31,28,31,30,31,30,31,31,30,31,30,31

  ; ini file strings
  szSectionAlarms db 'Alarms',0
    szAlarm       db 'Alarm'
    szAlarmNo     db '000=',0
    szCrLfSound   db 0dh,0ah
    szSound       db 'Sound'
    szSoundNo     db '000=',0
  szSectionInit   db 'Init',0
    szSoundType   db 'SoundType',0
    szSoundCount  db 'SoundCount',0
    szSoundFile   db 'SoundFile',0
    szLogFile     db 'LogFile',0
    szHolidays    db 'Holidays',0
  szSectionColors db 'Colors',0
    szTodayText   db 'TodayTextColor',0
    szTodayBack   db 'TodayBackColor',0
    szTomorrowText db 'TomorrowTextColor',0
    szTomorrowBack db 'TomorrowBackColor',0
    szFutureText  db 'FutureTextColor',0
    szFutureBack  db 'FutureBackColor',0
    szActiveText  db 'ActiveTextColor',0
    szActiveBack  db 'ActiveBackColor',0
  szSectionNotes  db 'Notes',0
    szLine        db 'Line',0
  szSectionWindow db 'Window',0
    szLeft        db 'Left',0
    szTop         db 'Top',0
    szRight       db 'Right',0
    szBottom      db 'Bottom',0
  szAlarmsHeader  db '[Alarms]',0dh,0ah,0
  SZALARMS_SIZE  = ($-szAlarmsHeader)-1

  _color          db '%d,%d,%d',0

  szTodayColor    db 'today',0
  szTomorrowColor db 'tomorrow',0
  szFutureColor   db 'future',0
  szActiveColor   db 'active',0
                  db 0          ; end of list

  align 8
                  ;  today    ,tomorrow ,future   ,active
  text_colors     dd 00000000h,00000000h,00000000h,00ffffffh
  back_colors     dd 00a0ffa0h,00a0ffffh,00ffffffh,00ff0000h

  tab_count     dd 3
  tab_stops     dd 36,46,84

  ; list for sound type combobox
  szWav         db 'Alarm.wav',0
  szBeep        db 'Beep',0
  szSiren       db 'Siren',0
  szMP3         db 'MP3',0
                db 0            ; end of list

  ; list for repeat intervals in combobox
  szNone        db 'none',0
  szDaily       db 'daily',0
  szWeekly      db 'weekly',0
  szMonthly     db 'monthly',0
  szYearly      db 'yearly',0
  szWeekdays    db 'weekdays',0
  szWeekends    db 'weekends',0
  sz2Weeks      db '2 weeks',0
  sz4Weeks      db '4 weeks',0
  sz6Weeks      db '6 weeks',0
  sz8Weeks      db '8 weeks',0
                db 0            ; end of list

  repeat_chars  db ' dwmyDEtfse'
  REPEAT_CHAR_COUNT = $-repeat_chars

  wav_fname     db 'Alarm.wav',0
  ini_fname     db 'Alarm.ini',0
  log_fname     db 'Alarm.log',0

if WAV_IN_MEMORY = 1
sound_in_mem:
  file          'Alarm.wav'
end if

  SubKey          db 'Alarm',0
  RegRunKey       db 'SOFTWARE\Microsoft\Windows\CurrentVersion\Run',0
  CurrentProgram  db '"'
  program_name    rb 514

  node          NOTIFYICONDATA
  current_time  SYSTEMTIME
  alarm_time    ALARM_TYPE
  new_alarm     ALARM_TYPE
  old_alarm     ALARM_TYPE
  alarm_day     ALARM_TYPE
  icex          INITCOMMONCONTROLSEX    ; structure for DateTimePicker
  saved_rect    RECT
  rect          RECT
  ti            TOOLINFO

  align 8

  showflag      rd 1            ; bit 0 - main window is visible
                                ; bit 1 - add/edit window is visible
                                ; bit 2 - notes window is visible
                                ; bit 3 - setup window is visible
                                ; bit 4 - write2log window is visible
                                ; bit 5 - about window is visible
  taskbarcreated rd 1
  hIcon         rd 1
  hInstance     rd 1
  hTrayMenu     rd 1            ; handle for systray menu
  hDatePicker   rd 1            ; handle for datepicker, edit dialog
  hDayList      rd 1            ; handle for day list, edit dialog
  hList         rd 1            ; handle for listbox
  hCount        rd 1            ; handle for alarm count
  hCurrentTime  rd 1            ; handle for current time / static
  hTodayBrush   rd 1            ; handle for today background color brush
  hAbout        rd 1            ; handle for about dialog
  hEdit         rd 1            ; handle for edit dialog
  hLog          rd 1            ; handle for write2log dialog
  hNotes        rd 1            ; handle for notes dialog
  hSetup        rd 1            ; handle for setup dialog
  logflag       rd 1            ; 0=no,1=yes save shown alarms in Alarm.log
  sound_type    rd 1            ; 0=Wav,1=Beep,2=Siren,3=MP3
  sound_count   rd 1            ; 0 to 9 times to repeat sound
  snd_type      rd 1            ; used by EditDialog
  snd_count     rd 1            ; used by EditDialog
  snd_file      rd 1            ; used by EditDialog
  mp3_flag      rd 1            ; used by EditDialog
  temp          rd 1            ; used by EditDialog
  old_flag      rd 1            ; used by EditDialog
  repeat_alarm  rd 1            ; 0=off,1=on,3=mp3
  per_min       rd 1            ; for repeat alarm
  min_counter   rd 1            ; for repeat alarm
  alarm_count   rd 1            ; counter for alarm_table
  holiday_count rd 1            ; counter for local bank holidays
  custColors    rd 16           ; used by SetupDialog
  temp_colors   rd 4*2          ; used by SetupDialog
  alarm_table   rb MAX_ALARM*sizeof.ALARM_TYPE
  holiday_table rb MAX_HOLIDAY*sizeof.HOLIDAY_TYPE

  align 8

  szHolidayBuffer rb 1024
  ini_file_name rb 512
  wav_file_name rb 512
  log_file_name rb 512
  mp3_file_name rb 512
  dOpenOfn      OPENFILENAME
  notes_buffer  rb NOTES_SIZE
  io_buffer     rb BUFFER_SIZE  ; used by ini,notes input/output
