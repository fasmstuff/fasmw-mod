





struct WINDMILL
   px	   dd ?
   py	   dd ?
   vx	   dd ?
   vy	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   Dalpha  db ?
   Dbeta   db ?
   Dgamma  db ?
	   db ?

	   dd ?
	   dd ?
ends


struct WINDMILLSPAWN
   px	   dd ?
   py	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   ct	   dd ?

ends



		      align  16
Windmill_Spawn:
			mov  esi,WindmillSpawnTable
			mov  eax,dword[FrameCount]
		       test  eax,0x03F
			jnz  .ret
			mov  eax,dword[WindmillSpawnCount]
			cmp  eax,WINDMILL_SPAWN_MAXCOUNT
			jae  .ret
			add  dword[WindmillSpawnCount],1
			shl  eax,4
			add  esi,eax
		       call  GetRand.word
			mov  dword[esi+WINDMILLSPAWN.alpha],eax
			xor  edx,edx
			mov  ecx,PLAY_WIDTH
			div  ecx
		       push  edx
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_HEIGHT
			div  ecx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WINDMILLSPAWN.py],xmm0
			pop  edx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WINDMILLSPAWN.px],xmm0

			mov  dword[esi+WINDMILLSPAWN.ct],64
	    .ret:	ret

align 4
.a dd 0.00002
.b dd 3.0





		      align  16
WindmillSpawn_Update:

			mov  ebx,[WindmillSpawnCount]
			mov  esi,WindmillSpawnTable
			mov  edi,esi
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+WINDMILLSPAWN.px]
			mov  eax,dword[esi+WINDMILLSPAWN.alpha]
			mov  edx,dword[esi+WINDMILLSPAWN.ct]
			sub  edx,1
			 js  .0
	      .4:     movsd  qword[edi+WINDMILLSPAWN.px],xmm0
			mov  dword[edi+WINDMILLSPAWN.alpha],eax
			mov  dword[edi+WINDMILLSPAWN.ct],edx
			add  edi,16
			jmp  .3
	      .0:	mov  ecx,dword[WindmillCount]
			 or  edx,-1
			cmp  ecx,WINDMILL_MAXCOUNT
			jae  .4
			add  dword[WindmillCount],1
			shl  ecx,5
			add  ecx,WindmillTable
		      movsd  qword[ecx+WINDMILL.px],xmm0
			mov  dword[ecx+WINDMILL.alpha],eax
		       call  GetRand.word
			and  eax,0x0FF
		      movsd  xmm0,[AngleTable+8*eax]
		      mulps  xmm0,dqword[.s]
		      movsd  qword[ecx+WINDMILL.vx],xmm0
		       call  GetRand.word
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WINDMILL.Dalpha],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WINDMILL.Dbeta],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WINDMILL.Dgamma],dl
	      .3:	add  esi,16
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,WindmillSpawnTable
			shr  edi,4
			mov  [WindmillSpawnCount],edi
			ret



align 16
.s dd WINDMILL_SPEED,WINDMILL_SPEED,WINDMILL_SPEED,WINDMILL_SPEED








		      align  16
Windmill_Update:


		       push  WINDMILL_HR
			mov  esi,WindmillTable
			mov  edi,esi
			mov  ebx,[WindmillCount]
			jmp  .2
	    .1:       movsd  xmm4,qword[esi+WINDMILL.px]
		      movsd  xmm5,qword[esi+WINDMILL.vx]
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		   cmpnltps  xmm4,xmm6
		       orps  xmm4,[const_f4x4_manmask]
		      andps  xmm5,xmm4
		      movsd  xmm4,qword[esi+WINDMILL.px]
		   cmpnltps  xmm4,xmm7
		      andps  xmm4,[const_f4x4_signmask]
		       orps  xmm5,xmm4
		      movsd  xmm4,qword[esi+WINDMILL.px]
		      addps  xmm4,xmm5
		       call  CheckShotIntersection
			 jc  .ex
		       movd  xmm0,dword[esi+WINDMILL.alpha]
		       movd  xmm1,dword[esi+WINDMILL.Dalpha]
		      paddb  xmm0,xmm1
		      movsd  qword[edi+WINDMILL.px],xmm4
		      movsd  qword[edi+WINDMILL.vx],xmm5
		       movd  dword[edi+WINDMILL.alpha],xmm0
		       movd  dword[edi+WINDMILL.Dalpha],xmm1
			add  edi,32
			jmp  .0
	    .ex:
		       push  dword[WindmillParColor]
		       push  WindmillExpCount
		       push  WindmillExpTable
		       push  WINDMILL_EXP_MAXCOUNT
		       push  WINDMILL_EXP_MINS
		       push  WINDMILL_EXP_DELTAS
		       push  WINDMILL_EXP_COUNT
		       push  WINDMILL_PAR_MINR
		       push  WINDMILL_PAR_DELTAR
		       push  WINDMILL_PAR_MINS
		       push  WINDMILL_PAR_DELTAS
		       push  WINDMILL_PAR_COUNT
		       call  ReleaseExplosion

		       call  ReleaseGeom
		       fild  qword[Score]
		       fild  qword[Multiplier]
		       push  WINDMILL_POINTS
		       fild  dword[esp]
		      fmulp  st1,st0
		      faddp  st1,st0
		      fistp  qword[Score]
			add  esp,4

	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,WindmillTable
			shr  edi,5
			mov  [WindmillCount],edi
			add  esp,4*1


		       call  GetRand.word
			xor  ecx,ecx
		       test  eax,0x03F
		       setz  cl
			add  ecx,dword[WindmillParColor]
			mov  eax,WINDMILL_COLOR_AVOID
			sub  eax,ecx
			and  eax,63
			mov  edx,WINDMILL_COLOR_AVOID+WINDMILL_COLOR_AVOID_WIDTH
			cmp  eax,16
		      cmovb  ecx,edx
			cmp  eax,64-WINDMILL_COLOR_AVOID_WIDTH
		      cmova  ecx,edx
			and  ecx,63
			mov  dword[WindmillParColor],ecx


			ret


align 16
.a: dd 0.9375,0.9375,0.9375,0.9375
.b: dd 0.0625,0.0625,0.0625,0.0625
.e: dd 1.00,1.00,1.00,1.00
.s: dd WINDMILL_SPEED,WINDMILL_SPEED,WINDMILL_SPEED,WINDMILL_SPEED
.r: dd 800.0,800.0,800.0,800.0














		      align  16
Windmill_Draw:

		     invoke  glColor4fv,addr ColorTable2+16*WINDMILL_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WindmillCount+CopyOffset]
			mov  esi,WindmillTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+WINDMILL.alpha]
		      movzx  edx,[esi+WINDMILL.beta]
		      movzx  ecx,[esi+WINDMILL.gamma]
		       call  SetRotationMatrix

			mov  ebp,28
			mov  edi,WindmillModel
	       .5:    movsd  xmm1,qword[esi+WINDMILL.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


		      align  16
WindmillModel:
		 dd +05.0,+00.0,+00.0,0   ,   +20.0,+20.0,+00.0,0
		 dd +00.0,+05.0,+00.0,0   ,   -20.0,+20.0,+00.0,0
		 dd -05.0,+00.0,+00.0,0   ,   -20.0,-20.0,+00.0,0
		 dd +00.0,-05.0,+00.0,0   ,   +20.0,-20.0,+00.0,0

		 dd +05.0,+00.0,+00.0,0   ,   +00.0,+05.0,+00.0,0
		 dd +00.0,+05.0,+00.0,0   ,   -05.0,+00.0,+00.0,0
		 dd -05.0,+00.0,+00.0,0   ,   +00.0,-05.0,+00.0,0
		 dd +00.0,-05.0,+00.0,0   ,   +05.0,+00.0,+00.0,0

		 dd +20.0,+00.0,+10.0,0   ,   +20.0,+00.0,-10.0,0
		 dd -20.0,+00.0,+10.0,0   ,   -20.0,+00.0,-10.0,0
		 dd +00.0,+20.0,+10.0,0   ,   +00.0,+20.0,-10.0,0
		 dd +00.0,-20.0,+10.0,0   ,   +00.0,-20.0,-10.0,0

		 dd +05.0,+00.0,+00.0,0   ,   +20.0,+00.0,+10.0,0
		 dd +05.0,+00.0,+00.0,0   ,   +20.0,+00.0,-10.0,0
		 dd +20.0,+20.0,+00.0,0   ,   +20.0,+00.0,+10.0,0
		 dd +20.0,+20.0,+00.0,0   ,   +20.0,+00.0,-10.0,0

		 dd +00.0,+05.0,+00.0,0   ,   +00.0,+20.0,+10.0,0
		 dd +00.0,+05.0,+00.0,0   ,   +00.0,+20.0,-10.0,0
		 dd -20.0,+20.0,+00.0,0   ,   +00.0,+20.0,+10.0,0
		 dd -20.0,+20.0,+00.0,0   ,   +00.0,+20.0,-10.0,0

		 dd -05.0,+00.0,+00.0,0   ,   -20.0,+00.0,+10.0,0
		 dd -05.0,+00.0,+00.0,0   ,   -20.0,+00.0,-10.0,0
		 dd -20.0,-20.0,+00.0,0   ,   -20.0,+00.0,+10.0,0
		 dd -20.0,-20.0,+00.0,0   ,   -20.0,+00.0,-10.0,0

		 dd +00.0,-05.0,+00.0,0   ,   +00.0,-20.0,+10.0,0
		 dd +00.0,-05.0,+00.0,0   ,   +00.0,-20.0,-10.0,0
		 dd +20.0,-20.0,+00.0,0   ,   +00.0,-20.0,+10.0,0
		 dd +20.0,-20.0,+00.0,0   ,   +00.0,-20.0,-10.0,0


		      align  16
WindmillSpawn_Draw:

		     invoke  glColor4fv,addr ColorTable2+16*WINDMILL_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WindmillSpawnCount+CopyOffset]
			mov  esi,WindmillSpawnTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+WINDMILLSPAWN.alpha]
		      movzx  edx,[esi+WINDMILLSPAWN.beta]
		      movzx  ecx,[esi+WINDMILLSPAWN.gamma]
		       call  SetRotationMatrix

			mov  eax,dword[esi+WINDMILLSPAWN.ct]
			and  eax,0x0F
		   cvtsi2ss  xmm0,eax
		      mulss  xmm0,dword[.a]
		      addss  xmm0,dword[.b]
		     shufps  xmm0,xmm0,0
		     movaps  xmm1,xmm0
		      mulps  xmm0,dqword[RotationMatrix.x]
		      mulps  xmm1,dqword[RotationMatrix.y]
		     movaps  dqword[RotationMatrix.x],xmm0
		     movaps  dqword[RotationMatrix.y],xmm1


			mov  ebp,28
			mov  edi,WindmillModel
	       .5:    movsd  xmm1,qword[esi+WINDMILLSPAWN.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,16
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


align 4

.a:	dd 0.125
.b	dd 1.0






		      align  16
Windmill_DestroyRadius:
			; xmm6 = center
			; xmm5 = radius
			mov  esi,WindmillTable
			mov  edi,esi
			mov  ebx,dword[WindmillCount]
			jmp  .2
	    .1:       movsd  xmm0,qword[esi+WINDMILL.px]
		      subps  xmm0,xmm6
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm5
		     movaps  xmm0,dqword[esi+16*0]
		     movaps  xmm1,dqword[esi+16*1]
			 jb  .ex
		     movaps  dqword[edi+16*0],xmm0
		     movaps  dqword[edi+16*1],xmm1
			add  edi,32
			jmp  .0
	    .ex:      movsd  xmm4,qword[esi+WINDMILL.px]
		      xorps  xmm7,xmm7
		       push  dword[WindmillParColor]
		       push  WindmillExpCount
		       push  WindmillExpTable
		       push  WINDMILL_EXP_MAXCOUNT
		       push  WINDMILL_EXP_MINS
		       push  WINDMILL_EXP_DELTAS
		       push  WINDMILL_EXP_COUNT/2
		       push  WINDMILL_PAR_MINR
		       push  WINDMILL_PAR_DELTAR
		       push  WINDMILL_PAR_MINS
		       push  WINDMILL_PAR_DELTAS
		       push  WINDMILL_PAR_COUNT/2
		       call  ReleaseExplosion
		       call  ReleaseGeom
	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,WindmillTable
			shr  edi,5
			mov  [WindmillCount],edi
			ret



