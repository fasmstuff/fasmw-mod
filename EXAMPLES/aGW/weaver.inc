
struct WEAVER
   px	   dd ?
   py	   dd ?
   vx	   dd ?
   vy	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   Dalpha  db ?
   Dbeta   db ?
   Dgamma  db ?
	   db ?

	   dd ?
	   dd ?
ends


struct WEAVERSPAWN
   px	   dd ?
   py	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   ct	   dd ?

ends



		      align  16
Weaver_Spawn:
			mov  esi,WeaverSpawnTable
			mov  eax,dword[FrameCount]
		       test  eax,0x7F
			jnz  .ret
			mov  eax,dword[WeaverSpawnCount]
			cmp  eax,WEAVER_SPAWN_MAXCOUNT
			jae  .ret
			add  dword[WeaverSpawnCount],1
			shl  eax,4
			add  esi,eax
		       call  GetRand.word
			mov  dword[esi+WEAVERSPAWN.alpha],eax
			xor  edx,edx
			mov  ecx,PLAY_WIDTH
			div  ecx
		       push  edx
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_HEIGHT
			div  ecx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WEAVERSPAWN.py],xmm0
			pop  edx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WEAVERSPAWN.px],xmm0

			mov  dword[esi+WEAVERSPAWN.ct],64
	    .ret:	ret

align 4
.a dd 0.00002
.b dd 3.0





		      align  16
WeaverSpawn_Update:

			mov  ebx,[WeaverSpawnCount]
			mov  esi,WeaverSpawnTable
			mov  edi,esi
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+WEAVERSPAWN.px]
			mov  eax,dword[esi+WEAVERSPAWN.alpha]
			mov  edx,dword[esi+WEAVERSPAWN.ct]
			sub  edx,1
			 js  .0
	      .4:     movsd  qword[edi+WEAVERSPAWN.px],xmm0
			mov  dword[edi+WEAVERSPAWN.alpha],eax
			mov  dword[edi+WEAVERSPAWN.ct],edx
			add  edi,16
			jmp  .3
	      .0:	mov  ecx,dword[WeaverCount]
			 or  edx,-1
		      xorps  xmm1,xmm1
			cmp  ecx,WEAVER_MAXCOUNT
			jae  .4
			add  dword[WeaverCount],1
			shl  ecx,5
			add  ecx,WeaverTable
		      movsd  qword[ecx+WEAVER.px],xmm0
		      movsd  qword[ecx+WEAVER.vx],xmm1
			mov  dword[ecx+WEAVER.alpha],eax
		       call  GetRand.word
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WEAVER.Dalpha],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WEAVER.Dbeta],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WEAVER.Dgamma],dl
	      .3:	add  esi,16
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,WeaverSpawnTable
			shr  edi,4
			mov  [WeaverSpawnCount],edi
			ret











		      align  16
Weaver_Update:

			xor  esi,esi
			mov  eax,WeaverTable
			jmp  .6
	     .5:	mov  edi,esi
			mov  ecx,esi
			shl  ecx,5
			add  ecx,WeaverTable
	     .7:
		      movsd  xmm0,qword[eax+WEAVER.px]
		      movsd  xmm1,qword[ecx+WEAVER.px]
		      subps  xmm0,xmm1
		     movaps  xmm2,xmm0
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		     movaps  xmm1,xmm0
		    cmpltps  xmm1,dqword[.r]
		      andps  xmm1,xmm2
		      maxps  xmm0,dqword[.e]
		    rsqrtps  xmm0,xmm0
		      mulps  xmm0,xmm1

		      movsd  xmm1,qword[eax+WEAVER.vx]
		      movsd  xmm2,qword[ecx+WEAVER.vx]
		      addps  xmm1,xmm0
		      subps  xmm2,xmm0
		      movsd  qword[eax+WEAVER.vx],xmm1
		      movsd  qword[ecx+WEAVER.vx],xmm2

			add  edi,1
			add  ecx,32
			cmp  edi,[WeaverCount]
			 jb  .7
			add  eax,32
	     .6:	add  esi,1
			cmp  esi,[WeaverCount]
			 jb  .5


			mov  esi,WeaverTable
			mov  ebx,[WeaverCount]
			jmp  .10
	     .9:	mov  edi,ShotTable
			mov  edx,[ShotCount]
		      xorps  xmm0,xmm0
			jmp  .12
	     .11:
		      movsd  xmm1,qword[edi+OBJ1.vx]
		      movsd  xmm2,qword[edi+OBJ1.px]
		      movsd  xmm3,qword[esi+WEAVER.px]
		      mulps  xmm1,dqword[.rep_l]
		      addps  xmm1,xmm2
		      subps  xmm3,xmm1
		     movaps  xmm1,xmm3
		      mulps  xmm3,xmm3
		     haddps  xmm3,xmm3
		     shufps  xmm3,xmm3,0
		     movaps  xmm2,xmm3
		      maxps  xmm3,dqword[.e]

		      mulps  xmm1,dqword[.rep_s]

		    rsqrtps  xmm3,xmm3
		    cmpltps  xmm2,dqword[.rep_r]
		      andps  xmm1,xmm2
		      mulps  xmm3,xmm1

		      addps  xmm0,xmm3

			add  edi,32
	     .12:	sub  edx,1
			jns  .11
		      movsd  xmm1,qword[esi+WEAVER.vx]
		      addps  xmm1,xmm0
		      movsd  qword[esi+WEAVER.vx],xmm1
			add  esi,32
	     .10:	sub  ebx,1
			jns  .9





		       push  WEAVER_HR
			mov  esi,WeaverTable
			mov  edi,esi
			mov  ebx,[WeaverCount]
			jmp  .2
	    .1:       movsd  xmm4,qword[esi+WEAVER.px]
		      movsd  xmm5,qword[esi+WEAVER.vx]
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		      addps  xmm4,xmm5
		      maxps  xmm4,xmm6
		      minps  xmm4,xmm7
		       call  CheckShotIntersection
			 jc  .ex
		      movsd  xmm0,qword[Player.px]
		      subps  xmm0,xmm4
		     movaps  xmm2,xmm0
		      mulps  xmm2,dqword[.s]
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		      maxss  xmm0,dword[.e]
		    rsqrtss  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		      mulps  xmm0,xmm2
		      mulps  xmm5,dqword[.a]
		      mulps  xmm0,dqword[.b]
		      addps  xmm5,xmm0
		       movd  xmm0,dword[esi+WEAVER.alpha]
		       movd  xmm1,dword[esi+WEAVER.Dalpha]
		      paddb  xmm0,xmm1
		      movsd  qword[edi+WEAVER.px],xmm4
		      movsd  qword[edi+WEAVER.vx],xmm5
		       movd  dword[edi+WEAVER.alpha],xmm0
		       movd  dword[edi+WEAVER.Dalpha],xmm1
			add  edi,32
			jmp  .0
	    .ex:
		       push  dword[WeaverParColor]
		       push  WeaverExpCount
		       push  WeaverExpTable
		       push  WEAVER_EXP_MAXCOUNT
		       push  WEAVER_EXP_MINS
		       push  WEAVER_EXP_DELTAS
		       push  WEAVER_EXP_COUNT
		       push  WEAVER_PAR_MINR
		       push  WEAVER_PAR_DELTAR
		       push  WEAVER_PAR_MINS
		       push  WEAVER_PAR_DELTAS
		       push  WEAVER_PAR_COUNT
		       call  ReleaseExplosion

		       call  ReleaseGeom
		       fild  qword[Score]
		       fild  qword[Multiplier]
		       push  WEAVER_POINTS
		       fild  dword[esp]
		      fmulp  st1,st0
		      faddp  st1,st0
		      fistp  qword[Score]
			add  esp,4

	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,WeaverTable
			shr  edi,5
			mov  [WeaverCount],edi
			add  esp,4*1
			ret


align 16
.a: dd 0.9375,0.9375,0.9375,0.9375
.b: dd 0.0625,0.0625,0.0625,0.0625
.e: dd 1.00,1.00,1.00,1.00
.s: dd WEAVER_SPEED,WEAVER_SPEED,WEAVER_SPEED,WEAVER_SPEED
.r: dd 1000.0,1000.0,1000.0,1000.0
.rep_r: dd 3000.0,3000.0,3000.0,3000.0
.rep_s: dd 0.7,0.7,0.7,0.7
.rep_l: dd 3.0,3.0,3.0,3.0














		      align  16
Weaver_Draw:

		     invoke  glColor4fv,addr ColorTable2+16*WEAVER_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WeaverCount+CopyOffset]
			mov  esi,WeaverTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+WEAVER.alpha]
		      movzx  edx,[esi+WEAVER.beta]
		      movzx  ecx,[esi+WEAVER.gamma]
		       call  SetRotationMatrix

			mov  ebp,24
			mov  edi,WeaverModel
	       .5:    movsd  xmm1,qword[esi+WEAVER.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


		      align  16
WeaverModel:
		 dd +20.0,+00.0,+00.0,0   ,   +00.0,+20.0,+00.0,0
		 dd +00.0,+20.0,+00.0,0   ,   -20.0,+00.0,+00.0,0
		 dd -20.0,+00.0,+00.0,0   ,   +00.0,-20.0,+00.0,0
		 dd +00.0,-20.0,+00.0,0   ,   +20.0,+00.0,+00.0,0

		 dd +17.0,+17.0,+10.0,0   ,   +17.0,+17.0,-10.0,0
		 dd +17.0,+17.0,+10.0,0   ,   +20.0,+00.0,+00.0,0
		 dd +17.0,+17.0,-10.0,0   ,   +20.0,+00.0,+00.0,0
		 dd +17.0,+17.0,+10.0,0   ,   +00.0,+20.0,+00.0,0
		 dd +17.0,+17.0,-10.0,0   ,   +00.0,+20.0,+00.0,0

		 dd +17.0,-17.0,+10.0,0   ,   +17.0,-17.0,-10.0,0
		 dd +17.0,-17.0,+10.0,0   ,   +20.0,+00.0,+00.0,0
		 dd +17.0,-17.0,-10.0,0   ,   +20.0,+00.0,+00.0,0
		 dd +17.0,-17.0,+10.0,0   ,   +00.0,-20.0,+00.0,0
		 dd +17.0,-17.0,-10.0,0   ,   +00.0,-20.0,+00.0,0

		 dd -17.0,+17.0,+10.0,0   ,   -17.0,+17.0,-10.0,0
		 dd -17.0,+17.0,+10.0,0   ,   -20.0,+00.0,+00.0,0
		 dd -17.0,+17.0,-10.0,0   ,   -20.0,+00.0,+00.0,0
		 dd -17.0,+17.0,+10.0,0   ,   +00.0,+20.0,+00.0,0
		 dd -17.0,+17.0,-10.0,0   ,   +00.0,+20.0,+00.0,0

		 dd -17.0,-17.0,+10.0,0   ,   -17.0,-17.0,-10.0,0
		 dd -17.0,-17.0,+10.0,0   ,   -20.0,+00.0,+00.0,0
		 dd -17.0,-17.0,-10.0,0   ,   -20.0,+00.0,+00.0,0
		 dd -17.0,-17.0,+10.0,0   ,   +00.0,-20.0,+00.0,0
		 dd -17.0,-17.0,-10.0,0   ,   +00.0,-20.0,+00.0,0



		      align  16
WeaverSpawn_Draw:

		     invoke  glColor4fv,addr ColorTable2+16*WEAVER_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WeaverSpawnCount+CopyOffset]
			mov  esi,WeaverSpawnTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+WEAVERSPAWN.alpha]
		      movzx  edx,[esi+WEAVERSPAWN.beta]
		      movzx  ecx,[esi+WEAVERSPAWN.gamma]
		       call  SetRotationMatrix

			mov  eax,dword[esi+WEAVERSPAWN.ct]
			and  eax,0x0F
		   cvtsi2ss  xmm0,eax
		      mulss  xmm0,dword[.a]
		      addss  xmm0,dword[.b]
		     shufps  xmm0,xmm0,0
		     movaps  xmm1,xmm0
		      mulps  xmm0,dqword[RotationMatrix.x]
		      mulps  xmm1,dqword[RotationMatrix.y]
		     movaps  dqword[RotationMatrix.x],xmm0
		     movaps  dqword[RotationMatrix.y],xmm1


			mov  ebp,24
			mov  edi,WeaverModel
	       .5:    movsd  xmm1,qword[esi+WEAVERSPAWN.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,16
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


align 4

.a:	dd 0.125
.b	dd 1.0









		      align  16
Weaver_DestroyRadius:
			; xmm6 = center
			; xmm5 = radius
			mov  esi,WeaverTable
			mov  edi,esi
			mov  ebx,dword[WeaverCount]
			jmp  .2
	    .1:       movsd  xmm0,qword[esi+WEAVER.px]
		      subps  xmm0,xmm6
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm5
		     movaps  xmm0,dqword[esi+16*0]
		     movaps  xmm1,dqword[esi+16*1]
			 jb  .ex
		     movaps  dqword[edi+16*0],xmm0
		     movaps  dqword[edi+16*1],xmm1
			add  edi,32
			jmp  .0
	    .ex:      movsd  xmm4,qword[esi+WEAVER.px]
		      xorps  xmm7,xmm7
		       push  dword[WeaverParColor]
		       push  WeaverExpCount
		       push  WeaverExpTable
		       push  WEAVER_EXP_MAXCOUNT
		       push  WEAVER_EXP_MINS
		       push  WEAVER_EXP_DELTAS
		       push  WEAVER_EXP_COUNT/2
		       push  WEAVER_PAR_MINR
		       push  WEAVER_PAR_DELTAR
		       push  WEAVER_PAR_MINS
		       push  WEAVER_PAR_DELTAS
		       push  WEAVER_PAR_COUNT/2
		       call  ReleaseExplosion
		       call  ReleaseGeom
	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,WeaverTable
			shr  edi,5
			mov  [WeaverCount],edi
			ret










