

struct PAR
   px	dd ?
   py	dd ?
   pz	dd ?
	dd ?
   vx	dd ?
   vy	dd ?
   vz	dd ?
   col	dd ?


ends



		      align  16
Par_Update:		mov  esi,ParTable
			mov  ebx,[ParCount]
			mov  edi,esi
		      movss  xmm7,dword[ParDamping]
		     shufps  xmm7,xmm7,0
			jmp  .2
	      .1:    movaps  xmm0,dqword[esi+PAR.px]
		     movaps  xmm2,dqword[esi+PAR.vx]
			mov  eax,dword[esi+PAR.col]
		      addps  xmm0,xmm2
		     movaps  xmm3,xmm2
		       dpps  xmm3,xmm2,01110001b
		     comiss  xmm3,dword[ParChop]
		      mulps  xmm2,xmm7
			 jb  .0
		     movaps  dqword[edi+PAR.px],xmm0
		     movaps  dqword[edi+PAR.vx],xmm2
			mov  dword[edi+PAR.col],eax
			add  edi,32
	      .0:	add  esi,32
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,ParTable
			shr  edi,5
			mov  [ParCount],edi
			ret




struct EXP
   px	dd ?
   py	dd ?
   vx	dd ?
   vy	dd ?
   a	dd ?
   da	dd ?
   ct	dd ?
	dd ?
ends


Exp_Update:
		     ; push  DiamondExpCount
		     ; push  DiamondExpTable
			mov  ebp,[esp+4*2]
			mov  ebx,[ebp]
			mov  esi,[esp+4*1]
			mov  edi,esi
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+EXP.px]
		      movsd  xmm1,qword[esi+EXP.vx]
			mov  eax,dword[esi+EXP.a]
			mov  ecx,dword[esi+EXP.da]
			mov  edx,dword[esi+EXP.ct]
		      addps  xmm0,xmm1
			add  eax,ecx
			and  eax,0x0FF
			sub  edx,1
			 js  .0
		   cmpnltps  xmm0,xmm6
		       orps  xmm0,[const_f4x4_manmask]
		      andps  xmm1,xmm0
		      movsd  xmm0,qword[esi+EXP.px]
		   cmpnltps  xmm0,xmm7
		      andps  xmm0,[const_f4x4_signmask]
		       orps  xmm1,xmm0
		      movsd  xmm0,qword[esi+EXP.px]
		      addps  xmm0,xmm1
		      movsd  qword[edi+EXP.px],xmm0
		      movsd  qword[edi+EXP.vx],xmm1
			mov  dword[edi+EXP.a],eax
			mov  dword[edi+EXP.da],ecx
			mov  dword[edi+EXP.ct],edx
			add  edi,32
	      .0:	add  esi,32
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,[esp+4*1]
			shr  edi,5
			mov  [ebp],edi
			ret  4*2








		      align  16
GetBlackholeForce_Par:
		    ;in:  xmm0 = position
		    ;out:  xmm1 = acceleration



		    push  esi ebx
		     mov  esi,BlackholeTable
		     mov  ebx,[BlackholeCount]
		   xorps  xmm1,xmm1
		     jmp  .2
	    .1:
		   movsd  xmm2,qword[esi+BLACKHOLE.px]
		     cmp  dword[esi+BLACKHOLE.hth],BLACKHOLE_MAXHEALTH
		   subps  xmm2,xmm0
		  movaps  xmm3,xmm2

		   mulps  xmm2,xmm2
		  haddps  xmm2,xmm2
		   addps  xmm2,dqword[.e]
		  shufps  xmm2,xmm2,0
		  movaps  xmm4,xmm2
		  movaps  xmm5,dqword[.r]
		cmpnltps  xmm5,xmm2

		;     jge  .3
		;   mulps  xmm4,xmm4
		;   mulps  xmm4,xmm2
		 rcpps	xmm2,xmm4

		   mulps  xmm3,dqword[.a]
		   mulps  xmm3,xmm2
		   andps  xmm3,xmm5
		   addps  xmm1,xmm3

	    ;      movaps  xmm3,xmm2
	    ;      haddps  xmm3,xmm3
	    ;      shufps  xmm3,xmm3,0
	    ;       mulps  xmm3,dqword[.0]
	    ;       addps  xmm5,dqword[.n]
	    ;       rcpps  xmm5,xmm5
	    ;      shufps  xmm2,xmm2,011110001b
	    ;       mulps  xmm2,dqword[.m]
	    ;       mulps  xmm2,xmm5
	    ;       mulps  xmm2,xmm3
	    ;       addps  xmm1,xmm2


	      .3:
		     add  esi,32
	    .2:      sub  ebx,1
		     jns  .1
		     pop  ebx esi
		     ret


align 16
.a dd 00040.0,00040.0,	0.0,0.0
.e dd 01000.0,01000.0,	10000.0,1000.0
.r dd 30000.0,30000.0,	100000.0,100000.0





		      align  16
GetBlackholeForce_Obj:

		    ;in:  xmm0 = position
		    ;out:  xmm1 = acceleration

		    push  esi ebx
		     mov  esi,BlackholeTable
		     mov  ebx,[BlackholeCount]
		   xorps  xmm1,xmm1
		     jmp  .2
	    .1:
		   movsd  xmm2,qword[esi+BLACKHOLE.px]
		     cmp  dword[esi+BLACKHOLE.hth],BLACKHOLE_MAXHEALTH
		   subps  xmm2,xmm0
		  movaps  xmm3,xmm2

		     jge  .3
		   mulps  xmm2,xmm2
		  haddps  xmm2,xmm2
		   addps  xmm2,dqword[.e]
		  shufps  xmm2,xmm2,0
		  movaps  xmm4,xmm2
		  movaps  xmm5,dqword[.r]
		cmpnltps  xmm5,xmm2

		   mulps  xmm4,xmm4
		   mulps  xmm4,xmm2
		 rsqrtps  xmm2,xmm4

		   mulps  xmm3,dqword[.a]
		   mulps  xmm3,xmm2
		   andps  xmm3,xmm5
		   addps  xmm1,xmm3

	      .3:
		     add  esi,32
	    .2:    ;  sub  ebx,1
		   ;  jns  .1
		     pop  ebx esi
		     ret


align 16
.a dd 01400.0,01400.0,	0.0,0.0
.e dd 01000.0,01000.0,	10000.0,1000.0
.r dd 30000.0,30000.0,	100000.0,100000.0






		      align  16
ReleaseExplosion:

		     ;  push  color index

		     ;  push  address of count
		     ;  push  address of table
		     ;  push  max count
		     ;  push  mins
		     ;  push  deltas
		     ;  push  count

		     ;  push  minr
		     ;  push  deltar
		     ;  push  mins
		     ;  push  deltas
		     ;  push  count
		     ; xmm4: position of object (preserved)
		     ;  xmm7: velocity

	.par:		mov  ecx,[ParCount]
			cmp  ecx,PAR_MAXCOUNT
			jae  .exp
			lea  eax,[ecx+1]
			shl  ecx,5
			add  ecx,ParTable
			mov  [ParCount],eax
		       call  GetRand.word

		       push  eax
			and  eax,0x0FF
		      movsd  xmm0,qword[AngleTable+8*eax]
		       call  GetRand.word
		      movsx  eax,ax
		   cvtsi2ss  xmm2,eax
		      movss  xmm3,dword[const_f4_1]
		      mulss  xmm2,dword[const_f4_1d2p15]
		      xorps  xmm1,xmm1
		      movss  xmm1,xmm2
		      mulss  xmm2,xmm2
		      subss  xmm3,xmm2
		      maxss  xmm3,dword[const_f4_1d2p10]
		     sqrtss  xmm3,xmm3
		     shufps  xmm3,xmm3,0
		      mulps  xmm0,xmm3
		     pslldq  xmm1,8
		       orps  xmm0,xmm1

			pop  eax
		   cvtsi2ss  xmm1,eax

		      mulss  xmm1,[esp+4*4]
		      addss  xmm1,[esp+4*5]
		     shufps  xmm1,xmm1,0
		      mulps  xmm1,xmm0
		      addps  xmm1,xmm4
		       call  GetRand.word
		   cvtsi2ss  xmm2,eax
		      mulss  xmm2,[esp+4*2]
		      addss  xmm2,[esp+4*3]
		     shufps  xmm2,xmm2,0
		      mulps  xmm2,xmm0
			mov  eax,[esp+4*12]
			shl  eax,4
		     movaps  dqword[ecx+PAR.px],xmm1
		     movaps  dqword[ecx+PAR.vx],xmm2
			mov  dword[ecx+PAR.col],eax
			sub  dword[esp+4*1],1
			jns  .par
	.exp:
			mov  ebp,[esp+4*11]
			jmp  .1
	.2:		mov  ecx,[ebp]
			cmp  ecx,[esp+4*9]
			jae  .ret
			lea  eax,[ecx+1]
			shl  ecx,5
			add  ecx,[esp+4*10]
			mov  [ebp],eax
		       call  GetRand.word
			mov  edx,eax
			and  eax,0x0FF
			shr  edx,7
			and  edx,0x1F
			sub  edx,16
			mov  dword[ecx+EXP.a],eax
			mov  dword[ecx+EXP.da],edx
		      movsd  xmm0,[AngleTable+8*eax]
		       call  GetRand.word
		   cvtsi2ss  xmm2,eax
		      mulss  xmm2,[esp+4*7]
		      addss  xmm2,[esp+4*8]
		     shufps  xmm2,xmm2,0
		      mulps  xmm2,xmm0
			and  eax,0x1F
			add  eax,0x20
			mov  dword[ecx+EXP.ct],eax
		      addps  xmm2,xmm7
		      addps  xmm2,xmm7
		      movsd  qword[ecx+EXP.px],xmm4
		      movsd  qword[ecx+EXP.vx],xmm2
	.1:		sub  dword[esp+4*6],1
			jns  .2


	.ret:
			ret  4*5+4*6+4*1




LineDraw:
		     movaps  xmm5,xmm7
		      subps  xmm5,xmm6
		     movaps  xmm0,xmm5
		      mulps  xmm0,xmm5
		     haddps  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		      addps  xmm0,dqword[.e]
		    rsqrtps  xmm0,xmm0
		      mulps  xmm5,xmm0
		     movaps  xmm4,xmm5
		     shufps  xmm4,xmm4,011110001b
		      mulps  xmm5,dqword[.n]
		      mulps  xmm4,dqword[.np]





		     invoke  glTexCoord2f,0.22,0.00
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      subps  xmm0,xmm5
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,0.75,0.00
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      subps  xmm0,xmm5
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.75,0.25
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.22,0.25
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f




		     invoke  glTexCoord2f,0.22,0.25
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,0.75,0.25
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.75,0.75
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.22,0.75
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f







		     invoke  glTexCoord2f,0.22,0.75
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,0.75,0.75
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.75,1.00
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      addps  xmm0,xmm5
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.22,1.00
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      addps  xmm0,xmm5
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f




			ret


align 16
.n:  dd +8.0,+8.0,0.0,0.0
.np: dd -4.0,+4.0,0.0,0.0
.e:  dd 0.0001,0.0001,0.0001,0.0001






LineDrawNoEnd:
		     movaps  xmm5,xmm7
		      subps  xmm5,xmm6
		     movaps  xmm0,xmm5
		      mulps  xmm0,xmm5
		     haddps  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		      addps  xmm0,dqword[.e]
		    rsqrtps  xmm0,xmm0
		      mulps  xmm5,xmm0
		     movaps  xmm4,xmm5
		     shufps  xmm4,xmm4,011110001b
		      mulps  xmm5,dqword[.n]
		      mulps  xmm4,dqword[.np]

			sub  esp,8*3

		      movsd  [esp+8*0],xmm4
		      movsd  [esp+8*1],xmm6
		      movsd  [esp+8*2],xmm7


		     invoke  glTexCoord2f,0.2177,0.25
		      movsd  xmm4,[esp+8*0]
		      movsd  xmm6,[esp+8*1]
		      movsd  xmm7,[esp+8*2]
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,0.75,0.25
		      movsd  xmm4,[esp+8*0]
		      movsd  xmm6,[esp+8*1]
		      movsd  xmm7,[esp+8*2]
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.75,0.75
		      movsd  xmm4,[esp+8*0]
		      movsd  xmm6,[esp+8*1]
		      movsd  xmm7,[esp+8*2]
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.2177,0.75
		      movsd  xmm4,[esp+8*0]
		      movsd  xmm6,[esp+8*1]
		      movsd  xmm7,[esp+8*2]
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


			add  esp,8*3

			ret


align 16
.n:  dd +8.0,+8.0,0.0,0.0
.np: dd -8.0,+8.0,0.0,0.0
.e:  dd 0.0001,0.0001,0.0001,0.0001
























		      align  16
Par_Draw:
		     invoke  glColor4fv,ParColorTable+16*15
		     invoke  glBindTexture,GL_TEXTURE_2D,[ParTexture]
		     invoke  glBegin,GL_TRIANGLES
			mov  ebx,[ParCount+CopyOffset]
			mov  esi,ParTable+CopyOffset
			jmp  .4
	      .3:	mov  eax,dword[esi+PAR.col]
			add  eax,ParColorTable
		     invoke  glColor4fv,eax
		       call  PrintPar
			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret




		      align  16
PrintPar:

virtual at esp
   dd ?,?,?,?
.n dd ?,?,?,?
.u dd ?,?,?,?
.v dd ?,?,?,?
.p dd ?,?,?,?
end virtual

		       push  ebp
			mov  ebp,esp

			sub  esp,16*6
			and  esp,-16


		      movsd  xmm0,qword[esi+PAR.vx]
		     movaps  xmm1,xmm0
		      mulps  xmm1,xmm0
		     haddps  xmm1,xmm1
		     shufps  xmm1,xmm1,0
		    rsqrtps  xmm1,xmm1
		      mulps  xmm1,xmm0

		     movaps  xmm0,dqword[esi+PAR.vx]
		      mulps  xmm0,dqword[.vs]
		      addps  xmm0,xmm1
		      addps  xmm0,xmm1
		      addps  xmm0,xmm1
		     movaps  dqword[.v],xmm0

		     shufps  xmm1,xmm1,011110001b
		      mulps  xmm1,dqword[.ns]
		     movaps  dqword[.n],xmm1

		     movaps  xmm0,dqword[esi+PAR.vx]
		     movaps  xmm1,xmm0
		       dpps  xmm1,xmm0,01111111b
		      mulps  xmm0,dqword[.us]
		    rsqrtps  xmm1,xmm1
		      mulps  xmm1,xmm0
		     movaps  dqword[.u],xmm1


		      addps  xmm1,dqword[.v]
		     movaps  dqword[.v],xmm1


		     movaps  xmm0,dqword[esi+PAR.px]

			cmp  dword[PausedQ],0
			jne  @f
			cmp  dword[CameraMode],CAMERA_MODE_ABOVE
			jne  @f
		      minps  xmm0,dqword[.max]
		      maxps  xmm0,dqword[.min]
		  @@:

		     movaps  dqword[.p],xmm0



		     invoke  glTexCoord2f,0.00,0.50
		     movaps  xmm0,dqword[.v]
		      mulps  xmm0,dqword[.h]
		      addps  xmm0,dqword[.n]
		      addps  xmm0,dqword[.up]
		      addps  xmm0,dqword[.p]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,0.50,0.00
		     movaps  xmm0,dqword[.p]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,0.50,1.00
		     movaps  xmm0,dqword[.v]
		      addps  xmm0,dqword[.p]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f


		     invoke  glTexCoord2f,1.00,0.50
		     movaps  xmm0,dqword[.v]
		      mulps  xmm0,dqword[.h]
		      subps  xmm0,dqword[.n]
		      addps  xmm0,dqword[.up]
		      addps  xmm0,dqword[.p]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,0.50,0.00
		     movaps  xmm0,dqword[.p]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,0.50,1.00
		     movaps  xmm0,dqword[.v]
		      addps  xmm0,dqword[.p]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f




			mov  esp,ebp
			pop  ebp
			ret


align 16
.vs dd +3.00,+3.00,+1.50,+1.00
.us dd +04.00,+04.00,+04.00,+0.00
.ns dd -8.00,+8.00,+0.00,+0.00
.h  dd 0.5,0.5,0.5,0.5
.up dd 0.0,0.0,4.0,0.0
.min dd -100000.0,-100000.0,-075.0,0.0
.max dd +100000.0,+100000.0,+075.0,0.0





		     invoke  glTexCoord2f,0.00,0.00
		     movaps  xmm0,dqword[.n]
		      subps  xmm0,dqword[.u]
		      addps  xmm0,dqword[esi+PAR.px]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,1.00,0.00
		      xorps  xmm0,xmm0
		      subps  xmm0,dqword[.n]
		      subps  xmm0,dqword[.u]
		      addps  xmm0,dqword[esi+PAR.px]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f


		     invoke  glTexCoord2f,0.00,0.25
		     movaps  xmm0,dqword[esi+PAR.px]
		      addps  xmm0,dqword[.n]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,1.00,0.25
		     movaps  xmm0,dqword[esi+PAR.px]
		      subps  xmm0,dqword[.n]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f


		     invoke  glTexCoord2f,0.00,0.75
		     movaps  xmm0,dqword[.v]
		      addps  xmm0,dqword[.n]
		      addps  xmm0,dqword[esi+PAR.px]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,1.00,0.75
		     movaps  xmm0,dqword[.v]
		      subps  xmm0,dqword[.n]
		      addps  xmm0,dqword[esi+PAR.px]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f


		     invoke  glTexCoord2f,0.00,1.00
		     movaps  xmm0,dqword[.v]
		      addps  xmm0,dqword[.n]
		      addps  xmm0,dqword[.u]
		      addps  xmm0,dqword[esi+PAR.px]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f

		     invoke  glTexCoord2f,1.00,1.00
		     movaps  xmm0,dqword[.v]
		      subps  xmm0,dqword[.n]
		      addps  xmm0,dqword[.u]
		      addps  xmm0,dqword[esi+PAR.px]
			sub  esp,4*3
		     movups  dqword[esp],xmm0
		     invoke  glVertex3f






		      align  16
Exp_Draw:
		     ; push  [DiamondExpCount+CopyOffset]
		     ; push  DiamondExpTable+CopyOffset

		     invoke  glBegin,GL_QUADS
			mov  ebx,[esp+4*2]
			mov  esi,[esp+4*1]
			jmp  .4
	      .3:      call  PrintExp
			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret  4*2





		      align  16
PrintExp:		mov  eax,dword[esi+EXP.a]
			and  eax,0x0FF
		      movsd  xmm6,qword[AngleTable+8*eax]
		      movsd  xmm7,qword[AngleTable+8*eax]
		     shufps  xmm6,xmm6,011110001b
		      mulps  xmm6,dqword[.n]
		      mulps  xmm7,dqword[.a]

			sub  esp,16

		      movsd  [esp+8*0],xmm6
		      movsd  [esp+8*1],xmm7



		     invoke  glTexCoord2f,0.0,0.0
		      movsd  xmm6,[esp+8*0]
		      movsd  xmm7,[esp+8*1]
			sub  esp,4*2
		      movsd  xmm0,qword[esi+EXP.px]
		      addps  xmm0,xmm7
		      addps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,1.0,0.0
		      movsd  xmm6,[esp+8*0]
		      movsd  xmm7,[esp+8*1]
			sub  esp,4*2
		      movsd  xmm0,qword[esi+EXP.px]
		      addps  xmm0,xmm7
		      subps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,1.0,1.0
		      movsd  xmm6,[esp+8*0]
		      movsd  xmm7,[esp+8*1]
			sub  esp,4*2
		      movsd  xmm0,qword[esi+EXP.px]
		      subps  xmm0,xmm7
		      subps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.0,1.0
		      movsd  xmm6,[esp+8*0]
		      movsd  xmm7,[esp+8*1]
			sub  esp,4*2
		      movsd  xmm0,qword[esi+EXP.px]
		      subps  xmm0,xmm7
		      addps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

			add  esp,16

			ret


align 16
.a: dd +20.0,+20.0,0.0,0.0
.n: dd +16.0,-16.0,0.0,0.0




