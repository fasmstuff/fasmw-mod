
struct GEOM
   px	   dd ?
   py	   dd ?
   vx	   dd ?
   vy	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   Dalpha  db ?
   Dbeta   db ?
   Dgamma  db ?
	   db ?

   ct	   dd ?
	   dd ?
ends





		      align  16
Geom_Update:	       push  2000.0
		       push  8000.0
		       push  GEOM_HR
		      movss  xmm0,dword[esp]
		       push  PLAYER_HR
		      addss  xmm0,dword[esp]
			add  esp,4
		      movss  dword[esp],xmm0
			mov  esi,GeomTable
			mov  edi,esi
			mov  ebx,[GeomCount]
			jmp  .2
	    .1:       movsd  xmm4,qword[esi+GEOM.px]
		      movsd  xmm5,qword[esi+GEOM.vx]
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		   cmpnltps  xmm4,xmm6
		       orps  xmm4,[const_f4x4_manmask]
		      andps  xmm5,xmm4
		      movsd  xmm4,qword[esi+GEOM.px]
		   cmpnltps  xmm4,xmm7
		      andps  xmm4,[const_f4x4_signmask]
		       orps  xmm5,xmm4
		      movsd  xmm4,qword[esi+GEOM.px]
		      addps  xmm4,xmm5
			mov  edx,dword[esi+GEOM.ct]
		      movsd  xmm1,qword[Player.px]
		     movaps  xmm0,xmm4
		      subps  xmm0,xmm1
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		      movsd  xmm1,qword[Player.px]
		      mulps  xmm1,dqword[.cf]
		     comiss  xmm0,dword[esp+4*0]
			 jb  .ex
		     comiss  xmm0,dword[esp+4*1]
			 ja  @f
		      mulps  xmm4,dqword[.f]
		      addps  xmm4,xmm1
		     comiss  xmm0,dword[esp+4*2]
			 ja  @f
		      movsd  xmm1,qword[Player.px]
		      mulps  xmm1,dqword[.cf2]
		      mulps  xmm4,dqword[.f2]
		      addps  xmm4,xmm1
		@@:    movd  xmm0,dword[esi+GEOM.alpha]
		       movd  xmm1,dword[esi+GEOM.Dalpha]
		      paddb  xmm0,xmm1
			sub  edx,1
			 js  .0
		      movsd  qword[edi+GEOM.px],xmm4
		      movsd  qword[edi+GEOM.vx],xmm5
		       movd  dword[edi+GEOM.alpha],xmm0
		       movd  dword[edi+GEOM.Dalpha],xmm1
			mov  dword[edi+GEOM.ct],edx
			add  edi,32
			jmp  .0
	    .ex:
		       fild  qword[Multiplier]
		       fld1
		      faddp  st1,st0
		      fistp  qword[Multiplier]

	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,GeomTable
			shr  edi,5
			mov  [GeomCount],edi
			add  esp,4*3
			ret


align 16
.f  dd 0.90,0.90,0.90,0.90
.cf dd 0.10,0.10,0.10,0.10
align 16
.f2  dd 0.80,0.80,0.80,0.80
.cf2 dd 0.20,0.20,0.20,0.20




		      align  16
ReleaseGeom:
		    ; xmm4: position of object (preserved)

	.par:		mov  ecx,[GeomCount]
			cmp  ecx,GEOM_MAXCOUNT
			jae  .ret
			lea  eax,[ecx+1]
			shl  ecx,5
			add  ecx,GeomTable
			mov  [GeomCount],eax
		       call  GetRand.word
			and  eax,0x0FF
		      movsd  xmm0,[AngleTable+8*eax]
		      mulps  xmm0,dqword[.m]
		      movsd  qword[ecx+GEOM.px],xmm4
		      movsd  qword[ecx+GEOM.vx],xmm0
			mov  dword[ecx+GEOM.alpha],0
			mov  dword[ecx+GEOM.Dalpha],0x00010000
			mov  dword[ecx+GEOM.ct],256
	.ret:
			ret

align 16
.m dd 0.5,0.5,0.5,0.5











		      align  16
Geom_Draw:

		     invoke  glColor4f,1.0,1.0,1.0,1.0
		     invoke  glBindTexture,GL_TEXTURE_2D,[GeomTexture]
		     invoke  glBegin,GL_QUADS
			mov  ebx,[GeomCount+CopyOffset]
			mov  esi,GeomTable+CopyOffset
			jmp  .4
	      .3:

			mov  eax,[esi+GEOM.ct]
			cmp  eax,256-32
			 ja  .5
			cmp  eax,64
			 ja  .6
		       test  eax,4
			 jz  .5

		.6:

		     invoke  glTexCoord2f,0.0,0.0
			sub  esp,4*2
		      movzx  eax,[esi+GEOM.gamma]
		      movsd  xmm0,qword[AngleTable+8*eax]
		      mulps  xmm0,dqword[.r]
		      addps  xmm0,dqword[esi+GEOM.px]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,1.0,0.0
			sub  esp,4*2
		      movzx  eax,[esi+GEOM.gamma]
			add  al,0x040
		      movsd  xmm0,qword[AngleTable+8*eax]
		      mulps  xmm0,dqword[.r]
		      addps  xmm0,dqword[esi+GEOM.px]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,1.0,1.0
			sub  esp,4*2
		      movzx  eax,[esi+GEOM.gamma]
			add  al,0x080
		      movsd  xmm0,qword[AngleTable+8*eax]
		      mulps  xmm0,dqword[.r]
		      addps  xmm0,dqword[esi+GEOM.px]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.0,1.0
			sub  esp,4*2
		      movzx  eax,[esi+GEOM.gamma]
			add  al,0x0C0
		      movsd  xmm0,qword[AngleTable+8*eax]
		      mulps  xmm0,dqword[.r]
		      addps  xmm0,dqword[esi+GEOM.px]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


	      .5:
			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


align  16
.r dd 32.0,32.0,16.0,16.0


