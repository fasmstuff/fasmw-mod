
struct WANDER
   px	   dd ?
   py	   dd ?
   vx	   dd ?
   vy	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   Dalpha  db ?
   Dbeta   db ?
   Dgamma  db ?
	   db ?

	   dd ?
	   dd ?
ends


struct WANDERSPAWN
   px	   dd ?
   py	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   ct	   dd ?

ends



		      align  16
Wander_Spawn:
			mov  esi,WanderSpawnTable
			mov  eax,dword[FrameCount]
		       test  eax,0xFF
			jnz  .ret
			mov  eax,dword[WanderSpawnCount]
			cmp  eax,WANDER_SPAWN_MAXCOUNT
			jae  .ret
			add  dword[WanderSpawnCount],1
			shl  eax,4
			add  esi,eax
		       call  GetRand.word
			mov  dword[esi+WANDERSPAWN.alpha],eax
			xor  edx,edx
			mov  ecx,PLAY_WIDTH
			div  ecx
		       push  edx
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_HEIGHT
			div  ecx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WANDERSPAWN.py],xmm0
			pop  edx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WANDERSPAWN.px],xmm0

			mov  dword[esi+WANDERSPAWN.ct],64
	    .ret:	ret

align 4
.a dd 0.00002
.b dd 3.0





		      align  16
WanderSpawn_Update:

			mov  ebx,[WanderSpawnCount]
			mov  esi,WanderSpawnTable
			mov  edi,esi
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+WANDERSPAWN.px]
			mov  eax,dword[esi+WANDERSPAWN.alpha]
			mov  edx,dword[esi+WANDERSPAWN.ct]
			sub  edx,1
			 js  .0
	      .4:     movsd  qword[edi+WANDERSPAWN.px],xmm0
			mov  dword[edi+WANDERSPAWN.alpha],eax
			mov  dword[edi+WANDERSPAWN.ct],edx
			add  edi,16
			jmp  .3
	      .0:	mov  ecx,dword[WanderCount]
			 or  edx,-1
		      xorps  xmm1,xmm1
			cmp  ecx,WANDER_MAXCOUNT
			jae  .4
			add  dword[WanderCount],1
			shl  ecx,5
			add  ecx,WanderTable
		      movsd  qword[ecx+WANDER.px],xmm0
		      movsd  qword[ecx+WANDER.vx],xmm1
			mov  dword[ecx+WANDER.alpha],eax
		       call  GetRand.word
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WANDER.Dalpha],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WANDER.Dbeta],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+WANDER.Dgamma],dl
	      .3:	add  esi,16
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,WanderSpawnTable
			shr  edi,4
			mov  [WanderSpawnCount],edi
			ret











		      align  16
Wander_Update:

			xor  esi,esi
			mov  eax,WanderTable
			jmp  .6
	     .5:	mov  edi,esi
			mov  ecx,esi
			shl  ecx,5
			add  ecx,WanderTable
	     .7:
		      movsd  xmm0,qword[eax+WANDER.px]
		      movsd  xmm1,qword[ecx+WANDER.px]
		      subps  xmm0,xmm1
		     movaps  xmm2,xmm0
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		     movaps  xmm1,xmm0
		    cmpltps  xmm1,dqword[.r]
		      andps  xmm1,xmm2
		      maxps  xmm0,dqword[.e]
		    rsqrtps  xmm0,xmm0
		      mulps  xmm0,xmm1

		      movsd  xmm1,qword[eax+WANDER.vx]
		      movsd  xmm2,qword[ecx+WANDER.vx]
		      addps  xmm1,xmm0
		      subps  xmm2,xmm0
		      movsd  qword[eax+WANDER.vx],xmm1
		      movsd  qword[ecx+WANDER.vx],xmm2

			add  edi,1
			add  ecx,32
			cmp  edi,[WanderCount]
			 jb  .7
			add  eax,32
	     .6:	add  esi,1
			cmp  esi,[WanderCount]
			 jb  .5


		       push  WANDER_HR
			mov  esi,WanderTable
			mov  edi,esi
			mov  ebx,[WanderCount]
			jmp  .2
	    .1:       movsd  xmm4,qword[esi+WANDER.px]
		      movsd  xmm5,qword[esi+WANDER.vx]
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		      addps  xmm4,xmm5
		      maxps  xmm4,xmm6
		      minps  xmm4,xmm7
		       call  CheckShotIntersection
			 jc  .ex
		      movsd  xmm0,qword[Player.px]
		      subps  xmm0,xmm4
		     movaps  xmm2,xmm0
		      mulps  xmm2,dqword[.s]
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		      maxss  xmm0,dword[.e]
		    rsqrtss  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		      mulps  xmm0,xmm2
		      mulps  xmm5,dqword[.a]
		      mulps  xmm0,dqword[.b]
		      addps  xmm5,xmm0
		       movd  xmm0,dword[esi+WANDER.alpha]
		       movd  xmm1,dword[esi+WANDER.Dalpha]
		      paddb  xmm0,xmm1
		      movsd  qword[edi+WANDER.px],xmm4
		      movsd  qword[edi+WANDER.vx],xmm5
		       movd  dword[edi+WANDER.alpha],xmm0
		       movd  dword[edi+WANDER.Dalpha],xmm1
			add  edi,32
			jmp  .0
	    .ex:
		       push  dword[WanderParColor]
		       push  WanderExpCount
		       push  WanderExpTable
		       push  WANDER_EXP_MAXCOUNT
		       push  WANDER_EXP_MINS
		       push  WANDER_EXP_DELTAS
		       push  WANDER_EXP_COUNT
		       push  WANDER_PAR_MINR
		       push  WANDER_PAR_DELTAR
		       push  WANDER_PAR_MINS
		       push  WANDER_PAR_DELTAS
		       push  WANDER_PAR_COUNT
		       call  ReleaseExplosion

		       call  ReleaseGeom
		       fild  qword[Score]
		       fild  qword[Multiplier]
		       push  WANDER_POINTS
		       fild  dword[esp]
		      fmulp  st1,st0
		      faddp  st1,st0
		      fistp  qword[Score]
			add  esp,4

	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,WanderTable
			shr  edi,5
			mov  [WanderCount],edi
			add  esp,4*1
			ret


align 16
.a: dd 0.9375,0.9375,0.9375,0.9375
.b: dd 0.0625,0.0625,0.0625,0.0625
.e: dd 1.00,1.00,1.00,1.00
.s: dd WANDER_SPEED,WANDER_SPEED,WANDER_SPEED,WANDER_SPEED
.r: dd 800.0,800.0,800.0,800.0














		      align  16
Wander_Draw:


		     invoke  glColor4fv,addr ColorTable2+16*WANDER_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WanderCount+CopyOffset]
			mov  esi,WanderTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+WANDER.alpha]
		      movzx  edx,[esi+WANDER.beta]
		      movzx  ecx,[esi+WANDER.gamma]
		       call  SetRotationMatrix

			mov  ebp,12
			mov  edi,WanderModel
	       .5:    movsd  xmm1,qword[esi+WANDER.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


		      align  16
WanderModel:
		 dd +20.0,+00.0,+00.0,0   ,   +00.0,+20.0,+00.0,0
		 dd +00.0,+20.0,+00.0,0   ,   -20.0,+00.0,+00.0,0
		 dd -20.0,+00.0,+00.0,0   ,   +00.0,-20.0,+00.0,0
		 dd +00.0,-20.0,+00.0,0   ,   +20.0,+00.0,+00.0,0

		 dd +20.0,+00.0,+00.0,0   ,   +00.0,+00.0,+20.0,0
		 dd +00.0,+20.0,+00.0,0   ,   +00.0,+00.0,+20.0,0
		 dd -20.0,+00.0,+00.0,0   ,   +00.0,+00.0,+20.0,0
		 dd +00.0,-20.0,+00.0,0   ,   +00.0,+00.0,+20.0,0

		 dd +20.0,+00.0,+00.0,0   ,   +00.0,+00.0,-20.0,0
		 dd +00.0,+20.0,+00.0,0   ,   +00.0,+00.0,-20.0,0
		 dd -20.0,+00.0,+00.0,0   ,   +00.0,+00.0,-20.0,0
		 dd +00.0,-20.0,+00.0,0   ,   +00.0,+00.0,-20.0,0



		      align  16
WanderSpawn_Draw:

		     invoke  glColor4fv,addr ColorTable2+16*WANDER_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WanderSpawnCount+CopyOffset]
			mov  esi,WanderSpawnTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+WANDERSPAWN.alpha]
		      movzx  edx,[esi+WANDERSPAWN.beta]
		      movzx  ecx,[esi+WANDERSPAWN.gamma]
		       call  SetRotationMatrix

			mov  eax,dword[esi+WANDERSPAWN.ct]
			and  eax,0x0F
		   cvtsi2ss  xmm0,eax
		      mulss  xmm0,dword[.a]
		      addss  xmm0,dword[.b]
		     shufps  xmm0,xmm0,0
		     movaps  xmm1,xmm0
		      mulps  xmm0,dqword[RotationMatrix.x]
		      mulps  xmm1,dqword[RotationMatrix.y]
		     movaps  dqword[RotationMatrix.x],xmm0
		     movaps  dqword[RotationMatrix.y],xmm1


			mov  ebp,12
			mov  edi,WanderModel
	       .5:    movsd  xmm1,qword[esi+WANDERSPAWN.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,16
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


align 4

.a:	dd 0.125
.b	dd 1.0







		      align  16
Wander_DestroyRadius:
			; xmm6 = center
			; xmm5 = radius

			mov  esi,WanderTable
			mov  edi,esi
			mov  ebx,dword[WanderCount]
			jmp  .2
	    .1:       movsd  xmm0,qword[esi+WANDER.px]
		      subps  xmm0,xmm6
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm5
		     movaps  xmm0,dqword[esi+16*0]
		     movaps  xmm1,dqword[esi+16*1]
			 jb  .ex
		     movaps  dqword[edi+16*0],xmm0
		     movaps  dqword[edi+16*1],xmm1
			add  edi,32
			jmp  .0
	    .ex:      movsd  xmm4,qword[esi+WANDER.px]
		      xorps  xmm7,xmm7
		       push  dword[WanderParColor]
		       push  WanderExpCount
		       push  WanderExpTable
		       push  WANDER_EXP_MAXCOUNT
		       push  WANDER_EXP_MINS
		       push  WANDER_EXP_DELTAS
		       push  WANDER_EXP_COUNT/2
		       push  WANDER_PAR_MINR
		       push  WANDER_PAR_DELTAR
		       push  WANDER_PAR_MINS
		       push  WANDER_PAR_DELTAS
		       push  WANDER_PAR_COUNT/2
		       call  ReleaseExplosion
		       call  ReleaseGeom
	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,WanderTable
			shr  edi,5
			mov  [WanderCount],edi
			ret























