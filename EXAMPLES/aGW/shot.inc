
		     align  16
ReleasePlayerShot:	;xmm3:xmm4 contains shot
			mov  edx,[ShotCount]
			cmp  edx,SHOT_MAXCOUNT
			jae  .ret
			lea  ecx,[edx+1]
			shl  edx,5
			add  edx,ShotTable
			mov  [ShotCount],ecx
		     movaps  dqword[edx+16*0],xmm3
		     movaps  dqword[edx+16*1],xmm4
      .ret:		ret





		      align  16
CheckShotIntersection:	; stack not cleaned
			; arg1: hr of object
			; xmm4: position of object (preserved)
			; xmm5,xmm5: (preserved)
			; out:
			; xmm7: normla
			;

		       push  esi edi ebx edx
			sub  esp,16
			xor  edx,edx
		     movups  [esp],xmm4

     _CheckShot:	mov  esi,ShotTable
			mov  edi,esi
			mov  ebx,dword[ShotCount]
			jmp  .2
	    .1:       movsd  xmm0,qword[esi+OBJ1.px]
		      movss  xmm1,dword[esi+OBJ1.hr]
		     movaps  xmm2,dqword[esi+16*0]
		     movaps  xmm3,dqword[esi+16*1]
		      addss  xmm1,[esp+16+4*5]
		      subps  xmm0,xmm4
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm1
			 jb  .ex
		     movaps  dqword[edi+16*0],xmm2
		     movaps  dqword[edi+16*1],xmm3
			add  edi,32
			jmp  .0
	    .ex:	 or  edx,-1
		     movaps  xmm4,xmm2
		      movsd  xmm7,qword[esi+OBJ1.nx]
		     movups  xmm4,[esp]
	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,ShotTable
			shr  edi,5
			mov  dword[ShotCount],edi


			add  esp,16
			add  edx,edx
			pop  edx ebx edi esi
			ret






		      align  16
Shot_Update:
			mov  ebx,[ShotCount]
			mov  esi,ShotTable
			mov  edi,esi
		      xorps  xmm6,xmm6
		      movsd  xmm7,[Corner]
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+OBJ1.px]
		      movsd  xmm1,qword[esi+OBJ1.nx]
		      movsd  xmm2,qword[esi+OBJ1.vx]
		     movaps  xmm4,dqword[esi+16*0]
		     movaps  xmm5,dqword[esi+16*1]
		      addps  xmm0,xmm2
		     movaps  xmm1,xmm0
		    cmpltps  xmm1,xmm6
		     movaps  xmm2,xmm0
		   cmpnltps  xmm2,xmm7
		       orps  xmm1,xmm2
		   movmskps  eax,xmm1
		      movsd  xmm4,xmm0
		       test  eax,3
			jnz  .ex
		     movaps  dqword[edi+16*0],xmm4
		     movaps  dqword[edi+16*1],xmm5
		      movsd  xmm0,xmm4
		       call  GetBlackholeForce_Obj
		      movsd  xmm0,qword[edi+OBJ1.vx]
		      subps  xmm0,xmm1
		      movsd  qword[edi+OBJ1.vx],xmm0
			add  edi,32
			jmp  .0
	      .ex:
		       push  dword[PlayerParColor]
		       push  0 0 0 0 0 0
		       push  SHOT_EXP_MINR
		       push  SHOT_EXP_DELTAR
		       push  SHOT_EXP_MINS
		       push  SHOT_EXP_DELTAS
		       push  SHOT_EXP_COUNT
		       call  ReleaseExplosion
	      .0:	add  esi,32
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,ShotTable
			shr  edi,5
			mov  [ShotCount],edi
			ret





		      align  16
Shot_Draw:
		     invoke  glColor4f,1.0,1.0,1.0,1.0
		     invoke  glBindTexture,GL_TEXTURE_2D,[ShotTexture]
		     invoke  glBegin,GL_QUADS
			mov  ebx,[ShotCount+CopyOffset]
			mov  esi,ShotTable+CopyOffset
			jmp  .4
	      .3:	lea  edi,[esi+8]
		       call  PrintOBJ1
			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


