



SNAKE_LINK_COUNT fix 96


struct SNAKE
   px	   dd ?
   py	   dd ?
   vx	   dd ?
   vy	   dd ?
	   rb 16*(SNAKE_LINK_COUNT-1)
ends




struct SNAKESPAWN
   px	   dd ?
   py	   dd ?
   angle   dd ?
   ct	   dd ?

ends



		      align  16
Snake_Spawn:
			mov  esi,SnakeSpawnTable
			mov  eax,dword[FrameCount]
		       test  eax,0xFF
			jnz  .ret
			mov  eax,dword[SnakeSpawnCount]
			cmp  eax,SNAKE_SPAWN_MAXCOUNT
			jae  .ret
			add  dword[SnakeSpawnCount],1
			shl  eax,4
			add  esi,eax
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_WIDTH
			div  ecx
		       push  edx
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_HEIGHT
			div  ecx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+SNAKESPAWN.py],xmm0
			pop  edx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+SNAKESPAWN.px],xmm0
		       call  GetRand.word
			and  eax,0x0FF
			mov  dword[esi+SNAKESPAWN.angle],eax
			mov  dword[esi+SNAKESPAWN.ct],64
	    .ret:	ret

align 4
.a dd 0.00002
.b dd 3.0





		      align  16
SnakeSpawn_Update:
			mov  ebx,[SnakeSpawnCount]
			mov  esi,SnakeSpawnTable
			mov  edi,esi
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+SNAKESPAWN.px]
			mov  eax,dword[esi+SNAKESPAWN.angle]
			mov  edx,dword[esi+SNAKESPAWN.ct]
			and  eax,0x0FF
			sub  edx,1
			 js  .0
	      .4:     movsd  qword[edi+SNAKESPAWN.px],xmm0
			mov  dword[edi+SNAKESPAWN.angle],eax
			mov  dword[edi+SNAKESPAWN.ct],edx
			add  edi,16
			jmp  .3
	      .0:	mov  ecx,dword[SnakeCount]
			 or  edx,-1
			cmp  ecx,SNAKE_MAXCOUNT
			jae  .4
			add  dword[SnakeCount],1
		       imul  ecx,sizeof.SNAKE
			add  ecx,SnakeTable
		     movhpd  xmm0,[AngleTable+8*eax]
		     movaps  dqword[ecx+SNAKE.px],xmm0

	    rept (SNAKE_LINK_COUNT-2) i {
		     movaps  dqword[ecx+16*i],xmm0	 }

			mov  dword[ecx+16*(SNAKE_LINK_COUNT-1)+4*0],eax
			mov  dword[ecx+16*(SNAKE_LINK_COUNT-1)+4*1],2
			mov  dword[ecx+16*(SNAKE_LINK_COUNT-1)+4*2],64
			mov  dword[ecx+16*(SNAKE_LINK_COUNT-1)+4*3],0

	      .3:	add  esi,16
	      .2:	sub  ebx,1
			jns  .1


			sub  edi,SnakeSpawnTable
			shr  edi,4
			mov  [SnakeSpawnCount],edi
			ret



align 16
.s dd SNAKE_SPEED,SNAKE_SPEED,SNAKE_SPEED,SNAKE_SPEED






		      align  16
Snake_Update:
		       push  SNAKE_HR
			mov  esi,SnakeTable
			mov  edi,esi
			mov  ebx,[SnakeCount]
			jmp  .2
	    .1:

		       push  ebx
		       push  SNAKE_HR
			mov  ebx,16*4
	      .7:     movsd  xmm4,[esi+ebx]
			mov  eax,dword[esi+ebx]
			 or  eax,dword[esi+ebx]
			 jz  .8
		       call  CheckShotIntersection
			jnc  .8
		       push  dword[SnakeParColor]
		       push  0 0 0 0 0 0
		       push  SHOT_EXP_MINR
		       push  SHOT_EXP_DELTAR
		       push  SHOT_EXP_MINS
		       push  SHOT_EXP_DELTAS
		       push  SHOT_EXP_COUNT
		       call  ReleaseExplosion
		.8:	add  ebx,16*4
			cmp  ebx,sizeof.SNAKE-16
			 jb  .7
			pop  eax
			pop  ebx

			mov  eax,dword[esi+SNAKE.vx]
			 or  eax,dword[esi+SNAKE.vy]
			 jz  .dead

		      movsd  xmm4,qword[esi+SNAKE.px]
		      movsd  xmm0,qword[esi+SNAKE.vx]
		      mulps  xmm0,dqword[.s]
		      addps  xmm4,xmm0
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		      maxps  xmm4,xmm6
		      minps  xmm4,xmm7

		       call  CheckShotIntersection
			 jc  .ex

			mov  edx,dword[esi+16*(SNAKE_LINK_COUNT-1)+4*1]
			mov  eax,dword[esi+16*(SNAKE_LINK_COUNT-1)+4*0]
			add  eax,edx
			and  eax,0x0FF
			mov  dword[esi+16*(SNAKE_LINK_COUNT-1)+4*0],eax
		      movsd  xmm5,qword[AngleTable+8*eax]
			sub  dword[esi+16*(SNAKE_LINK_COUNT-1)+4*2],1
			jns  .10
		       call  GetRand.word
			and  eax,0x00F
			add  eax,0x03F
			mov  dword[esi+16*(SNAKE_LINK_COUNT-1)+4*2],eax
			and  eax,0x03
			add  eax,1
		       test  edx,edx
			 js  @f
			neg  eax
		    @@: mov  dword[esi+16*(SNAKE_LINK_COUNT-1)+4*1],eax
		  .10:



		  rept (SNAKE_LINK_COUNT-2) i {
		     movaps  xmm0,dqword[esi+16*(SNAKE_LINK_COUNT-2-i)]
		     movaps  dqword[edi+16*(SNAKE_LINK_COUNT-1-i)],xmm0       }

		     movaps  xmm0,dqword[esi+16*(SNAKE_LINK_COUNT-1)]
		     movaps  dqword[edi+16*(SNAKE_LINK_COUNT-1)],xmm0

		      movsd  qword[edi+SNAKE.px],xmm4
		      movsd  qword[edi+SNAKE.vx],xmm5

			add  edi,sizeof.SNAKE
			jmp  .0
	    .ex:
		       push  dword[SnakeParColor]
		       push  SnakeHeadExpCount
		       push  SnakeHeadExpTable
		       push  SNAKEHEAD_EXP_MAXCOUNT
		       push  SNAKE_EXP_MINS
		       push  SNAKE_EXP_DELTAS
		       push  SNAKE_EXP_COUNT
		       push  SNAKE_PAR_MINR
		       push  SNAKE_PAR_DELTAR
		       push  SNAKE_PAR_MINS
		       push  SNAKE_PAR_DELTAS
		       push  SNAKE_PAR_COUNT
		       call  ReleaseExplosion

		       call  ReleaseGeom
		       fild  qword[Score]
		       fild  qword[Multiplier]
		       push  SNAKE_POINTS
		       fild  dword[esp]
		      fmulp  st1,st0
		      faddp  st1,st0
		      fistp  qword[Score]
			add  esp,4

			mov  dword[esi+8],0
			mov  dword[esi+12],0
			jmp  .copy

	    .dead:
			mov  ecx,SNAKE_LINK_COUNT-1
			mov  edx,esi
		   @@:	mov  eax,[edx+8]
			 or  eax,[edx+12]
			jnz  .found
			add  edx,16
			sub  ecx,1
			jnz  @b
			jmp  .0
	      .found:
		      movsd  xmm4,[edx+0]
		      xorps  xmm7,xmm7
			mov  dword[edx+8],0
			mov  dword[edx+12],0
			mov  dword[edx+16*1+8],0
			mov  dword[edx+16*1+12],0

		       push  dword[SnakeParColor]
		       push  SnakeTailExpCount
		       push  SnakeTailExpTable
		       push  SNAKETAIL_EXP_MAXCOUNT
		       push  SNAKETAIL_EXP_MINS
		       push  SNAKETAIL_EXP_DELTAS
		       push  SNAKETAIL_EXP_COUNT
		       push  SNAKETAIL_PAR_MINR
		       push  SNAKETAIL_PAR_DELTAR
		       push  SNAKETAIL_PAR_MINS
		       push  SNAKETAIL_PAR_DELTAS
		       push  SNAKETAIL_PAR_COUNT
		       call  ReleaseExplosion

	      .copy:
		      rept (SNAKE_LINK_COUNT) i {
		     movaps  xmm0,dqword[esi+16*(i-1)]
		     movaps  dqword[edi+16*(i-1)],xmm0	     }

			add  edi,sizeof.SNAKE

	    .0: 	add  esi,sizeof.SNAKE
	    .2: 	sub  ebx,1
			jns  .1
			lea  eax,[edi-SnakeTable]
			xor  edx,edx
			mov  ecx,sizeof.SNAKE
			div  ecx
			mov  [SnakeCount],eax
			add  esp,4*1
			ret


align 16
.a: dd 0.9375,0.9375,0.9375,0.9375
.b: dd 0.0625,0.0625,0.0625,0.0625
.e: dd 1.00,1.00,1.00,1.00
.s: dd SNAKE_SPEED,SNAKE_SPEED,SNAKE_SPEED,SNAKE_SPEED
.r: dd 800.0,800.0,800.0,800.0



		      align  16
SnakeDead_Update:
			mov  esi,SnakeTable
			mov  edi,esi
			mov  ebx,[SnakeCount]
			jmp  .2
	    .1:
		       push  ebx
		       push  SNAKE_HR
			mov  ebx,16*4
	      .7:     movsd  xmm4,[esi+ebx]
			mov  eax,dword[esi+ebx]
			 or  eax,dword[esi+ebx]
			 jz  .8
		       call  CheckShotIntersection
			jnc  .8
		       push  dword[SnakeParColor]
		       push  0 0 0 0 0 0
		       push  SHOT_EXP_MINR
		       push  SHOT_EXP_DELTAR
		       push  SHOT_EXP_MINS
		       push  SHOT_EXP_DELTAS
		       push  SHOT_EXP_COUNT
		       call  ReleaseExplosion
		.8:	add  ebx,16*4
			cmp  ebx,sizeof.SNAKE-16
			 jb  .7
			pop  eax
			pop  ebx

			mov  eax,dword[esi+SNAKE.vx]
			 or  eax,dword[esi+SNAKE.vy]
			 jz  .dead
			jmp  .copy
	    .ex:
		       push  dword[SnakeParColor]
		       push  SnakeHeadExpCount
		       push  SnakeHeadExpTable
		       push  SNAKEHEAD_EXP_MAXCOUNT
		       push  SNAKE_EXP_MINS
		       push  SNAKE_EXP_DELTAS
		       push  SNAKE_EXP_COUNT
		       push  SNAKE_PAR_MINR
		       push  SNAKE_PAR_DELTAR
		       push  SNAKE_PAR_MINS
		       push  SNAKE_PAR_DELTAS
		       push  SNAKE_PAR_COUNT
		       call  ReleaseExplosion

		       call  ReleaseGeom
		       fild  qword[Score]
		       fild  qword[Multiplier]
		       push  SNAKE_POINTS
		       fild  dword[esp]
		      fmulp  st1,st0
		      faddp  st1,st0
		      fistp  qword[Score]
			add  esp,4

			mov  dword[esi+8],0
			mov  dword[esi+12],0
			jmp  .copy

	    .dead:
			mov  ecx,SNAKE_LINK_COUNT-1
			mov  edx,esi
		   @@:	mov  eax,[edx+8]
			 or  eax,[edx+12]
			jnz  .found
			add  edx,16
			sub  ecx,1
			jnz  @b
			jmp  .0
	      .found:
		      movsd  xmm4,[edx+0]
		      xorps  xmm7,xmm7
			mov  dword[edx+8],0
			mov  dword[edx+12],0

		       push  dword[SnakeParColor]
		       push  SnakeTailExpCount
		       push  SnakeTailExpTable
		       push  SNAKETAIL_EXP_MAXCOUNT
		       push  SNAKETAIL_EXP_MINS
		       push  SNAKETAIL_EXP_DELTAS
		       push  SNAKETAIL_EXP_COUNT
		       push  SNAKETAIL_PAR_MINR
		       push  SNAKETAIL_PAR_DELTAR
		       push  SNAKETAIL_PAR_MINS
		       push  SNAKETAIL_PAR_DELTAS
		       push  SNAKETAIL_PAR_COUNT
		       call  ReleaseExplosion

	      .copy:
		      rept (SNAKE_LINK_COUNT) i {
		     movaps  xmm0,dqword[esi+16*(i-1)]
		     movaps  dqword[edi+16*(i-1)],xmm0	     }

			add  edi,sizeof.SNAKE

	    .0: 	add  esi,sizeof.SNAKE
	    .2: 	sub  ebx,1
			jns  .1
			lea  eax,[edi-SnakeTable]
			xor  edx,edx
			mov  ecx,sizeof.SNAKE
			div  ecx
			mov  [SnakeCount],eax
			ret


align 16
.a: dd 0.9375,0.9375,0.9375,0.9375
.b: dd 0.0625,0.0625,0.0625,0.0625
.e: dd 1.00,1.00,1.00,1.00
.s: dd SNAKE_SPEED,SNAKE_SPEED,SNAKE_SPEED,SNAKE_SPEED
.r: dd 800.0,800.0,800.0,800.0












		      align  16
Snake_Draw:

			mov  ebx,[SnakeCount+CopyOffset]
			mov  esi,SnakeTable+CopyOffset
			jmp  .4
	      .3:
			xor  eax,eax
			xor  ecx,ecx
			xor  edx,edx
		       call  SetRotationMatrix

		     invoke  glColor4fv,addr ColorTable2+16*SNAKE_COLOR
		     invoke  glBegin,GL_QUADS
			mov  eax,dword[esi+SNAKE.vx]
			 or  eax,dword[esi+SNAKE.vy]
			 jz  .8
			mov  ebp,11
			mov  edi,SnakeHeadModel
	       .5:    movsd  xmm1,qword[esi+SNAKE.px]
		      movsd  xmm2,qword[esi+SNAKE.vx]
		      movsd  xmm3,qword[esi+SNAKE.vx]
		     shufps  xmm3,xmm3,011110001b
		      xorps  xmm3,dqword[normalmaker]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5
	      .8:


		     invoke  glColor4fv,addr ColorTable2+16*SNAKETAIL_COLOR


		       push  ebx

			mov  ebx,16*4
	       .7:	mov  ebp,5
			mov  edi,SnakeTailModel

			mov  eax,dword[esi+ebx+SNAKE.vx]
			 or  eax,dword[esi+ebx+SNAKE.vy]
			 jz  .9

	       .6:    movsd  xmm1,qword[esi+ebx]
		      movsd  xmm2,qword[esi+ebx+8]
		      movsd  xmm3,qword[esi+ebx+8]
		     shufps  xmm3,xmm3,011110001b
		      xorps  xmm3,dqword[normalmaker]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .6

	      .9:	add  ebx,16*4
			cmp  ebx,sizeof.SNAKE-16
			 jb  .7

			pop  ebx
		     invoke  glEnd

			add  esi,sizeof.SNAKE
	      .4:	sub  ebx,1
			jns  .3
			ret


		      align  16
SnakeHeadModel:
		 dd +00.0,-10.0,0,0 , +00.0,+15.0,0,0

		 dd +00.0,+15.0,0,0 , +07.8,+10.2,0,0
		 dd +07.8,+10.2,0,0 , +14.0,+02.0,0,0
		 dd +14.0,+02.0,0,0 , +14.0,-06.4,0,0
		 dd +14.0,-06.4,0,0 , +08.2,-13.4,0,0
		 dd +08.2,-13.4,0,0 , +00.0,-10.0,0,0

		 dd -00.0,+15.0,0,0 , -07.8,+10.2,0,0
		 dd -07.8,+10.2,0,0 , -14.0,+02.0,0,0
		 dd -14.0,+02.0,0,0 , -14.0,-06.4,0,0
		 dd -14.0,-06.4,0,0 , -08.2,-13.4,0,0
		 dd -08.2,-13.4,0,0 , -00.0,-10.0,0,0

		      align  16
SnakeTailModel:
		 dd +00.0,+15.0,+00.0,0   ,   +00.0,-15.0,+00.0,0
		 dd +00.0,+15.0,+00.0,0   ,   +10.0,+00.0,+00.0,0
		 dd +00.0,-15.0,+00.0,0   ,   +10.0,+00.0,+00.0,0
		 dd +00.0,+15.0,+00.0,0   ,   -10.0,+00.0,+00.0,0
		 dd +00.0,-15.0,+00.0,0   ,   -10.0,+00.0,+00.0,0

		      align  16
SnakeSpawn_Draw:

		invoke	glColor4f,1.0,1.0,1.0,1.0
			mov  ebx,[SnakeSpawnCount+CopyOffset]
			mov  esi,SnakeSpawnTable+CopyOffset
			jmp  .4
	      .3:

		     invoke  glColor4fv,addr ColorTable2+16*SNAKE_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebp,11
			mov  edi,SnakeHeadModel
	       .5:    movsd  xmm1,qword[esi+SNAKESPAWN.px]
		      movzx  eax,byte[esi+SNAKESPAWN.angle]
		      movsd  xmm2,qword[AngleTable+8*eax]
		      movsd  xmm3,qword[AngleTable+8*eax]
		     shufps  xmm3,xmm3,011110001b
		      xorps  xmm3,dqword[normalmaker]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm7,xmm0


			mov  eax,dword[esi+SNAKESPAWN.ct]
			and  eax,0x0F
		   cvtsi2ss  xmm0,eax
		      mulss  xmm0,dword[.a]
		      addss  xmm0,dword[.b]
		     shufps  xmm0,xmm0,0
		      mulps  xmm6,xmm0
		      mulps  xmm7,xmm0


		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

		     invoke  glColor4fv,addr ColorTable2+16*SNAKETAIL_COLOR

			mov  ebp,5
			mov  edi,SnakeTailModel
	       .6:    movsd  xmm1,qword[esi+SNAKESPAWN.px]
		      movzx  eax,byte[esi+SNAKESPAWN.angle]
		      movsd  xmm2,qword[AngleTable+8*eax]
		      movsd  xmm3,qword[AngleTable+8*eax]
		     shufps  xmm3,xmm3,011110001b
		      xorps  xmm3,dqword[normalmaker]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,xmm3,00110001b
		       dpps  xmm0,xmm2,00110010b
		       orps  xmm7,xmm0

			mov  eax,dword[esi+SNAKESPAWN.ct]
			and  eax,0x0F
		   cvtsi2ss  xmm0,eax
		      mulss  xmm0,dword[.a]
		      addss  xmm0,dword[.b]
		     shufps  xmm0,xmm0,0
		      mulps  xmm6,xmm0
		      mulps  xmm7,xmm0

		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .6

		     invoke  glEnd

			add  esi,16
	      .4:	sub  ebx,1
			jns  .3
			ret


align 4

.a:	dd 0.125
.b	dd 1.0






		      align  16
Snake_DestroyRadius:
			; xmm6 = center
			; xmm5 = radius
			mov  esi,SnakeTable
			mov  ebx,dword[SnakeCount]
			jmp  .2
	    .1: 	mov  eax,dword[esi+SNAKE.vx]
			 or  eax,dword[esi+SNAKE.vy]
			 jz  .0
		      movsd  xmm0,qword[esi+SNAKE.px]
		      subps  xmm0,xmm6
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm5
			 ja  .0
		      movsd  xmm4,qword[esi+SNAKE.px]
		      xorps  xmm7,xmm7
			mov  dword[esi+SNAKE.vx],0
			mov  dword[esi+SNAKE.vy],0
		       push  dword[SnakeParColor]
		       push  SnakeHeadExpCount
		       push  SnakeHeadExpTable
		       push  SNAKEHEAD_EXP_MAXCOUNT
		       push  SNAKE_EXP_MINS
		       push  SNAKE_EXP_DELTAS
		       push  SNAKE_EXP_COUNT
		       push  SNAKE_PAR_MINR
		       push  SNAKE_PAR_DELTAR
		       push  SNAKE_PAR_MINS
		       push  SNAKE_PAR_DELTAS
		       push  SNAKE_PAR_COUNT
		       call  ReleaseExplosion
	    .0: 	add  esi,sizeof.SNAKE
	    .2: 	sub  ebx,1
			jns  .1
			ret



		      align  16
Snake_PlayerIntersection:
			; out: eax = 0 if no hit
			; otherwise eax = address of hit diamond


		       push  PLAYER_HR
		       push  SNAKE_HR
		      movss  xmm1,dword[esp+4*1]
		      addss  xmm1,dword[esp+4*0]
			add  esp,8
		      movsd  xmm2,qword[Player.px]

			xor  eax,eax
			mov  esi,SnakeTable
			mov  ebx,[SnakeCount]
			jmp  .2
	    .1:
			xor  edx,edx
	      .3:	mov  ecx,dword[esi+edx+8]
		      movsd  xmm0,qword[esi+edx]
		      subps  xmm0,xmm2
		      mulps  xmm0,xmm0
			 or  ecx,dword[esi+edx+12]
			 jz  .4
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm1
		      cmovb  eax,esi
		  .4:	add  edx,16*4
			cmp  edx,sizeof.SNAKE-16
			 jb  .3


			add  esi,sizeof.SNAKE
	    .2: 	sub  ebx,1
			jns  .1
			ret



