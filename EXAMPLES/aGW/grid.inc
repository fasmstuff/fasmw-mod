

GRID_SPACING	 fix 16
GRID_MDAMPING	 fix +0.950; +0.950
GRID_MK 	 fix -0.010; -0.004
GRID_SDAMPING	 fix +0.050; +0.150
GRID_SSTIFFNESS  fix -0.700; -0.450
GRID_EDAMPING	 fix +0.900; +0.900

GRID_COLOR_DAMPING  fix +0.95

struct GRID    ; must be 32 bytes
   px	dd ?	 ; position
   py	dd ?	 ;
   vx	dd ?	; velocity
   vy	dd ?	;
   fx	dd ?	 ; accel
   fy	dd ?	 ;
   Dx	dd ?	; damping
   Dy	dd ?	;
ends

		      align  16
Grid_Initialize:
		       push  GRID_MDAMPING
		       push  GRID_MDAMPING
		      xorps  xmm2,xmm2
		      movsd  xmm3,[esp]
			add  esp,8
			mov  ebx,GridTable
			xor  esi,esi
	      .2:	xor  edi,edi
	      .1:  cvtsi2ss  xmm0,edi
		   cvtsi2ss  xmm1,esi
		      movss  dword[ebx+GRID.px],xmm0
		      movss  dword[ebx+GRID.py],xmm1
		      movsd  qword[ebx+GRID.vx],xmm2
		      movsd  qword[ebx+GRID.fx],xmm2
		      movsd  qword[ebx+GRID.Dx],xmm3
			add  ebx,32
			add  edi,GRID_SPACING
			cmp  edi,PLAY_WIDTH
			jbe  .1
			add  esi,GRID_SPACING
			cmp  esi,PLAY_HEIGHT
			jbe  .2



			sub  esp,8
			mov  ebx,GridColorTable
			xor  esi,esi
	      .4:	xor  edi,edi
	      .3:
			mov  dword[esp+4*0],esi
			mov  dword[esp+4*1],edi
			fild  dword[esp+4*0]
		       fmul  dword[.a]
		       fsin
		       fmul  st0,st0
			fild  dword[esp+4*1]
		       fmul  dword[.b]
		       fsin
		       fmul  st0,st0
		       fstp  dword[ebx+4*0]
		       fstp  dword[ebx+4*1]
			mov  dword[ebx+4*2],0.0
			mov  dword[ebx+4*3],0.0
			mov  dword[ebx+4*2],0.0
			mov  dword[ebx+4*3],0.0

			add  ebx,16
			add  edi,GRID_SPACING
			cmp  edi,PLAY_WIDTH
			jbe  .3
			add  esi,GRID_SPACING
			cmp  esi,PLAY_HEIGHT
			jbe  .4

			add  esp,8
			ret


align 4
.a: dd 0.01
.b: dd 0.008







align  16
ReflectLight_Large:
		      ;  xmm4: position   ; -> (esi,edi)
		      ;  xmm5: radius
		      ;  xmm7: color

		       push  ebx esi edi

		     movaps  xmm0,xmm4
		     psrldq  xmm0,4
		   cvtss2si  ebx,xmm5
		   cvtss2si  esi,xmm4
		   cvtss2si  edi,xmm0
		      mulss  xmm5,xmm5
			shr  esi,4
			shr  edi,4
			shr  ebx,4
			sub  esi,ebx
			sub  edi,ebx
			add  ebx,ebx

			xor  edx,edx
	      .1:	xor  eax,eax
	      .2:      push  esi edi

			add  esi,eax
			 js  .skip
			add  edi,edx
			 js  .skip
			cmp  esi,PLAY_WIDTH/GRID_SPACING
			 ja  .skip
			cmp  edi,PLAY_HEIGHT/GRID_SPACING
			 ja  .skip

		       imul  edi,(PLAY_WIDTH/GRID_SPACING)+1
			add  edi,esi
			shl  edi,4
		      movsd  xmm0,qword[GridTable+2*edi+GRID.px]
		      subps  xmm0,xmm4
		     movaps  xmm1,xmm0
		      mulps  xmm1,xmm0
		     haddps  xmm1,xmm1
		     comiss  xmm1,xmm5
		     movaps  xmm0,dqword[GridColorTable+edi]
		      addps  xmm0,xmm7
		      mulps  xmm0,xmm6
			 ja  .skip
		     movaps  dqword[GridColorTable+edi],xmm0
		 .skip:
			pop  edi esi
			add  eax,1
			cmp  eax,ebx
			jbe  .2
			add  edx,1
			cmp  edx,ebx
			jbe  .1

			pop  edi esi ebx
			ret




ReflectLight:	      movss  xmm0,dword[esi+4*0]
		      movss  xmm1,dword[esi+4*1]
		   cvtss2si  eax,xmm0
		   cvtss2si  ecx,xmm1
			sar  eax,4
			sar  ecx,4
			xor  edx,edx
			cmp  eax,edx
		      cmovl  eax,edx
			cmp  ecx,edx
		      cmovl  ecx,edx
			mov  edx,(PLAY_WIDTH/GRID_SPACING)-1
			cmp  eax,edx
		      cmovg  eax,edx
			mov  edx,(PLAY_HEIGHT/GRID_SPACING)-1
			cmp  ecx,edx
		      cmovg  ecx,edx
		       imul  ecx,16*((PLAY_WIDTH/GRID_SPACING)+1)
		       imul  eax,16*(1)
			add  ecx,eax
		     movaps  xmm0,dqword[GridColorTable+ecx+16*(0)]
		     movaps  xmm1,dqword[GridColorTable+ecx+16*(1)]
		     movaps  xmm2,dqword[GridColorTable+ecx+16*(0+(PLAY_WIDTH/GRID_SPACING)+1)]
		     movaps  xmm3,dqword[GridColorTable+ecx+16*(1+(PLAY_WIDTH/GRID_SPACING)+1)]
		      addps  xmm0,xmm7
		      addps  xmm1,xmm7
		      addps  xmm2,xmm7
		      addps  xmm3,xmm7
		      mulps  xmm0,xmm6
		      mulps  xmm1,xmm6
		      mulps  xmm2,xmm6
		      mulps  xmm3,xmm6
		     movaps  dqword[GridColorTable+ecx+16*(0)],xmm0
		     movaps  dqword[GridColorTable+ecx+16*(1)],xmm1
		     movaps  dqword[GridColorTable+ecx+16*(0+(PLAY_WIDTH/GRID_SPACING)+1)],xmm2
		     movaps  dqword[GridColorTable+ecx+16*(1+(PLAY_WIDTH/GRID_SPACING)+1)],xmm3
			ret


GridBackgroundColors:
		       push  GRID_COLOR_DAMPING
		       push  GRID_COLOR_DAMPING
		       push  GRID_COLOR_DAMPING
		       push  GRID_COLOR_DAMPING
		     movups  xmm1,[esp]
			add  esp,16

			mov  esi,GridColorTable
			mov  ebx,((PLAY_WIDTH/GRID_SPACING)+1)*((PLAY_HEIGHT/GRID_SPACING)+1)
	    .0:      movaps  xmm0,[esi]
		      mulps  xmm0,xmm1
		     movaps  [esi],xmm0
			add  esi,16
			sub  ebx,1
			jnz  .0



GridBackgroundColors_ReflectPar:

			mov  eax,0.55
		       movd  xmm6,eax
		     shufps  xmm6,xmm6,0

			mov  esi,ParTable+CopyOffset
			mov  ebx,dword[ParCount+CopyOffset]
			jmp  .2
	    .1: 	mov  eax,dword[esi+PAR.col]
		     movaps  xmm7,dqword[ParColorTable+eax]
		       call  ReflectLight
			add  esi,32
	    .2: 	sub  ebx,1
			jns  .1

			mov  esi,WindmillTable
			mov  ebx,dword[WindmillCount]
			jmp  .4
	    .3:      movaps  xmm7,dqword[ColorTable3+16*WINDMILL_COLOR]
		       call  ReflectLight
			add  esi,32
	    .4: 	sub  ebx,1
			jns  .3

			mov  esi,DiamondTable
			mov  ebx,dword[DiamondCount]
			jmp  .6
	    .5:      movaps  xmm7,dqword[ColorTable3+16*DIAMOND_COLOR]
		       call  ReflectLight
			add  esi,32
	    .6: 	sub  ebx,1
			jns  .5

			mov  esi,WeaverTable
			mov  ebx,dword[WeaverCount]
			jmp  .8
	    .7:      movaps  xmm7,dqword[ColorTable3+16*WEAVER_COLOR]
		       call  ReflectLight
			add  esi,32
	    .8: 	sub  ebx,1
			jns  .7

			mov  esi,SnakeTable
			mov  ebx,dword[SnakeCount]
			jmp  .10
	    .9:
			lea  eax,[esi+sizeof.SNAKE-16]
		       push  esi
		       push  eax
		     movaps  xmm7,dqword[ColorTable3+16*SNAKE_COLOR]
		       call  ReflectLight
		   @@:	add  esi,4*16
		     movaps  xmm7,dqword[ColorTable3+16*SNAKETAIL_COLOR]
		       call  ReflectLight
			cmp  esi,[esp]
			 jb  @b
			pop  eax
			pop  esi
			add  esi,sizeof.SNAKE
	    .10:	sub  ebx,1
			jns  .9


			mov  esi,BlackholeTable
			mov  ebx,dword[BlackholeCount]
			jmp  .12
	    .11:      movsd  xmm4,qword[esi+BLACKHOLE.px]
			mov  eax,400.0
		       movd  xmm5,eax
			mov  eax,[FrameCount]
			add  eax,dword[esi+BLACKHOLE.sp]
			shr  eax,4
			and  eax,64-1
			shl  eax,4
		     movaps  xmm7,dqword[ColorTable3+eax]
			cmp  dword[esi+BLACKHOLE.hth],BLACKHOLE_MAXHEALTH
			jge  @f
		       call  ReflectLight_Large
		   @@:	add  esi,32
	    .12:	sub  ebx,1
			jns  .11



GridBackgroundColors_Blend_Ver:


			mov  esi,GridColorTable
			sub  esp,16*(((PLAY_HEIGHT/GRID_SPACING))+1)

	  .0:
			xor  ebx,ebx
	  .2:
		      xorps  xmm0,xmm0
			mov  edx,-4*16
	  .1:	     movaps  xmm2,dqword[Weights+edx]
		       imul  ecx,ebx,(((PLAY_WIDTH/GRID_SPACING))+1)
		     movaps  xmm1,dqword[esi+ecx]
			lea  eax,[edx]
			add  eax,ebx
			 js  @f
			cmp  eax,16*((PLAY_HEIGHT/GRID_SPACING))
			 ja  @f
		       imul  ecx,eax,(((PLAY_WIDTH/GRID_SPACING))+1)
		     movaps  xmm1,dqword[esi+ecx]
		 @@:  mulps  xmm1,xmm2
		      addps  xmm0,xmm1
			add  edx,16
			cmp  edx,+4*16
			jle  .1
		     movups  dqword[esp+ebx],xmm0
			add  ebx,16
			cmp  ebx,16*((PLAY_HEIGHT/GRID_SPACING))
			jbe  .2

			xor  ebx,ebx
	     @@:     movups  xmm0,[esp+ebx]
		       imul  ecx,ebx,(((PLAY_WIDTH/GRID_SPACING))+1)
		     movaps  [esi+ecx],xmm0
			add  ebx,16
			cmp  ebx,16*((PLAY_HEIGHT/GRID_SPACING))
			jbe  @b

			add  esi,16
			cmp  esi,GridColorTable+16*(((PLAY_WIDTH/GRID_SPACING))+1)
			 jb  .0


			add  esp,16*(((PLAY_HEIGHT/GRID_SPACING))+1)



GridBackgroundColors_Blend_Hor:



			mov  esi,GridColorTable+0*16*((PLAY_WIDTH/GRID_SPACING)+1)

			sub  esp,16*((PLAY_WIDTH/GRID_SPACING)+2)

	  .0:
			xor  ebx,ebx
	  .2:
		      xorps  xmm0,xmm0
			mov  edx,-4*16
	  .1:	     movaps  xmm2,dqword[Weights+edx]
		     movaps  xmm1,dqword[esi+ebx]
			lea  eax,[edx]
			add  eax,ebx
			 js  @f
			cmp  eax,16*(PLAY_WIDTH/GRID_SPACING)
			 ja  @f
		     movaps  xmm1,dqword[esi+eax]
		 @@:  mulps  xmm1,xmm2
		      addps  xmm0,xmm1
			add  edx,16
			cmp  edx,+4*16
			jle  .1
		     movups  dqword[esp+ebx],xmm0
			add  ebx,16
			cmp  ebx,16*(PLAY_WIDTH/GRID_SPACING)
			jbe  .2

			xor  ebx,ebx
	     @@:     movups  xmm0,[esp+ebx]
		     movaps  [esi+ebx],xmm0
			add  ebx,16
			cmp  ebx,16*(PLAY_WIDTH/GRID_SPACING)
			jbe  @b

			add  esi,16*((PLAY_WIDTH/GRID_SPACING)+1)

			cmp  esi,GridColorTable+16*((PLAY_WIDTH/GRID_SPACING)+1)*((PLAY_HEIGHT/GRID_SPACING)+1)
			 jb  .0


			add  esp,16*((PLAY_WIDTH/GRID_SPACING)+2)


			ret




dist0 fix 0.26825538
dist1 fix 0.20692388
dist2 fix 0.10659610
dist3 fix 0.04036746
dist4 fix 0.01198487

dist5 fix 0.03624511
dist6 fix 0.02120962
dist7 fix 0.01139009
dist8 fix 0.00563723


align 16
.f1: dd 0.25,0.25,0.25,0.25
.f2: dd 0.50,0.50,0.50,0.50
.f3: dd 0.75,0.75,0.75,0.75

dd dist8,dist8,dist8,dist8
dd dist7,dist7,dist7,dist7
dd dist6,dist6,dist6,dist6
dd dist5,dist5,dist5,dist5
dd dist4,dist4,dist4,dist4
dd dist3,dist3,dist3,dist3
dd dist2,dist2,dist2,dist2
dd dist1,dist1,dist1,dist1
Weights:
dd dist0,dist0,dist0,dist0
dd dist1,dist1,dist1,dist1
dd dist2,dist2,dist2,dist2
dd dist3,dist3,dist3,dist3
dd dist4,dist4,dist4,dist4
dd dist5,dist5,dist5,dist5
dd dist6,dist6,dist6,dist6
dd dist7,dist7,dist7,dist7
dd dist8,dist8,dist8,dist8


align 16
A: dd 0.5,0.5,0.5,0.5
B: dd 0.5,0.5,0.5,0.5


		      align  16
GridBackground_Draw:

			sub  esp,16
			mov  ebp,esp


		     invoke  glBindTexture,GL_TEXTURE_2D,[BorderTexture]


		     invoke  glBegin,GL_QUADS

; top
			mov  esi,GridColorTable
			xor  edi,edi
			mov  dword[ebp+4*1],0.0
			mov  dword[ebp+4*3],-32.0
	   @@:	   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*0],xmm0
		       movd  dword[ebp+4*2],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,0.00,0.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,1.00,0.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
			add  edi,32
			add  esi,32
		   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*0],xmm0
		       movd  dword[ebp+4*2],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,1.00,1.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,0.00,1.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
			cmp  edi,16*(PLAY_WIDTH/GRID_SPACING)
			 jb  @b


;bottom
			mov  esi,GridColorTable+16*(PLAY_HEIGHT/GRID_SPACING)*((PLAY_WIDTH/GRID_SPACING)+1)
			xor  edi,edi
			mov  eax,PLAY_HEIGHT+32
		   cvtsi2ss  xmm0,eax
		      movss  dword[ebp+4*1],xmm0
			mov  eax,PLAY_HEIGHT
		   cvtsi2ss  xmm0,eax
		      movss  dword[ebp+4*3],xmm0
	   @@:	   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*0],xmm0
		       movd  dword[ebp+4*2],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,0.00,0.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,1.00,0.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
			add  edi,32
			add  esi,32
		   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*0],xmm0
		       movd  dword[ebp+4*2],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,1.00,1.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,0.00,1.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
			cmp  edi,16*(PLAY_WIDTH/GRID_SPACING)
			 jb  @b



; left
			mov  esi,GridColorTable
			xor  edi,edi
			mov  dword[ebp+4*0],0.0
			mov  dword[ebp+4*2],-32.0
	   @@:	   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*1],xmm0
		       movd  dword[ebp+4*3],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,0.00,0.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,1.00,0.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
			add  edi,32
			add  esi,32*((PLAY_WIDTH/GRID_SPACING)+1)
		   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*1],xmm0
		       movd  dword[ebp+4*3],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,1.00,1.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,0.00,1.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
			cmp  edi,16*(PLAY_HEIGHT/GRID_SPACING)
			 jb  @b


;right
			mov  esi,GridColorTable+16*(PLAY_WIDTH/GRID_SPACING)
			xor  edi,edi
			mov  eax,PLAY_WIDTH+32
		   cvtsi2ss  xmm0,eax
		      movss  dword[ebp+4*0],xmm0
			mov  eax,PLAY_WIDTH
		   cvtsi2ss  xmm0,eax
		      movss  dword[ebp+4*2],xmm0
	   @@:	   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*1],xmm0
		       movd  dword[ebp+4*3],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,0.00,0.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,1.00,0.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
			add  edi,32
			add  esi,32*((PLAY_WIDTH/GRID_SPACING)+1)
		   cvtsi2ss  xmm0,edi
		       movd  dword[ebp+4*1],xmm0
		       movd  dword[ebp+4*3],xmm0
			sub  esp,16
		     movaps  xmm0,[esi]
		      addps  xmm0,[A]
		      mulps  xmm0,[B]
		     movups  [esp],xmm0
		     invoke  glColor4f
		     invoke  glTexCoord2f,1.00,1.00
			lea  eax,[ebp+4*2]
		     invoke  glVertex2fv,eax
		     invoke  glTexCoord2f,0.00,1.00
			lea  eax,[ebp+4*0]
		     invoke  glVertex2fv,eax
			cmp  edi,16*(PLAY_HEIGHT/GRID_SPACING)
			 jb  @b



		     invoke  glEnd

			add  esp,16
			ret







Grid_Draw:
			sub  esp,16

		       push  8.0


		     invoke  glBindTexture,GL_TEXTURE_2D,[GridTexture]

		     invoke  glBegin,GL_QUADS

;jmp  .9


			xor  esi,esi
	      .2:
			mov  eax,esi
			and  eax,7
			shl  eax,4
		     movaps  xmm7,[GridColors+eax]


		     movups  dqword[esp+4],xmm7



		       imul  ebx,esi,32*((PLAY_WIDTH/GRID_SPACING)+1)
			add  ebx,GridTable+CopyOffset
			xor  edi,edi
	      .1:

		     movups  xmm7,dqword[esp+4]
			lea  eax,[ebx-(GridTable+CopyOffset)]
			shr  eax,1
			sub  esp,16
		     movaps  xmm0,dqword[eax+GridColorTable+CopyOffset]
		      mulps  xmm0,xmm7
		     movups  dqword[esp],xmm0
		     invoke  glColor4f

		     invoke  glTexCoord2f,0.0,0.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*0+GRID.px]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*0+GRID.py]
		       fsub  dword[esp+4*3]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f

		     invoke  glTexCoord2f,1.0,0.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*0+GRID.px]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*0+GRID.py]
		       fadd  dword[esp+4*3]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f


		     movups  xmm7,dqword[esp+4]
			lea  eax,[ebx+32*1-(GridTable+CopyOffset)]
			shr  eax,1
			sub  esp,16
		     movaps  xmm0,dqword[eax+GridColorTable+CopyOffset]
		      mulps  xmm0,xmm7
		     movups  dqword[esp],xmm0
		     invoke  glColor4f


		     invoke  glTexCoord2f,1.0,1.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*1+GRID.px]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*1+GRID.py]
		       fadd  dword[esp+4*3]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f

		     invoke  glTexCoord2f,0.0,1.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*1+GRID.px]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*1+GRID.py]
		       fsub  dword[esp+4*3]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f
			add  ebx,32
			add  edi,1
			cmp  edi,PLAY_WIDTH/GRID_SPACING
			 jb   .1
		.1s:
			add  esi,1
			cmp  esi,PLAY_HEIGHT/GRID_SPACING
			jbe  .2

;jmp  .10

.9:


			xor  edi,edi
	      .4:
			mov  eax,edi
			and  eax,7
			shl  eax,4
		     movaps  xmm7,[GridColors+eax]

		     movups  dqword[esp+4],xmm7


		       imul  ebx,edi,32
			add  ebx,GridTable+CopyOffset
			xor  esi,esi
	      .3:


		     movups  xmm7,dqword[esp+4]
			lea  eax,[ebx-(GridTable+CopyOffset)]
			shr  eax,1
			sub  esp,16
		     movaps  xmm0,dqword[eax+GridColorTable+CopyOffset]
		      mulps  xmm0,xmm7
		     movups  dqword[esp],xmm0
		     invoke  glColor4f

		     invoke  glTexCoord2f,0.0,0.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*0*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.px]
		       fsub  dword[esp+4*3]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*0*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.py]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f

		     invoke  glTexCoord2f,1.0,0.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*0*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.px]
		       fadd  dword[esp+4*3]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*0*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.py]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f

		     movups  xmm7,dqword[esp+4]
			lea  eax,[ebx+32*1*((PLAY_WIDTH/GRID_SPACING)+1)-(GridTable+CopyOffset)]
			shr  eax,1
			sub  esp,16
		     movaps  xmm0,dqword[eax+GridColorTable+CopyOffset]
		      mulps  xmm0,xmm7
		     movups  dqword[esp],xmm0
		     invoke  glColor4f

		     invoke  glTexCoord2f,1.0,1.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*1*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.px]
		       fadd  dword[esp+4*3]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*1*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.py]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f

		     invoke  glTexCoord2f,0.0,1.0
		       push  GRID_Z
			sub  esp,4*2
			fld  dword[ebx+32*1*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.px]
		       fsub  dword[esp+4*3]
		       fstp  dword[esp+4*0]
			fld  dword[ebx+32*1*((PLAY_WIDTH/GRID_SPACING)+1)+GRID.py]
		       fstp  dword[esp+4*1]
		     invoke  glVertex3f


			add  ebx,32*((PLAY_WIDTH/GRID_SPACING)+1)
			add  esi,1
			cmp  esi,PLAY_HEIGHT/GRID_SPACING
			 jb  .3
	    .3s:
			add  edi,1
			cmp  edi,PLAY_WIDTH/GRID_SPACING
			jbe  .4

.10:

		     invoke  glEnd
			add  esp,4+16
			ret




		      align  16

Grid_Update:


		      ;  xmm4: position   ; -> (esi,edi)
		      ;  xmm5: radius
		      ;  xmm6: f
		      ;  xmm7: a
		      ; f*(x-y)/(a+|x-y|^2)

		       push  100.0
		       push  25.0
		       push  20.0
			mov  ebx,dword[ShotCount+CopyOffset]
			mov  esi,ShotTable+CopyOffset
			jmp  .6
	      .5:     movsd  xmm4,qword[esi+OBJ1.px]
		      movss  xmm5,dword[esp+4*0]
		      movss  xmm6,dword[esp+4*1]
		      movss  xmm7,dword[esp+4*2]
		       call  Grid_ApplyForce
			add  esi,32
	      .6:	sub  ebx,1
			jns  .5
			add  esp,4*3

		      push  200.0
		      push  -200.0
		      push  800.0
			mov  ebx,dword[BlackholeCount+CopyOffset]
			mov  esi,BlackholeTable+CopyOffset
			jmp  .4
	      .3:     movsd  xmm4,qword[esi+BLACKHOLE.px]
			cmp  dword[esi+BLACKHOLE.hth],BLACKHOLE_MAXHEALTH
		      movss  xmm5,dword[esp+4*0]
		      movss  xmm6,dword[esp+4*1]
		      movss  xmm7,dword[esp+4*2]
			jge  @f
		       call  Grid_ApplyForce
		  @@:	add  esi,32
	      .4:	sub  ebx,1
			jns  .3
			add  esp,4*3


			cmp  dword[Bomb.ct],0
			jle  @f
		       push  10000.0
		       push  +500.0
		       push  20.0
		      movsd  xmm4,qword[Bomb.px]
		      movss  xmm5,dword[Bomb.r]
		      movss  xmm6,dword[esp+4*1]
		      movss  xmm7,dword[esp+4*2]
		       call  Grid_ApplyForce
			add  esp,4*3
		  @@:

			cmp  dword[GameMode],GAMEMODE_START
			jne  @f
		       push  10000.0
		       push  -5000.0
		       push  2000.0
		      movsd  xmm4,qword[Player.px]
		      movss  xmm5,dword[esp+4*0]
		      movss  xmm6,dword[esp+4*1]
		      movss  xmm7,dword[esp+4*2]
		       call  Grid_ApplyForce
			add  esp,4*3
		  @@:


Grid_Update_ClearBorder:
		      xorps  xmm0,xmm0
			mov  esi,GridTable

			mov  ebx,PLAY_WIDTH/GRID_SPACING
	      .1:    movups  dqword[esi+GRID.vx],xmm0
			add  esi,32
			sub  ebx,1
			jnz  .1

			mov  ebx,PLAY_HEIGHT/GRID_SPACING-1
	      .2:    movups  dqword[esi+32*0+GRID.vx],xmm0
		     movups  dqword[esi+32*1+GRID.vx],xmm0
			add  esi,32*((PLAY_WIDTH/GRID_SPACING)+1)
			sub  ebx,1
			jnz  .2

			mov  ebx,PLAY_WIDTH/GRID_SPACING+2
	      .3:    movups  dqword[esi+GRID.vx],xmm0
			add  esi,32
			sub  ebx,1
			jnz  .3







Grid_Update_Forces:
		       push  GRID_MDAMPING
		       push  GRID_MDAMPING
		       push  0.0
		       push  0.0
		     movups  xmm7,[esp]
			add  esp,16

		       push  1.0e-6
		       push  1.0e-6
		       push  1.0e-6
		       push  1.0e-6
		     movups  xmm6,[esp]
			add  esp,16

		     movaps  xmm5,dqword[const_f4x4_manmask]

			mov  esi,GridTable
			mov  ebx,((PLAY_WIDTH/GRID_SPACING)+1)*((PLAY_HEIGHT/GRID_SPACING)+1)
	      .1:     movsd  xmm0,qword[esi+32*0+GRID.px]
		     movhpd  xmm0,qword[esi+32*1+GRID.px]
		      movsd  xmm1,qword[esi+32*0+GRID.vx]
		     movhpd  xmm1,qword[esi+32*1+GRID.vx]
		      movsd  xmm2,qword[esi+32*0+GRID.fx]
		     movhpd  xmm2,qword[esi+32*1+GRID.fx]
		      movsd  xmm3,qword[esi+32*0+GRID.Dx]
		     movhpd  xmm3,qword[esi+32*1+GRID.Dx]
		      addps  xmm1,xmm2
		      addps  xmm0,xmm1
		      mulps  xmm1,xmm3
		     movaps  xmm4,xmm1
		      andps  xmm4,xmm5
		   cmpnltps  xmm4,xmm6
		      andps  xmm1,xmm4
		      movsd  qword[esi+32*0+GRID.px],xmm0
		     movhpd  qword[esi+32*1+GRID.px],xmm0
		      movsd  qword[esi+32*0+GRID.vx],xmm1
		     movhpd  qword[esi+32*1+GRID.vx],xmm1
		     movaps  dqword[esi+32*0+GRID.fx],xmm7
		     movaps  dqword[esi+32*1+GRID.fx],xmm7
			add  esi,32*2
			sub  ebx,2
			jns  .1

			mov  eax,GridTable+32*((PLAY_WIDTH/GRID_SPACING)+1)
			mov  ecx,GridTable+32*((PLAY_WIDTH/GRID_SPACING)+2)
			mov  esi,PLAY_HEIGHT/GRID_SPACING-1
	      .3:	mov  edi,PLAY_WIDTH/GRID_SPACING
	      .2:      call  Spring
			add  eax,32
			add  ecx,32
			sub  edi,1
			jnz  .2
			add  eax,32
			add  ecx,32
			sub  esi,1
			jnz  .3

			mov  eax,GridTable+32*(1)
			mov  ecx,GridTable+32*((PLAY_WIDTH/GRID_SPACING)+2)
			mov  esi,PLAY_HEIGHT/GRID_SPACING
	      .5:	mov  edi,PLAY_WIDTH/GRID_SPACING-1
	      .4:      call  Spring
			add  eax,32
			add  ecx,32
			sub  edi,1
			jnz  .4
			add  eax,64
			add  ecx,64
			sub  esi,1
			jnz  .5


Grid_Update_Normalize:


		       push  GRID_MK
		       push  GRID_MK
		      movsd  xmm7,qword[esp]
			add  esp,8

			mov  ebx,GridTable
			xor  esi,esi
	      .2:	xor  edi,edi
	      .1:
		      xorps  xmm0,xmm0
		   cvtsi2ss  xmm0,edi
		   cvtsi2ss  xmm1,esi
		     pslldq  xmm1,4
		       orps  xmm0,xmm1
		      movsd  xmm2,qword[ebx+GRID.px]
		      movsd  xmm3,qword[ebx+GRID.fx]
		      subps  xmm2,xmm0
		      mulps  xmm2,xmm7
		      addps  xmm2,xmm3
		      movsd  qword[ebx+GRID.fx],xmm2
			add  ebx,32
			add  edi,GRID_SPACING
			cmp  edi,PLAY_WIDTH
			jbe  .1
			add  esi,GRID_SPACING
			cmp  esi,PLAY_HEIGHT
			jbe  .2

			ret



		      align  16
   Spring:	      movsd  xmm0,qword[eax+GRID.px]
		      movsd  xmm1,qword[ecx+GRID.px]
		      subps  xmm0,xmm1
		      movsd  xmm1,qword[eax+GRID.vx]
		      movsd  xmm2,qword[ecx+GRID.vx]
		      subps  xmm1,xmm2
		      movsd  xmm4,qword[ecx+GRID.fx]
		      movsd  xmm5,qword[eax+GRID.fx]
		     movaps  xmm2,xmm0
		      mulps  xmm2,xmm0
		     haddps  xmm2,xmm2
		    rsqrtss  xmm2,xmm2
		      subss  xmm2,dword[const_f4_1d16]
		     shufps  xmm2,xmm2,0
		     movaps  xmm3,xmm2
		      psrad  xmm3,31
		      mulps  xmm2,dqword[Minus16Stiffness]
		      mulps  xmm0,xmm2
		      mulps  xmm1,dqword[Damping]
		      addps  xmm0,xmm1
		      andps  xmm0,xmm3
		      addps  xmm4,xmm0
		      subps  xmm5,xmm0
		      movsd  qword[ecx+GRID.fx],xmm4
		      movsd  qword[eax+GRID.fx],xmm5
			ret



align 16
Minus16Stiffness:  dd GRID_SSTIFFNESS,GRID_SSTIFFNESS,GRID_SSTIFFNESS,GRID_SSTIFFNESS
Damping:	  dd GRID_SDAMPING,GRID_SDAMPING,GRID_SDAMPING,GRID_SDAMPING



		      align  16
Grid_ApplyForce:
		      ;  xmm4: position   ; -> (esi,edi)
		      ;  xmm5: radius
		      ;  xmm6: f
		      ;  xmm7: a
		      ; f*(x-y)/(a+|x-y|^2)

		       push  ebx esi edi

		     movaps  xmm0,xmm4
		     psrldq  xmm0,4
		   cvtss2si  ebx,xmm5
		   cvtss2si  esi,xmm4
		   cvtss2si  edi,xmm0
		      mulss  xmm5,xmm5
			shr  esi,4
			shr  edi,4
			shr  ebx,4
			sub  esi,ebx
			sub  edi,ebx
			add  ebx,ebx

			xor  edx,edx
	      .1:	xor  eax,eax
	      .2:      push  esi edi

			add  esi,eax
			 js  .skip
			add  edi,edx
			 js  .skip
			cmp  esi,PLAY_WIDTH/GRID_SPACING
			 ja  .skip
			cmp  edi,PLAY_HEIGHT/GRID_SPACING
			 ja  .skip

		       imul  edi,(PLAY_WIDTH/GRID_SPACING)+1
			add  edi,esi
			shl  edi,5
			add  edi,GridTable

		      movsd  xmm0,qword[edi+GRID.px]
		      subps  xmm0,xmm4
		     movaps  xmm1,xmm0
		      mulps  xmm1,xmm0
		     haddps  xmm1,xmm1
		     comiss  xmm1,xmm5
			 ja  .skip

		      movsd  xmm2,qword[edi+GRID.fx]
		      movsd  xmm3,qword[edi+GRID.Dx]
		     movaps  xmm3,dqword[.a]
		      addss  xmm1,xmm7
		      rcpss  xmm1,xmm1
		      mulss  xmm1,xmm6
		     shufps  xmm1,xmm1,0
		      mulps  xmm1,xmm0
		      addps  xmm2,xmm1
		      movsd  qword[edi+GRID.fx],xmm2
		      movsd  qword[edi+GRID.Dx],xmm3

		.skip:
			pop  edi esi
			add  eax,1
			cmp  eax,ebx
			jbe  .2
			add  edx,1
			cmp  edx,ebx
			jbe  .1

			pop  edi esi ebx
			ret


align 16
 .a    dd GRID_EDAMPING,GRID_EDAMPING,GRID_EDAMPING,GRID_EDAMPING








		      align  16
PrintDot:	     invoke  glTexCoord2f,0.0,0.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi]
		      addps  xmm0,dqword[.1]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,1.0,0.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi]
		      addps  xmm0,dqword[.2]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,1.0,1.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi]
		      addps  xmm0,dqword[.3]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.0,1.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi]
		      addps  xmm0,dqword[.4]
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

			ret
align 16
.1: dd -128.0,-128.0,0.0,0.0
.2: dd +128.0,-128.0,0.0,0.0
.3: dd +128.0,+128.0,0.0,0.0
.4: dd -128.0,+128.0,0.0,0.0





