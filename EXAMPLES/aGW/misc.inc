



		      align  16
SetRotationMatrix:
		; eax = alpha
		; edx = beta
		; ecx = gamma

		      movss  xmm0,dword[AngleTable+8*edx+4*0]
		      mulss  xmm0,dword[AngleTable+8*ecx+4*0]
		      movss  dword[RotationMatrix.x+4*0],xmm0

		      xorps  xmm0,xmm0
		      movss  dword[RotationMatrix.x+4*3],xmm0

		      movss  xmm1,dword[AngleTable+8*edx+4*0]
		      mulss  xmm1,dword[AngleTable+8*ecx+4*1]
		      subss  xmm0,xmm1
		      movss  dword[RotationMatrix.x+4*1],xmm0

		      movss  xmm0,dword[AngleTable+8*edx+4*1]
		      movss  dword[RotationMatrix.x+4*2],xmm0


		      movss  xmm0,dword[AngleTable+8*ecx+4*0]
		      mulss  xmm0,dword[AngleTable+8*eax+4*1]
		      mulss  xmm0,dword[AngleTable+8*edx+4*1]
		      movss  xmm1,dword[AngleTable+8*eax+4*0]
		      mulss  xmm1,dword[AngleTable+8*ecx+4*1]
		      addss  xmm0,xmm1
		      movss  dword[RotationMatrix.y+4*0],xmm0

		      movss  xmm0,dword[AngleTable+8*eax+4*1]
		      mulss  xmm0,dword[AngleTable+8*edx+4*1]
		      mulss  xmm0,dword[AngleTable+8*ecx+4*1]
		      movss  xmm1,dword[AngleTable+8*eax+4*0]
		      mulss  xmm1,dword[AngleTable+8*ecx+4*0]
		      subss  xmm1,xmm0
		      movss  dword[RotationMatrix.y+4*1],xmm1

		      xorps  xmm0,xmm0
		      movss  dword[RotationMatrix.y+4*3],xmm0

		      movss  xmm1,dword[AngleTable+8*edx+4*0]
		      mulss  xmm1,dword[AngleTable+8*eax+4*1]
		      subss  xmm0,xmm1
		      movss  dword[RotationMatrix.y+4*2],xmm0

			ret




		      align  16

GLInit:

		   invoke  glViewport,0,0,WINDOW_WIDTH,WINDOW_HEIGHT


		     invoke  glMatrixMode, GL_PROJECTION
		     invoke  glLoadIdentity
		  ;     fild  [rc.right]
		  ;    fidiv  [rc.bottom]
		  ;     fstp  qword[Perspective+8*1]


			sub  esp,32
		     movups  xmm0,dqword[Perspective+8*0]
		     movups  xmm1,dqword[Perspective+8*2]
		     movups  dqword[esp+8*0],xmm0
		     movups  dqword[esp+8*2],xmm1
		     invoke  gluPerspective

		     invoke  glMatrixMode,GL_MODELVIEW
		     invoke  glDisable,GL_DEPTH_TEST
		     invoke  glDisable,GL_COLOR_MATERIAL
		     invoke  glEnable,GL_BLEND
		     invoke  glDisable,GL_LIGHTING



		       call  LoadGLExtensions
		       test  eax,eax
			 jz  .fail

		     invoke  glGenTextures,(TextureTableEnd-TextureTable)/4,TextureTable

		     invoke  glTexParameterf,GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR
		     invoke  glTexParameterf,GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR


		     invoke  glBindTexture,GL_TEXTURE_2D,[PlayerTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,64,64,GL_RGBA,GL_UNSIGNED_BYTE,PlayerTextureData

		     invoke  glBindTexture,GL_TEXTURE_2D,[ShotTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,32,32,GL_RGBA,GL_UNSIGNED_BYTE,ShotTextureData

		     invoke  glBindTexture,GL_TEXTURE_2D,[GeomTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,32,32,GL_RGBA,GL_UNSIGNED_BYTE,GeomTextureData

		     invoke  glBindTexture,GL_TEXTURE_2D,[ParTexture]
		   invoke  glTexParameterf,GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR
		     invoke  glTexParameterf,GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR
		       invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,16,16,GL_RGBA,GL_UNSIGNED_BYTE,ParTextureData



		     invoke  glBindTexture,GL_TEXTURE_2D,[StickTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,32,32,GL_RGBA,GL_UNSIGNED_BYTE,StickTextureData


		     invoke  glBindTexture,GL_TEXTURE_2D,[BlackholeTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,32,32,GL_RGBA,GL_UNSIGNED_BYTE,BlackholeTextureData


		     invoke  glBindTexture,GL_TEXTURE_2D,[GridTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,32,32,GL_RGBA,GL_UNSIGNED_BYTE,GridTextureData

		     invoke  glBindTexture,GL_TEXTURE_2D,[BombTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,32,32,GL_RGBA,GL_UNSIGNED_BYTE,BombTextureData

		     invoke  glBindTexture,GL_TEXTURE_2D,[BorderTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,16,16,GL_RGBA,GL_UNSIGNED_BYTE,BorderTextureData

		     invoke  glBindTexture,GL_TEXTURE_2D,[LineTexture]
		     invoke  gluBuild2DMipmaps,GL_TEXTURE_2D,GL_RGBA,32,32,GL_RGBA,GL_UNSIGNED_BYTE,LineTextureData


		     invoke  glCreateShader,GL_VERTEX_SHADER
			mov  [HorVertexShaderObject],eax
		     invoke  glCreateShader,GL_FRAGMENT_SHADER
			mov  [HorFragmentShaderObject],eax
		     invoke  glShaderSource,[HorVertexShaderObject],1,HorVertexShaderSourcePtr,NULL
		     invoke  glShaderSource,[HorFragmentShaderObject],1,HorFragmentShaderSourcePtr,NULL
		     invoke  glCompileShader,[HorVertexShaderObject]
		     invoke  glGetShaderiv,[HorVertexShaderObject],GL_COMPILE_STATUS,CompiledQ
			cmp  dword[CompiledQ],GL_TRUE
			 je  @f
		     invoke  glGetShaderInfoLog,[HorVertexShaderObject],1024,NULL,text
		     invoke  MessageBox,0,text,'HorVertex',MB_OK
			jmp  .fail
			@@:
		     invoke  glCompileShader,[HorFragmentShaderObject]
		     invoke  glGetShaderiv,[HorFragmentShaderObject],GL_COMPILE_STATUS,CompiledQ
			cmp  dword[CompiledQ],GL_TRUE
			 je  @f
		     invoke  glGetShaderInfoLog,[HorFragmentShaderObject],1024,NULL,text
		     invoke  MessageBox,0,text,'HorFragment',MB_OK
			jmp  .fail
			@@:
		     invoke  glCreateProgram
			mov  [HorProgram],eax
		     invoke  glAttachShader,[HorProgram],[HorVertexShaderObject]
		     invoke  glAttachShader,[HorProgram],[HorFragmentShaderObject]
		     invoke  glLinkProgram,[HorProgram]
		     invoke  glGetProgramiv,[HorProgram],GL_LINK_STATUS,CompiledQ
			cmp  dword[CompiledQ],GL_TRUE
			 je  @f
		     invoke  MessageBox,0,'hor link failed','HorProgram',MB_OK
			jmp  .fail
			@@:



		     invoke  glCreateShader,GL_VERTEX_SHADER
			mov  [VerVertexShaderObject],eax
		     invoke  glCreateShader,GL_FRAGMENT_SHADER
			mov  [VerFragmentShaderObject],eax
		     invoke  glShaderSource,[VerVertexShaderObject],1,VerVertexShaderSourcePtr,NULL
		     invoke  glShaderSource,[VerFragmentShaderObject],1,VerFragmentShaderSourcePtr,NULL
		     invoke  glCompileShader,[VerVertexShaderObject]
		     invoke  glGetShaderiv,[VerVertexShaderObject],GL_COMPILE_STATUS,CompiledQ
			cmp  dword[CompiledQ],GL_TRUE
			 je  @f
		     invoke  glGetShaderInfoLog,[VerVertexShaderObject],1024,NULL,text
		     invoke  MessageBox,0,text,'VerVertex',MB_OK
			jmp  .fail
			@@:
		     invoke  glCompileShader,[VerFragmentShaderObject]
		     invoke  glGetShaderiv,[VerFragmentShaderObject],GL_COMPILE_STATUS,CompiledQ
			cmp  dword[CompiledQ],GL_TRUE
			 je  @f
		     invoke  glGetShaderInfoLog,[VerFragmentShaderObject],1024,NULL,text
		     invoke  MessageBox,0,text,'VerFragment',MB_OK
			jmp  .fail
			@@:
		     invoke  glCreateProgram
			mov  [VerProgram],eax
		     invoke  glAttachShader,[VerProgram],[VerVertexShaderObject]
		     invoke  glAttachShader,[VerProgram],[VerFragmentShaderObject]
		     invoke  glLinkProgram,[VerProgram]
		     invoke  glGetProgramiv,[VerProgram],GL_LINK_STATUS,CompiledQ
			cmp  dword[CompiledQ],GL_TRUE
			 je  @f
		     invoke  MessageBox,0,'ver link failed','VerProgram',MB_OK
			jmp  .fail
			@@:





		     invoke  GetClientRect,[hwnd],rc
		     invoke  glViewport,0,0,[rc.right],[rc.bottom]
		     invoke  glMatrixMode,GL_PROJECTION
		     invoke  glMatrixMode,GL_MODELVIEW
		     invoke  glDisable,GL_DEPTH_TEST
		     invoke  glDisable,GL_COLOR_MATERIAL
		     invoke  glEnable,GL_BLEND
		     invoke  glEnable,GL_TEXTURE_2D
		     invoke  glDisable,GL_LIGHTING


		    ; invoke  glGenTextures,(TextureTableEnd-TextureTable)/4,TextureTable     already did it
		    ; invoke  glGenRenderbuffers,(RBTableEnd-TextureTable)/4,RBTable
		     invoke  glGenFramebuffers,(FBOTableEnd-FBOTable)/4,FBOTable







		     invoke  glBindFramebuffer,GL_FRAMEBUFFER,[FBOA]
		     invoke  glBindTexture,GL_TEXTURE_2D,[TexA]
		     invoke  glTexImage2D,GL_TEXTURE_2D,0,GL_RGBA8,WINDOW_WIDTH,WINDOW_HEIGHT,0,GL_RGBA,GL_FLOAT,NULL
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE
		     invoke  glFramebufferTexture2D,GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,[TexA],0
	       ;      invoke  glBindRenderbuffer,GL_RENDERBUFFER,[RBA]
	       ;      invoke  glRenderbufferStorage,GL_RENDERBUFFER,GL_DEPTH_COMPONENT24,[rc.right],[rc.bottom]
		     invoke  glCheckFramebufferStatus,GL_FRAMEBUFFER
			cmp  eax,GL_FRAMEBUFFER_COMPLETE
			jne  .fail

		     invoke  glBindFramebuffer,GL_FRAMEBUFFER,[FBOB]
		     invoke  glBindTexture,GL_TEXTURE_2D,[TexB]
		     invoke  glTexImage2D,GL_TEXTURE_2D,0,GL_RGBA8,WINDOW_WIDTH,WINDOW_HEIGHT,0,GL_RGBA,GL_FLOAT,NULL
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE
		     invoke  glFramebufferTexture2D,GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,[TexB],0
	       ;      invoke  glBindRenderbuffer,GL_RENDERBUFFER,[RBA]
	       ;      invoke  glRenderbufferStorage,GL_RENDERBUFFER,GL_DEPTH_COMPONENT24,[rc.right],[rc.bottom]
		     invoke  glCheckFramebufferStatus,GL_FRAMEBUFFER
			cmp  eax,GL_FRAMEBUFFER_COMPLETE
			jne  .fail

		     invoke  glBindFramebuffer,GL_FRAMEBUFFER,[FBOC]
		     invoke  glBindTexture,GL_TEXTURE_2D,[TexC]
		     invoke  glTexImage2D,GL_TEXTURE_2D,0,GL_RGBA8,WINDOW_WIDTH,WINDOW_HEIGHT,0,GL_RGBA,GL_FLOAT,NULL
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE
		     invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE
		     invoke  glFramebufferTexture2D,GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,[TexC],0
	       ;      invoke  glBindRenderbuffer,GL_RENDERBUFFER,[RBA]
	       ;      invoke  glRenderbufferStorage,GL_RENDERBUFFER,GL_DEPTH_COMPONENT24,[rc.right],[rc.bottom]
		     invoke  glCheckFramebufferStatus,GL_FRAMEBUFFER
			cmp  eax,GL_FRAMEBUFFER_COMPLETE
			jne  .fail

	   ;          invoke  glBindFramebuffer,GL_FRAMEBUFFER,[FBOD]
	   ;          invoke  glBindTexture,GL_TEXTURE_2D,[TexD]
	   ;          invoke  glTexImage2D,GL_TEXTURE_2D,0,GL_RGBA8,WINDOW_WIDTH,WINDOW_HEIGHT,0,GL_RGBA,GL_FLOAT,NULL
	   ;          invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST
	   ;          invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST
	   ;          invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE
	   ;          invoke  glTexParameteri,GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE
	   ;          invoke  glFramebufferTexture2D,GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,[TexD],0
	   ;    ;      invoke  glBindRenderbuffer,GL_RENDERBUFFER,[RBA]
	   ;    ;      invoke  glRenderbufferStorage,GL_RENDERBUFFER,GL_DEPTH_COMPONENT24,[rc.right],[rc.bottom]
	   ;          invoke  glCheckFramebufferStatus,GL_FRAMEBUFFER
	   ;             cmp  eax,GL_FRAMEBUFFER_COMPLETE
	   ;             jne  .fail

		     invoke  glBindFramebuffer,GL_FRAMEBUFFER, 0



			 or  eax,-1
			ret

  .fail:		xor  eax,eax
			ret



GLCleanup:
		     invoke  glDeleteTextures,(TextureTableEnd-TextureTable)/4,TextureTable
			ret







		      align  16
GetRand.word:		sub  esp,16*4
		     movups  [esp+16*0],xmm0
		     movups  [esp+16*1],xmm1
		     movups  [esp+16*2],xmm2
		     movups  [esp+16*3],xmm3
		     movdqa  xmm0,dqword[RandSeed]
		     movdqa  xmm1,xmm0
		     movdqa  xmm2,xmm0
		     pslldq  xmm1,1
		     psrldq  xmm2,15
			por  xmm1,xmm2
		     movdqa  xmm2,xmm0
		      psllq  xmm2,1
			por  xmm1,xmm2
		     movdqa  xmm2,xmm0
		     movdqa  xmm3,xmm0
		     psrldq  xmm2,1
		     pslldq  xmm3,15
			por  xmm2,xmm3
		     movdqa  xmm3,xmm0
		      psrlq  xmm3,1
		       pxor  xmm2,xmm3
			por  xmm0,xmm1
		       pxor  xmm0,xmm2
		     movdqa  dqword[RandSeed],xmm0
		   pmovmskb  eax,xmm0
		     movups  xmm0,[esp+16*0]
		     movups  xmm1,[esp+16*1]
		     movups  xmm2,[esp+16*2]
		     movups  xmm3,[esp+16*3]
			add  esp,16*4
			ret


		      align  16
PrintInteger:	    ; eax: number
		    ; edi: string result
		       push  ebx
		       push  ebp
		       push  edx
		       push  ecx
			mov  ebx,4
			mov  ecx,10
			mov  ebp,esp
		.l1:	xor  edx,edx
			div  ecx
			sub  ebx,1
			jnz  @f
		       push  ','-'0'
			mov  ebx,3
		 @@:   push  edx
		       test  eax,eax
			jnz  .l1
		.l2:	pop  eax
			add  al,'0'
		      stosb
			cmp  esp,ebp
			 jb  .l2
			pop  ecx
			pop  edx
			pop  ebp
			pop  ebx
			ret



		      align  16
PrintStats:

		     invoke  glColor4f,1.00,1.00,1.00,1.0
		     invoke  glBindTexture,GL_TEXTURE_2D,dword[LineTexture]
		     invoke  glBegin,GL_QUADS


			mov  eax,dword[Score]
			mov  edi,text
		       call  PrintInteger
			mov  byte[edi],0
		       push  +0.9
		       push  -0.96
		       push  0.06
		       push  0.02
			mov  esi,text
		       call  DrawString



			mov  eax,dword[GridClick.x]
			mov  edi,text
		       call  PrintInteger
			mov  byte[edi],0
		       push  0.9
		       push  0.8
		       push  0.045
		       push  0.015
			mov  esi,text
		       call  DrawString

			mov  eax,dword[GridClick.y]
			mov  edi,text
		       call  PrintInteger
			mov  byte[edi],0
		       push  0.8
		       push  0.8
		       push  0.045
		       push  0.015
			mov  esi,text
		       call  DrawString



			mov  eax,dword[RenderRate]
			mov  edi,text
		       call  PrintInteger
			mov  byte[edi],0
		       push  -0.85
		       push  -0.96
		       push  0.045
		       push  0.015
			mov  esi,text
		       call  DrawString

			mov  eax,dword[GridRate]
			mov  edi,text
		       call  PrintInteger
			mov  byte[edi],0
		       push  -0.90
		       push  -0.96
		       push  0.045
		       push  0.015
			mov  esi,text
		       call  DrawString

			mov  eax,dword[PhysicsRate]
			mov  edi,text
		       call  PrintInteger
			mov  byte[edi],0
		       push  -0.95
		       push  -0.96
		       push  0.045
		       push  0.015
			mov  esi,text
		       call  DrawString

			mov  edi,text
			mov  al,'x'
		      stosb
			mov  eax,dword[Multiplier]
		       call  PrintInteger
			mov  byte[edi],0
		       push  +0.82
		       push  -0.96
		       push  0.045
		       push  0.015
			mov  esi,text
		       call  DrawString

		     invoke  glEnd
			ret


DrawString:	     ; esi: null term string
		     ; push off.y
		     ; push off.x
		     ; push scale.y
		     ; push scale.x

			jmp  .3
	      .0:	mov  ebp,[CharLookup+4*eax]
			mov  ebx,[ebp+4*1]
			lea  edi,[ebp+4*2]
			jmp  .2
	      .1:     movsd  xmm6,[edi+0]
		      movsd  xmm7,[edi+8]
		      movsd  xmm0,[esp+4*1]
		      movsd  xmm1,[esp+4*3]
		      mulps  xmm6,xmm0
		      mulps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  CharLineDrawNoEnd
			add  edi,16
	      .2:	sub  ebx,1
			jns  .1
		      movss  xmm0,[esp+4*1]
		      movss  xmm1,[esp+4*3]
		      mulss  xmm0,[ebp+4*0]
		      addss  xmm1,xmm0
		      movss  [esp+4*3],xmm1
	      .3:     lodsb
			and  eax,0x07F
			jnz  .0

	.ret:
			ret  4*4


CharLineDrawNoEnd:
		     movaps  xmm5,xmm7
		      subps  xmm5,xmm6
		     movaps  xmm0,xmm5
		      mulps  xmm0,xmm5
		     haddps  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		      addps  xmm0,dqword[.e]
		    rsqrtps  xmm0,xmm0
		      mulps  xmm5,xmm0
		     movaps  xmm4,xmm5
		     shufps  xmm4,xmm4,011110001b
		      mulps  xmm5,dqword[.n]
		      mulps  xmm4,dqword[.np]



		     invoke  glTexCoord2f,0.25,0.25
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,0.75,0.25
			sub  esp,4*2
		      movsd  xmm0,xmm6
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.75,0.75
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      subps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.25,0.75
			sub  esp,4*2
		      movsd  xmm0,xmm7
		      addps  xmm0,xmm4
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

			ret


align 16
.n:  dd +0.015,+0.015,0.0,0.0
.np: dd -0.015,+0.015,0.0,0.0
.e:  dd 0.0001,0.0001,0.0001,0.0001





		      align  16
PrintRenderTime:

		     ;  test  dword[FrameCount],0x0F
		     ;   jnz  .ret

			mov  edi,1.0
			mov  esi,9.0
			mov  ebx,8.0
		     invoke  glColor4f,1.0,1.0,1.0,1.0
			mov  eax,[RenderRate]
		       push  ebp
			mov  ebp,esp
			mov  ecx,10
		 @@:	xor  edx,edx
			div  ecx
		       push  edx
		       test  eax,eax
			jnz  @b
		 @@:	pop  eax

		    ; invoke  glBindTexture,GL_TEXTURE_2D,dword[CharTable+4*eax]
		     invoke  glBegin,GL_QUADS
		     invoke  glTexCoord2f,0.0,0.0
		     invoke  glVertex2f,edi,1.0
		     invoke  glTexCoord2f,1.0,0.0
		     invoke  glVertex2f,esi,1.0
		     invoke  glTexCoord2f,1.0,1.0
		     invoke  glVertex2f,esi,9.0
		     invoke  glTexCoord2f,0.0,1.0
		     invoke  glVertex2f,edi,9.0
		     invoke  glEnd

		       movd  xmm0,edi
		       movd  xmm1,esi
		       movd  xmm2,ebx
		      addss  xmm0,xmm2
		      addss  xmm1,xmm2
		       movd  edi,xmm0
		       movd  esi,xmm1

			cmp  esp,ebp
			 jb  @b
			pop  ebp
	       .ret:
			ret




		      align  16
SetTargetFPS:	       fild  qword[Frequency]
			fld  dword[TargetFPS]
		      fdivp  st1,st0
		      fistp  qword[TickIncrement]
			ret



		      align  16
CorrectMainWindowSize:
		       push  esi edi
			mov  esi,WINDOW_WIDTH
			mov  edi,WINDOW_HEIGHT
		     invoke  MoveWindow,dword[hwnd],0,0,esi,edi,TRUE
		     invoke  GetClientRect,[hwnd],rc
			mov  eax,2*WINDOW_WIDTH
			mov  ecx,2*WINDOW_HEIGHT
			sub  eax,dword[rc.right]
			sub  ecx,dword[rc.bottom]
		       push  eax
		       push  ecx
		     invoke  GetSystemMetrics,SM_CXSCREEN
			mov  esi,eax
		     invoke  GetSystemMetrics,SM_CYSCREEN
			mov  edi,eax
			shr  esi,1
			shr  edi,1
			mov  eax,WINDOW_WIDTH/2
			mov  ecx,WINDOW_HEIGHT/2
			sub  esi,eax
			sub  edi,ecx
			mov  eax,esi
			sar  eax,31
			not  eax
			and  esi,eax
			mov  eax,edi
			sar  eax,31
			not  eax
			and  edi,eax
			pop  ecx
			pop  eax
		     invoke  MoveWindow,dword[hwnd],esi,edi,eax,ecx,TRUE
		     invoke  GetClientRect,[hwnd],rc
		     invoke  glViewport,0,0,WINDOW_WIDTH,WINDOW_HEIGHT
			pop  edi esi
			ret



LoadGLExtensions:
		     invoke  wglGetProcAddress,'glGenRenderbuffers'
		       test  eax,eax
			 jz  .retz
			mov  [glGenRenderbuffers],eax

		     invoke  wglGetProcAddress,'glDeleteRenderbuffers'
		       test  eax,eax
			 jz  .retz
			mov  [glDeleteRenderbuffers],eax

		     invoke  wglGetProcAddress,'glBindRenderbuffer'
		       test  eax,eax
			 jz  .retz
			mov  [glBindRenderbuffer],eax


		     invoke  wglGetProcAddress,'glGenFramebuffers'
		       test  eax,eax
			 jz  .retz
			mov  [glGenFramebuffers],eax

		     invoke  wglGetProcAddress,'glDeleteFramebuffers'
		       test  eax,eax
			 jz  .retz
			mov  [glDeleteFramebuffers],eax

		     invoke  wglGetProcAddress,'glBindFramebuffer'
		       test  eax,eax
			 jz  .retz
			mov  [glBindFramebuffer],eax


		     invoke  wglGetProcAddress,'glFramebufferTexture2D'
		       test  eax,eax
			 jz  .retz
			mov  [glFramebufferTexture2D],eax

		     invoke  wglGetProcAddress,'glCheckFramebufferStatus'
		       test  eax,eax
			 jz  .retz
			mov  [glCheckFramebufferStatus],eax

		     invoke  wglGetProcAddress,'glRenderbufferStorage'
		       test  eax,eax
			 jz  .retz
			mov  [glRenderbufferStorage],eax

		     invoke  wglGetProcAddress,'glFramebufferRenderbuffer'
		       test  eax,eax
			 jz  .retz
			mov  [glFramebufferRenderbuffer],eax



		     invoke  wglGetProcAddress,'glCreateShader'
		       test  eax,eax
			 jz  .retz
			mov  [glCreateShader],eax


		     invoke  wglGetProcAddress,'glDeleteShader'
		       test  eax,eax
			 jz  .retz
			mov  [glDeleteShader],eax

		     invoke  wglGetProcAddress,'glShaderSource'
		       test  eax,eax
			 jz  .retz
			mov  [glShaderSource],eax

		     invoke  wglGetProcAddress,'glCompileShader'
		       test  eax,eax
			 jz  .retz
			mov  [glCompileShader],eax

		     invoke  wglGetProcAddress,'glGetShaderiv'
		       test  eax,eax
			 jz  .retz
			mov  [glGetShaderiv],eax

		     invoke  wglGetProcAddress,'glGetShaderInfoLog'
		       test  eax,eax
			 jz  .retz
			mov  [glGetShaderInfoLog],eax


		     invoke  wglGetProcAddress,'glCreateProgram'
		       test  eax,eax
			 jz  .retz
			mov  [glCreateProgram],eax
		     invoke  wglGetProcAddress,'glDeleteProgram'
		       test  eax,eax
			 jz  .retz
			mov  [glDeleteProgram],eax
		     invoke  wglGetProcAddress,'glAttachShader'
		       test  eax,eax
			 jz  .retz
			mov  [glAttachShader],eax
		     invoke  wglGetProcAddress,'glLinkProgram'
		       test  eax,eax
			 jz  .retz
			mov  [glLinkProgram],eax
		     invoke  wglGetProcAddress,'glGetProgramiv'
		       test  eax,eax
			 jz  .retz
			mov  [glGetProgramiv],eax
		     invoke  wglGetProcAddress,'glUseProgram'
		       test  eax,eax
			 jz  .retz
			mov  [glUseProgram],eax

			 or  eax,-1
			ret
	     .retz:
			xor  eax,eax
			ret




