




		      align  16
PlayerFire_Update:
			mov  eax,[SelectedWeapon]
			mov  eax,[eax+4*2]
			sub  [FireRecharge],1
			jns  .ret
			mov  [FireRecharge],eax

		       test  dword[MouseState.Buttons],MOUSE_LBUTTON
			jnz  .Mouse
		       test  dword[KeyboardState.Buttons],KEYBOARD_PLAYER_FIRE_UP or KEYBOARD_PLAYER_FIRE_DOWN or KEYBOARD_PLAYER_FIRE_LEFT or KEYBOARD_PLAYER_FIRE_RIGHT
			jnz  .Keyboard

  .Controler:		cmp  dword[CameraMode],CAMERA_MODE_FIRST
			 je  .ControlerFirst
		       fild  word[xstate.ThumbRX]
		       fild  word[xstate.ThumbRY]
			fld  st1
		       fmul  st0,st0
			fld  st1
		       fmul  st0,st0
		      faddp  st1,st0
		       push  200000000.0
			fld  dword[esp]
			add  esp,4
		     fcomip  st1
		       fstp  st0
			 ja  .2
			fld  st0
		       fchs
			sub  esp,4*3
		       fstp  dword[esp+4*2]
		       fstp  dword[esp+4*1]
		       fstp  dword[esp+4*0]
		       call  CameraDirToGridDirection
		       test  eax,eax
			 jz  .ret
			sub  esp,32+8
		      movsd  qword[esp],xmm0
			jmp  .Fire
	      .2:      fstp  st0
		       fstp  st0
	      .ret:	ret

  .ControlerFirst:	cmp  byte[xstate.RightTrigger],10
			 jb  .ret
			sub  esp,32+8
		      movsd  xmm0,qword[CameraModeFirst.x]
		      movsd  qword[esp],xmm0
			jmp  .Fire

  .Keyboard:		cmp  dword[CameraMode],CAMERA_MODE_FIRST
			 je  .KeyboardFirst
			mov  eax,[KeyboardState.Buttons]
			shr  eax,4
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
			shr  eax,2
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
		       fldz
			sub  esp,4*3
		       fstp  dword[esp+4*2]
		       fstp  dword[esp+4*1]
		       fstp  dword[esp+4*0]
		       call  CameraDirToGridDirection
		       test  eax,eax
			 jz  .ret
			sub  esp,32+8
		      movsd  qword[esp],xmm0
			jmp  .Fire

  .KeyboardFirst:      test  dword[KeyboardState.Buttons],KEYBOARD_PLAYER_FIRE_UP
			 jz  .ret
			sub  esp,32+8
		      movsd  xmm0,qword[CameraModeFirst.x]
		      movsd  qword[esp],xmm0
			jmp  .Fire

  .Mouse:	       push  dword[MouseState.y]
		       push  dword[MouseState.x]
		       call  MouseCoordToGridCoor
			sub  esp,32+8
		      movsd  xmm1,qword[Player.px]
		      subps  xmm0,xmm1
		     movaps  xmm2,xmm0
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     sqrtss  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		      divps  xmm2,xmm0
		      movsd  qword[esp],xmm2

  .Fire:
			fld  dword[PlayerVel.y]
		       fmul  dword[const_f4_1d2]
			fld  dword[PlayerVel.x]
		       fmul  dword[const_f4_1d2]

			mov  esi,[SelectedWeapon]
			add  esi,4*3
			mov  ebx,[esi]
			add  esi,4
	 .4:	  ; position
			fld  dword[esi+4*0]
		       fmul  dword[esp+4*0]
			fld  dword[esi+4*1]
		       fmul  dword[esp+4*1]
		      fsubp  st1,st0
		       fadd  dword[Player.px]
		       fstp  dword[esp+8+OBJ1.px]
			fld  dword[esi+4*0]
		       fmul  dword[esp+4*1]
			fld  dword[esi+4*1]
		       fmul  dword[esp+4*0]
		      faddp  st1,st0
		       fadd  dword[Player.py]
		       fstp  dword[esp+8+OBJ1.py]
		 ; velocity
			fld  dword[esi+4*2]
		       fmul  dword[esp+4*0]
			fld  dword[esi+4*3]
		       fmul  dword[esp+4*1]
		      fsubp  st1,st0
		       fsub  st0,st2
			fst  dword[esp+8+OBJ1.vx]
			fld  dword[esi+4*2]
		       fmul  dword[esp+4*1]
			fld  dword[esi+4*3]
		       fmul  dword[esp+4*0]
		      faddp  st1,st0
		       fsub  st0,st2
			fst  dword[esp+8+OBJ1.vy]
		  ; speed
			fld  st1
		       fmul  st0,st0
			fld  st1
		       fmul  st0,st0
		      faddp  st1,st0
		      fsqrt
		       fdiv  st1,st0
		      fdivp  st2,st0
		       fstp  dword[esp+8+OBJ1.ny]
		       fstp  dword[esp+8+OBJ1.nx]
		  ; radi
			mov  dword[esp+8+OBJ1.hr],SHOT_HR
			mov  dword[esp+8+OBJ1.gr],SHOT_GR
		     movups  xmm3,dqword[esp+8+16*0]
		     movups  xmm4,dqword[esp+8+16*1]
		       call  ReleasePlayerShot
			add  esi,4*4
			sub  ebx,1
			jnz  .4


			add  esp,32+8
		       fstp  st0
		       fstp  st0
			ret


align 4

Weapon1:   dd Weapon1
	   dd Weapon2
	   dd 5
	   dd 3
	   dd 10.0,+12.0
	   dd 18.0,+0.5
	   dd 10.0,-12.0
	   dd 18.0,-0.5
	   dd 20.0,0.0
	   dd 18.0,0.0

Weapon2:   dd Weapon1
	   dd Weapon3
	   dd 4
	   dd 1
	   dd 15.0,0.0
	   dd 12.0,0.0

Weapon3:   dd Weapon2
	   dd Weapon4
	   dd 4
	   dd 2
	   dd 20.0,+6.0
	   dd 12.0,0.0
	   dd 20.0,-6.0
	   dd 12.0,0.0

Weapon4:   dd Weapon3
	   dd Weapon5
	   dd 3
	   dd 3
	   dd 10.0,+12.0
	   dd 13.0,0.0
	   dd 10.0,-12.0
	   dd 13.0,0.0
	   dd 20.0,0.0
	   dd 13.0,0.0

Weapon5:   dd Weapon4
	   dd Weapon6
	   dd 8
	   dd 4
	   dd 12.0,+9.0
	   dd 7.98040800203, +0.5595427787
	   dd 18.0,-1.0
	   dd 7.98040800203, -0.5595427787
	   dd 18.0,+1.0
	   dd 7.98040800203, +0.5595427787
	   dd 12.0,-9.0
	   dd 7.98040800203, -0.5595427787


Weapon6:   dd Weapon5
	   dd Weapon7
	   dd 4
	   dd 4
	   dd 10.0,+17.0
	   dd 14.0,0.0
	   dd 20.0,+7.0
	   dd 14.0,0.0
	   dd 20.0,-7.0
	   dd 14.0,0.0
	   dd 10.0,-17.0
	   dd 14.0,0.0



Weapon7:   dd Weapon6
	   dd Weapon8
	   dd 8
	   dd 5
	   dd 20.0,0.0
	   dd 8.0,0.0
	   dd 18.0,+6.0
	   dd 7.99000208316, +0.3998333542
	   dd 18.0,-6.0
	   dd 7.99000208316, -0.3998333542
	   dd 12.0,+9.0
	   dd 7.98040800203, +0.5595427787
	   dd 12.0,-9.0
	   dd 7.98040800203, -0.5595427787


Weapon8:   dd Weapon7
	   dd Weapon8
	   dd 4
	   dd 5
	   dd 10.0,+18.0
	   dd 14.0,0.0
	   dd 15.0,+9.0
	   dd 14.0,0.0
	   dd 20.0,0.0
	   dd 14.0,0.0
	   dd 15.0,-9.0
	   dd 14.0,0.0
	   dd 10.0,-18.0
	   dd 14.0,0.0





		      align  16
Player_Update:
		       call  GetRand.word
			xor  ecx,ecx
		       test  eax,0x00F
		       setz  cl
			add  ecx,dword[PlayerParColor]
			and  ecx,63
			mov  dword[PlayerParColor],ecx


		       test  dword[KeyboardState.Buttons],KEYBOARD_PLAYER_MOVE_UP or KEYBOARD_PLAYER_MOVE_DOWN or KEYBOARD_PLAYER_MOVE_LEFT or KEYBOARD_PLAYER_MOVE_RIGHT
			jnz  .Keyboard


    .Controler: 	cmp  dword[CameraMode],CAMERA_MODE_FIRST
			jne  @f

		      movsx  eax,word[xstate.ThumbRX]
			cdq
			xor  eax,edx
			sub  eax,edx
			cmp  eax,10000
			 jb  @f
			fld  dword[CameraModeFirst.y]
			fld  dword[CameraModeFirst.x]
		       fild  word[xstate.ThumbRX]
		       push  PLAYER_TURN_SPEED_CONTROLER
			fld  dword[esp]
			add  esp,4
		      fmulp  st1,st0
		    fsincos
			fld  st2
		       fmul  st0,st1
			fld  st4
		       fmul  st0,st3
		      fsubp  st1,st0
		       fstp  st5
		      fmulp  st3,st0
		      fmulp  st1,st0
		      faddp  st1,st0
			fld  st1
		       fmul  st0,st0
			fld  st1
		       fmul  st0,st0
		      faddp  st1,st0
		      fsqrt
		       fdiv  st1,st0
		      fdivp  st1,st0
		       fstp  dword[CameraModeFirst.y]
		       fstp  dword[CameraModeFirst.x]
		@@:    fild  word[xstate.ThumbLX]
		       fild  word[xstate.ThumbLY]
			fld  st1
		       fmul  st0,st0
			fld  st1
		       fmul  st0,st0
		      faddp  st1,st0
		       push  200000000.0
			fld  dword[esp]
		     fcomip  st1
		      fsqrt
			mov  dword[esp],PLAYER_SPEED
			fld  dword[esp]
			mov  dword[esp],0x08000
		       fild  dword[esp]
		      fdivp  st1,st0
		      fmulp  st1,st0
		       fstp  dword[esp]
		      movsd  xmm1,[esp]
		     shufps  xmm1,xmm1,0
			lea  esp,[esp+4]
			fld  st0
		       fchs
			 jb  @f
		       fldz
			fst  st1
			fst  st2
		       fstp  st3
		 @@:	sub  esp,4*3
		       fstp  dword[esp+4*2]
		       fstp  dword[esp+4*1]
		       fstp  dword[esp+4*0]
		       call  CameraDirToGridDirection
		      mulps  xmm1,xmm0
		       test  eax,eax
			 jz  Player_Update_Bomb
		      movsd  xmm2,qword[Player.px]
		      addps  xmm2,xmm1
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		      maxps  xmm2,xmm6
		      minps  xmm2,xmm7
		      movsd  qword[Player.px],xmm2
		      movsd  qword[Player.nx],xmm0
		      movsd  qword[Player.vx],xmm1
			jmp  Player_Update_Tail


    .Keyboard:
			cmp  dword[CameraMode],CAMERA_MODE_FIRST
			jne  @f
			fld  dword[CameraModeFirst.y]
			fld  dword[CameraModeFirst.x]
			mov  eax,[KeyboardState.Buttons]
			shr  eax,4
			and  eax,3
			fld  dword[Lookup+4*eax]
		       push  PLAYER_TURN_SPEED
			fld  dword[esp]
			add  esp,4
		      fmulp  st1,st0
		    fsincos
			fld  st2
		       fmul  st0,st1
			fld  st4
		       fmul  st0,st3
		      fsubp  st1,st0
		       fstp  st5
		      fmulp  st3,st0
		      fmulp  st1,st0
		      faddp  st1,st0
			fld  st1
		       fmul  st0,st0
			fld  st1
		       fmul  st0,st0
		      faddp  st1,st0
		      fsqrt
		       fdiv  st1,st0
		      fdivp  st1,st0
		       fstp  dword[CameraModeFirst.y]
		       fstp  dword[CameraModeFirst.x]
	      @@:
			sub  esp,4*3
			mov  eax,dword[KeyboardState.Buttons]
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
			shr  eax,2
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
			fld  st0
		       fchs
			mov  eax,PLAYER_SPEED
		       movd  xmm1,eax
		     shufps  xmm1,xmm1,0
		       fstp  dword[esp+4*2]
		       fstp  dword[esp+4*1]
		       fstp  dword[esp+4*0]
		       call  CameraDirToGridDirection
		       test  eax,eax
			 jz  Player_Update_Bomb
		      mulps  xmm1,xmm0
		      movsd  xmm2,qword[Player.px]
		      addps  xmm2,xmm1
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		      maxps  xmm2,xmm6
		      minps  xmm2,xmm7
		      movsd  qword[Player.px],xmm2
		      movsd  qword[Player.nx],xmm0
		      movsd  qword[Player.vx],xmm1

Player_Update_Tail:
		      movss  xmm6,dword[const_f4_18]
		      movss  xmm7,dword[TailScale]
		     shufps  xmm6,xmm6,0
		     shufps  xmm7,xmm7,0

			mov  ecx,2
	       .7:	mov  edx,1
	       .6:	cmp  [ParCount],PAR_MAXCOUNT
			jae  .done
			mov  edi,[ParCount]
			shl  edi,5
			add  edi,ParTable
			add  [ParCount],1
		      movsd  xmm0,qword[Player.px]
		      movsd  xmm1,qword[Player.nx]
		      mulps  xmm1,xmm6
		      subps  xmm0,xmm1
		      movsd  qword[edi+PAR.px],xmm0
			mov  dword[edi+PAR.pz],0
		       call  GetRand.word
		      movsx  eax,ax
			sar  eax,cl
		   cvtsi2ss  xmm0,eax
		      movss  dword[edi+PAR.vx],xmm0
		       call  GetRand.word
		      movsx  eax,ax
			sar  eax,cl
		   cvtsi2ss  xmm0,eax
		      movss  dword[edi+PAR.vy],xmm0
		      movsd  xmm0,qword[edi+PAR.vx]
		      movsd  xmm1,qword[Player.nx]
		      addps  xmm1,xmm1
		      addps  xmm1,xmm1
		      mulps  xmm0,xmm7
		      subps  xmm0,xmm1
		     movaps  dqword[edi+PAR.vx],xmm0
			mov  eax,[PlayerParColor]
			shl  eax,4
			mov  dword[edi+PAR.col],eax
		      mulps  xmm2,xmm0
			sub  edx,1
			jns  .6
			sub  ecx,1
			jnz  .7
      .done:




Player_Update_Bomb:
			cmp  dword[Bomb.ct],0
			jle  .mei

		       push  15.0
		      movss  xmm0,dword[esp]
			add  esp,4
		      addss  xmm0,dword[Bomb.r]
		      movss  dword[Bomb.r],xmm0

		      movss  xmm5,dword[Bomb.r]
		      mulss  xmm5,xmm5
		      movsd  xmm6,qword[Bomb.px]
		       call  Windmill_DestroyRadius
		       call  Diamond_DestroyRadius
		       call  Weaver_DestroyRadius
		       call  Wander_DestroyRadius
		       call  Snake_DestroyRadius

			sub  dword[Bomb.ct],1
			jmp  .done

		.mei:
		       test  dword[KeyboardState.Buttons],KEYBOARD_PLAYER_BOMB
			jnz  @f
		       test  word[xstate.Buttons],XINPUT_GAMEPAD_A
			 jz  .done
		       test  word[xstate2.Buttons],XINPUT_GAMEPAD_A
			jnz  .done
		@@:   movsd  xmm0,qword[Player.px]
		      movsd  qword[Bomb.px],xmm0
			mov  dword[Bomb.r],60.0
			mov  dword[Bomb.ct],60
			and  dword[KeyboardState.Buttons],not KEYBOARD_PLAYER_BOMB

		.done:




Player_Update_CheckEnemyCollision:

			mov  dword[PlayerAliveQ],-1

			mov  eax,dword[PlayerShieldCount]
			sub  eax,1
			mov  dword[PlayerShieldCount],eax
			 js  .noshield

			mov  eax,PLAYER_SHIELD_R
		       movd  xmm5,eax
		      mulss  xmm5,xmm5
		      movsd  xmm6,qword[Player.px]
		       call  Windmill_DestroyRadius
		       call  Diamond_DestroyRadius
		       call  Weaver_DestroyRadius
		       call  Wander_DestroyRadius
		       call  Snake_DestroyRadius

			jmp  .ret

	      .noshield:
			mov  dword[PlayerShieldCount],0

		       push  PLAYER_HR
		       push  WINDMILL_HR
		       push  WindmillTable
		       push  dword[WindmillCount]
		       call  Enemy_PlayerIntersection
			mov  esi,WindmillCount
			mov  edi,WindmillTable
			mov  ecx,32
		       test  eax,eax
			jnz  .col

		       push  PLAYER_HR
		       push  DIAMOND_HR
		       push  DiamondTable
		       push  dword[DiamondCount]
		       call  Enemy_PlayerIntersection
			mov  esi,DiamondCount
			mov  edi,DiamondTable
			mov  ecx,32
		       test  eax,eax
			jnz  .col

		       push  PLAYER_HR
		       push  WEAVER_HR
		       push  WeaverTable
		       push  dword[WeaverCount]
		       call  Enemy_PlayerIntersection
			mov  esi,WeaverCount
			mov  edi,WeaverTable
			mov  ecx,32
		       test  eax,eax
			jnz  .col

		       push  PLAYER_HR
		       push  WANDER_HR
		       push  WanderTable
		       push  dword[WanderCount]
		       call  Enemy_PlayerIntersection
			mov  esi,WanderCount
			mov  edi,WanderTable
			mov  ecx,32
		       test  eax,eax
			jnz  .col

		       call  Snake_PlayerIntersection
			mov  esi,SnakeCount
			mov  edi,SnakeTable
			mov  ecx,sizeof.SNAKE
		       test  eax,eax
			jnz  .col


			jmp  .ret
		.col:

			mov  dword[PlayerAliveQ],0

			sub  esp,ecx
		       push  esi ; count
		       push  edi ; table
		       push  ecx

			mov  esi,eax
			lea  edi,[esp+4*3]
			shr  ecx,2
		  rep movsd

		      movsd  xmm5,qword[Corner]
		      mulps  xmm5,xmm5
		     haddps  xmm5,xmm5
		      movsd  xmm6,qword[Player.px]
		       call  Windmill_DestroyRadius
		       call  Diamond_DestroyRadius
		       call  Weaver_DestroyRadius
		       call  Wander_DestroyRadius
		       call  Snake_DestroyRadius

			pop  ecx
			pop  edi
			pop  esi
			mov  dword[esi],1
			mov  esi,esp
			mov  edx,ecx
			shr  ecx,2
		  rep movsd
			add  esp,edx


	     .ret:
			ret



		      align  16
Enemy_PlayerIntersection:
		       ;push  player hr
		       ;push  enemy hr
		       ;push  enemytable
		       ;push  enemy count

			; out: eax = 0 if no hit
			; otherwise eax = address of hit diamond

		      movss  xmm1,dword[esp+4*4]
		      addss  xmm1,dword[esp+4*3]
		      movsd  xmm2,qword[Player.px]
			xor  eax,eax
			mov  esi,dword[esp+4*2]
			mov  ebx,dword[esp+4*1]
			jmp  .2
	    .1:       movsd  xmm0,qword[esi] ; .px
		      subps  xmm0,xmm2
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm1
		      cmovb  eax,esi
			add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			ret  4*4








		      align  16
Player_Draw:
		     invoke  glColor4f,1.0,1.0,1.0,1.0
		     invoke  glBindTexture,GL_TEXTURE_2D,[PlayerTexture]
		     invoke  glBegin,GL_QUADS
			mov  esi,Player+CopyOffset
			lea  edi,[esi+8]
		       call  PrintOBJ1
		     invoke  glEnd

			cmp  dword[Bomb.ct],0
			jle  .3
		     invoke  glBindTexture,GL_TEXTURE_2D,[BombTexture]
		     invoke  glBegin,GL_QUADS
			xor  ebx,ebx
	      .1:	mov  esi,2
	      .2:	lea  ecx,[ebx+1]
			and  ecx,0x0FF
		      movsd  xmm0,qword[Bomb.px]
		      movss  xmm1,dword[Bomb.r]
		      addss  xmm1,dword[.a+4*esi]
		     shufps  xmm1,xmm1,0
		       movq  xmm6,qword[AngleTable+8*ebx]
		       movq  xmm7,qword[AngleTable+8*ecx]
		      mulps  xmm6,xmm1
		      mulps  xmm7,xmm1
		      addps  xmm6,xmm0
		      addps  xmm7,xmm0
		       call  LineDrawNoEnd
			sub  esi,1
			jns  .2
			add  ebx,1
			cmp  ebx,256
			 jb  .1
		     invoke  glEnd
	      .3:

			mov  eax,dword[PlayerShieldCount]
			cmp  eax,0
			jle  .4
			cmp  eax,32*3
			jae  .6
		       test  eax,0x08
			jnz .4
	      .6:
		     invoke  glBindTexture,GL_TEXTURE_2D,[BombTexture]
		     invoke  glBegin,GL_QUADS
			xor  ebx,ebx
	      .5:	lea  ecx,[ebx+16]
			and  ecx,0x0FF
		      movsd  xmm0,qword[Player.px]
			mov  eax,PLAYER_SHIELD_R
		       movd  xmm1,eax
		     shufps  xmm1,xmm1,0
		       movq  xmm6,qword[AngleTable+8*ebx]
		       movq  xmm7,qword[AngleTable+8*ecx]
		      mulps  xmm6,xmm1
		      mulps  xmm7,xmm1
		      addps  xmm6,xmm0
		      addps  xmm7,xmm0
		       call  LineDrawNoEnd
			add  ebx,16
			cmp  ebx,256
			 jb  .5
		     invoke  glEnd
	      .4:




			ret



align 4
.a dd 60.0,30.0,0.0



















