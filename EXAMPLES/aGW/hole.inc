

struct HOLE
   px	 dd ?
   py	 dd ?
	 dd ?
	 dd ?
   sz	 dd ?
	 dd ?
	 dd ?
	 dd ?
ends

struct LGEXP
   px	dd ?
   py	dd ?
   vx	dd ?	  ; velocity
   vy	dd ?	  ;
   sx	dd ?	; vx^2+vy^2
   sy	dd ?	; vx^2+vy^2
	dd ?	;
	dd ?
	dd ?
ends

		      align  16
Windmill_Spawn:
			mov  esi,WindmillTable
			mov  eax,dword[WindmillCount]
			cmp  eax,5
			 ja  .ret
			add  dword[WindmillCount],1
			shl  eax,5
			add  esi,eax
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_WIDTH
			div  ecx
		       push  edx
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_HEIGHT
			div  ecx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WINDMILL.py],xmm0
			pop  edx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+WINDMILL.px],xmm0
		       call  GetRand.word
			and  eax,0x0FF
		      movsd  xmm0,[AngleTable+8*eax]
		      movss  xmm1,[WindmillSpeed]
		     shufps  xmm1,xmm1,0
		      mulps  xmm0,xmm1
		      movsd  qword[esi+WINDMILL.vx],xmm0
			mov  dword[esi+WINDMILL.angle],eax
			mov  dword[esi+WINDMILL.gr],WINDMILL_GR
	    .ret:	ret



		      align  16
Windmill_Update:
		       push  WINDMILL_HR
			mov  esi,WindmillTable
			mov  edi,esi
			mov  ebx,[WindmillCount]
			jmp  .2
	    .1:       movsd  xmm4,qword[esi+WINDMILL.px]
		      movsd  xmm5,qword[esi+WINDMILL.vx]
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
			mov  edx,dword[esi+WINDMILL.angle]
			add  edx,5
		   cmpnltps  xmm4,xmm6
		       orps  xmm4,[const_f4x4_manmask]
		      andps  xmm5,xmm4
		      movsd  xmm4,qword[esi+WINDMILL.px]
		      movss  xmm2,dword[esp]
		   cmpnltps  xmm4,xmm7
		      andps  xmm4,[const_f4x4_signmask]
		       orps  xmm5,xmm4
		      movsd  xmm4,qword[esi+WINDMILL.px]
		      addps  xmm4,xmm5
		       call  CheckShotIntersection
			 jc  .ex
		      movsd  qword[edi+WINDMILL.px],xmm4
		      movsd  qword[edi+WINDMILL.vx],xmm5
			mov  dword[edi+WINDMILL.angle],edx
			mov  dword[edi+WINDMILL.gr],WINDMILL_GR
			add  edi,32
			jmp  .0
	    .ex:       call  Winmill_ReleaseExplosion
	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,WindmillTable
			shr  edi,5
			mov  [WindmillCount],edi
			add  esp,4
			ret


		      align  16
Windmill_Draw:
			sub  esp,8
			mov  edi,esp
		     invoke  glColor3f,1.0,1.0,1.0
		     invoke  glBindTexture,GL_TEXTURE_2D,[WindmillTexture]
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WindmillCount+CopyOffset]
			mov  esi,WindmillTable+CopyOffset
			jmp  .4
	      .3:	mov  eax,[esi+WINDMILL.angle]
			and  eax,0x0FF
		      movsd  xmm0,[AngleTable+8*eax]
		      movsd  [edi],xmm0
		       call  PrintOBJ1
			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd

			add  esp,8
			ret


		      align  16
Winmill_ReleaseExplosion:
			; xmm4: position of object (preserved)
		       push  WINDMILL_LGEXP_PARTICLE
	.1:		mov  edx,[WindmillLgExpCount]
			cmp  edx,MAX_WINDMILL_LGEXP_COUNT
			jae  .sm
			lea  ecx,[edx+1]
			shl  edx,5
			add  edx,WindmillLgExpTable
			mov  [WindmillLgExpCount],ecx
		       call  GetRand.word
			mov  ecx,eax
			and  ecx,0x0FF
		      movsd  xmm0,[AngleTable+8*ecx]
			and  eax,0x07FFF
			add  eax,0x08000
		      movss  xmm2,[WindmillLgExpMinR]
		     shufps  xmm2,xmm2,0
		      mulps  xmm2,xmm0
		      addps  xmm2,xmm4
		   cvtsi2ss  xmm1,eax
		      mulss  xmm1,[WindmillLgExpScale]
		     shufps  xmm1,xmm1,0
		      mulps  xmm0,xmm1
		      mulps  xmm1,xmm1
		      movsd  qword[edx+LGEXP.px],xmm2
		      movsd  qword[edx+LGEXP.vx],xmm0
		      movsd  qword[edx+LGEXP.sx],xmm1
			sub  dword[esp],1
			jnz  .1
	.sm:		mov  dword[esp],WINDMILL_SMEXP_PARTICLE
	.2:		mov  edx,[WindmillSmExpCount]
			cmp  edx,MAX_WINDMILL_SMEXP_COUNT
			jae  .ret
			lea  ecx,[edx+1]
			shl  edx,5
			add  edx,WindmillSmExpTable
			mov  [WindmillSmExpCount],ecx
		       call  GetRand.word
			mov  ecx,eax
			and  ecx,0x0FF
		      movsd  xmm0,[AngleTable+8*ecx]
			mov  ecx,eax
			shr  ecx,8
		      movsd  xmm3,[AngleTable+8*ecx]
			mov  ecx,eax
			shr  ecx,11
			add  ecx,32
			and  eax,0x07FFF
			add  eax,0x08000
		   cvtsi2ss  xmm1,eax
		      mulss  xmm1,[WindmillSmExpScale]
		     shufps  xmm1,xmm1,0
		      mulps  xmm1,xmm0
		      addps  xmm1,xmm7
		      movsd  qword[edx+OBJ2.px],xmm4
		      movsd  qword[edx+OBJ2.nx],xmm3
		      movsd  qword[edx+OBJ2.vx],xmm1
			mov  dword[edx+OBJ2.ct],ecx
			mov  dword[edx+OBJ2.gr],WINDMILL_SMEXP_GR
			sub  dword[esp],1
			jnz  .2
	.ret:		add  esp,4
			ret





		      align  16
WindmillSmExp_Update:
			mov  ebx,[WindmillSmExpCount]
			mov  esi,WindmillSmExpTable
			mov  edi,esi
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+OBJ2.px]
		      movsd  xmm1,qword[esi+OBJ2.nx]
		      movsd  xmm2,qword[esi+OBJ2.vx]
			mov  eax,dword[esi+OBJ2.ct]
			mov  ecx,dword[esi+OBJ2.gr]
		      addps  xmm0,xmm2
			sub  eax,1
			 jz  .0
		   cmpnltps  xmm0,xmm6
		       orps  xmm0,[const_f4x4_manmask]
		      andps  xmm1,xmm0
		      andps  xmm2,xmm0
		      movsd  xmm0,qword[esi+OBJ2.px]
		   cmpnltps  xmm0,xmm7
		      andps  xmm0,[const_f4x4_signmask]
		       orps  xmm1,xmm0
		       orps  xmm2,xmm0
		      movsd  xmm0,qword[esi+OBJ2.px]
		      addps  xmm0,xmm2
		      movsd  qword[edi+OBJ2.px],xmm0
		      movsd  qword[edi+OBJ2.nx],xmm1
		      movsd  qword[edi+OBJ2.vx],xmm2
			mov  dword[edi+OBJ2.ct],eax
			mov  dword[edi+OBJ2.gr],ecx
			add  edi,32
	      .0:	add  esi,32
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,WindmillSmExpTable
			shr  edi,5
			mov  [WindmillSmExpCount],edi
			ret


		      align  16
WindmillSmExp_Draw:
		     invoke  glColor3f,1.0,1.0,1.0
		     invoke  glBindTexture,GL_TEXTURE_2D,[WindmillPartTexture]
		     invoke  glBegin,GL_QUADS
			mov  ebx,[WindmillSmExpCount+CopyOffset]
			mov  esi,WindmillSmExpTable+CopyOffset
			jmp  .4
	      .3:	lea  edi,[esi+8]
		       call  PrintOBJ1
			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret





		      align  16
WindmillLgExp_Update:
		       push  LGEXP_DAMPING
		       push  LGEXP_DAMPING
		      movsd  xmm5,qword[esp]
			add  esp,8
			mov  ebx,[WindmillLgExpCount]
			mov  esi,WindmillLgExpTable
			mov  edi,esi
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+LGEXP.px]
		      movsd  xmm2,qword[esi+LGEXP.vx]
		      addps  xmm0,xmm2
		     movaps  xmm3,xmm2
		      mulps  xmm3,xmm2
		     haddps  xmm3,xmm3
		     comiss  xmm3,dword[LgExpChop]
			 jb  .0
		   cmpnltps  xmm0,xmm6
		       orps  xmm0,[const_f4x4_manmask]
		      andps  xmm2,xmm0
		      movsd  xmm0,qword[esi+OBJ2.px]
		   cmpnltps  xmm0,xmm7
		      andps  xmm0,[const_f4x4_signmask]
		       orps  xmm2,xmm0
		      movsd  xmm0,qword[esi+OBJ2.px]
		      addps  xmm0,xmm2
		      mulps  xmm2,xmm5
		      movsd  qword[edi+LGEXP.px],xmm0
		      movsd  qword[edi+LGEXP.vx],xmm2
		      movss  dword[edi+LGEXP.sx],xmm3
		      movss  dword[edi+LGEXP.sy],xmm3
			add  edi,32
	      .0:	add  esi,32
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,WindmillLgExpTable
			shr  edi,5
			mov  [WindmillLgExpCount],edi
			ret



		      align  16
WindmillLgExp_Draw:
		     invoke  glBindTexture,GL_TEXTURE_2D,[LongExplosionTexture]
		     invoke  glBegin,GL_QUADS
		     invoke  glColor3f,0.90,0.4,0.95
			mov  ebx,[WindmillLgExpCount+CopyOffset]
			mov  esi,WindmillLgExpTable+CopyOffset
			jmp  .4
	      .3:	lea  edi,[esi+8]
		       call  PrintLgExp
			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret



		      align  16
PrintLgExp:	       push  0x80000000
		       push  0
		      movsd  xmm2,[esp]
		       push  LGEXP_GR
		       push  LGEXP_GR
		      movsd  xmm6,[esp]
		      movsd  xmm0,qword[esi+LGEXP.vx]
		      movsd  xmm1,qword[esi+LGEXP.sx]
		    rsqrtps  xmm1,xmm1
		      xorps  xmm1,xmm2
		      mulps  xmm6,xmm0
		     movaps  xmm7,xmm6
		      mulps  xmm7,dqword[const_f4v4_1d2]
		     shufps  xmm6,xmm6,011110001b
		      mulps  xmm6,xmm1
			add  esp,16


		     invoke  glTexCoord2f,0.0,0.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi+LGEXP.px]
		      addps  xmm0,xmm7
		      addps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,1.0,0.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi+LGEXP.px]
		      addps  xmm0,xmm7
		      subps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,1.0,1.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi+LGEXP.px]
		      subps  xmm0,xmm7
		      subps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f


		     invoke  glTexCoord2f,0.0,1.0
			sub  esp,4*2
		      movsd  xmm0,qword[esi+LGEXP.px]
		      subps  xmm0,xmm7
		      addps  xmm0,xmm6
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

			ret




