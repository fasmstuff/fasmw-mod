
struct BLACKHOLE
   px	 dd ?
   py	 dd ?
   vx	 dd ?
   vy	 dd ?
   sp	 dd ?
	 dd ?
   hth	 dd ?
   gr	 dd ?
ends

struct BLACKHOLESPAWN
   px	 dd ?
   py	 dd ?
   sp	 dd ?
   ct	 dd ?
ends


		      align  16
Blackhole_Spawn:
			mov  esi,BlackholeSpawnTable
			mov  eax,dword[FrameCount]
		       test  eax,0xFF
			jnz  .ret
			mov  eax,dword[BlackholeSpawnCount]
			cmp  eax,BLACKHOLE_SPAWN_MAXCOUNT
			jae  .ret
			add  dword[BlackholeSpawnCount],1
			shl  eax,5
			add  esi,eax
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_WIDTH
			div  ecx
		       push  edx
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_HEIGHT
			div  ecx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+BLACKHOLESPAWN.py],xmm0
			pop  edx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+BLACKHOLESPAWN.px],xmm0
			mov  eax,dword[FrameCount]
			mov  dword[esi+BLACKHOLESPAWN.sp],eax
			mov  dword[esi+BLACKHOLESPAWN.ct],64
	    .ret:	ret



		      align  16
BlackholeSpawn_Update:
			mov  ebx,[BlackholeSpawnCount]
			mov  esi,BlackholeSpawnTable
			mov  edi,esi
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+BLACKHOLESPAWN.px]
			mov  eax,dword[esi+BLACKHOLESPAWN.sp]
			mov  edx,dword[esi+BLACKHOLESPAWN.ct]
			sub  edx,1
			 js  .0
	      .4:     movsd  qword[edi+BLACKHOLESPAWN.px],xmm0
			mov  dword[edi+BLACKHOLESPAWN.sp],eax
			mov  dword[edi+BLACKHOLESPAWN.ct],edx
			add  edi,16
			jmp  .3
	      .0:	mov  ecx,dword[BlackholeCount]
			 or  edx,-1
		      xorps  xmm1,xmm1
			cmp  ecx,BLACKHOLE_MAXCOUNT
			jae  .4
			add  dword[BlackholeCount],1
			shl  ecx,5
			add  ecx,BlackholeTable
		      movsd  qword[ecx+BLACKHOLE.px],xmm0
		      movsd  qword[ecx+BLACKHOLE.vx],xmm1
			mov  dword[ecx+BLACKHOLE.sp],eax
			mov  dword[ecx+BLACKHOLE.hth],BLACKHOLE_MAXHEALTH
			mov  dword[ecx+BLACKHOLE.gr],BLACKHOLE_GR
	      .3:	add  esi,16
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,BlackholeSpawnTable
			shr  edi,4
			mov  [BlackholeSpawnCount],edi
			ret






		      align  16
Blackhole_Update:

			xor  esi,esi
			mov  eax,BlackholeTable
			jmp  .16
	     .15:	mov  edi,esi
			mov  ecx,esi
			shl  ecx,5
			add  ecx,BlackholeTable
	     .17:
		      movsd  xmm0,qword[eax+BLACKHOLE.px]
		      movsd  xmm1,qword[ecx+BLACKHOLE.px]
		      subps  xmm0,xmm1
		     movaps  xmm2,xmm0
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     shufps  xmm0,xmm0,0

		     movaps  xmm1,xmm0
		    cmpltps  xmm1,dqword[.r]
		      andps  xmm1,dqword[.a]
		      addps  xmm1,dqword[.b]
		      mulps  xmm2,xmm1

		      maxps  xmm0,dqword[.e]
		     movaps  xmm1,xmm0
		      mulps  xmm1,xmm0
		      mulps  xmm1,xmm0
		    rsqrtps  xmm0,xmm1

		      mulps  xmm0,xmm2

		      movsd  xmm1,qword[eax+BLACKHOLE.vx]
		      movsd  xmm2,qword[ecx+BLACKHOLE.vx]
		      addps  xmm1,xmm0
		      subps  xmm2,xmm0
		      movsd  qword[eax+BLACKHOLE.vx],xmm1
		      movsd  qword[ecx+BLACKHOLE.vx],xmm2

			add  edi,1
			add  ecx,32
			cmp  edi,[BlackholeCount]
			 jb  .17
			add  eax,32
	     .16:	add  esi,1
			cmp  esi,[BlackholeCount]
			 jb  .15




			sub  esp,4
			mov  esi,BlackholeTable
			mov  edi,esi
			mov  ebx,[BlackholeCount]
			jmp  .2
	    .1:

	    .active:

	     .nact:
		      movsd  xmm4,qword[esi+BLACKHOLE.px]
		      movsd  xmm5,qword[esi+BLACKHOLE.vx]
			mov  edx,dword[esi+BLACKHOLE.hth]
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		   cmpnltps  xmm4,xmm6
		       orps  xmm4,[const_f4x4_manmask]
		      andps  xmm5,xmm4
		      movsd  xmm4,qword[esi+WINDMILL.px]
		      movss  xmm2,dword[esi+BLACKHOLE.gr]
		      mulss  xmm2,xmm2
		      movss  dword[esp],xmm2
		   cmpnltps  xmm4,xmm7
		      andps  xmm4,[const_f4x4_signmask]
		       orps  xmm5,xmm4
		      movsd  xmm4,qword[esi+BLACKHOLE.px]
		      addps  xmm4,xmm5
		     movaps  xmm6,dqword[esi+BLACKHOLE.sp]
			mov  edx,dword[esi+BLACKHOLE.hth]
		       call  CheckShotIntersection
			jnc  .nex
	    .ex:       call  Blackhole_ReleaseExplosion
			sub  edx,1
			 js  .3
	    .nex:     movsd  qword[edi+BLACKHOLE.px],xmm4
		      mulps  xmm5,dqword[.d]
		      movsd  qword[edi+BLACKHOLE.vx],xmm5
		     movaps  dqword[edi+BLACKHOLE.sp],xmm6
			mov  dword[edi+BLACKHOLE.hth],edx
			add  edi,32
			jmp  .0
	    .3:
		       push  edi esi ebx
			mov  eax,200000.0
		       movd  xmm5,eax
		      movsd  xmm6,xmm4
		       call  Windmill_DestroyRadius
		       call  Diamond_DestroyRadius
		       call  Weaver_DestroyRadius
		       call  Wander_DestroyRadius
		       call  Snake_DestroyRadius
			pop  ebx esi edi

	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,BlackholeTable
			shr  edi,5
			mov  [BlackholeCount],edi
			add  esp,4
			ret


align 16
.e: dd 0.01,0.01,0.01,0.01
.r  dd 10000.0,10000.0,10000.0,10000.0
.a: dd +80.00,+80.00,+80.00,+80.00
.b: dd -40.00,-40.00,-40.00,-40.00
.d  dd 0.99,0.99,0.99,0.99




		      align  16
Blackhole_ReleaseExplosion:



			; xmm4: position of object (preserved)
		       push  edx
		       push  dword[PlayerParColor]
		       push  0 0 0 0 0 0
		       push  BLACKHOLE_PAR_MINR
		       push  BLACKHOLE_PAR_DELTAR
		       push  BLACKHOLE_PAR_MINS
		       push  BLACKHOLE_PAR_DELTAS
		       push  BLACKHOLE_PAR_COUNT
		       call  ReleaseExplosion
			pop  edx
			ret







DrawPatch:	; push  py
		; push  px
		; push  y
		; push  x
		; ret address
		; x
		; -y
			sub  esp,4*2
			fld  dword[esp+4*3]
		       fstp  dword[esp+4*1]
			fld  dword[esp+4*4]
		       fchs
		       fstp  dword[esp+4*0]

		      invoke  glTexCoord2f,0.0,0.0
		      movsd  xmm0,qword[esp+4*5]
		      movsd  xmm6,qword[esp+4*3]
		      movsd  xmm7,qword[esp+4*0]
		      addps  xmm0,xmm7
		      addps  xmm0,xmm6
			sub  esp,4*2
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,1.0,0.0
		      movsd  xmm0,qword[esp+4*5]
		      movsd  xmm6,qword[esp+4*3]
		      movsd  xmm7,qword[esp+4*0]
		      addps  xmm0,xmm7
		      subps  xmm0,xmm6
			sub  esp,4*2
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,1.0,1.0
		      movsd  xmm0,qword[esp+4*5]
		      movsd  xmm6,qword[esp+4*3]
		      movsd  xmm7,qword[esp+4*0]
		      subps  xmm0,xmm7
		      subps  xmm0,xmm6
			sub  esp,4*2
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

		     invoke  glTexCoord2f,0.0,1.0
		      movsd  xmm0,qword[esp+4*5]
		      movsd  xmm6,qword[esp+4*3]
		      movsd  xmm7,qword[esp+4*0]
		      subps  xmm0,xmm7
		      addps  xmm0,xmm6
			sub  esp,4*2
		      movsd  qword[esp],xmm0
		     invoke  glVertex2f

			add  esp,4*2
			ret  4*4



		      align  16
Blackhole_Draw:
		     invoke  glBindTexture,GL_TEXTURE_2D,[BlackholeTexture]
		     invoke  glBegin,GL_QUADS
			mov  ebx,[BlackholeCount+CopyOffset]
			mov  esi,BlackholeTable+CopyOffset
			jmp  .4
	      .3:



			mov  eax,[FrameCount]
			add  eax,dword[esi+BLACKHOLE.sp]
			shr  eax,4
			and  eax,64-1
			shl  eax,4
			add  eax,ColorTable3
		     invoke  glColor4fv,eax



		       push  16
		       push  0
		.5:
		       fild  dword[esp+4*0]
		       fild  dword[esp+4*1]
		      fdivp  st1,st0
		       fmul  dword[const_f4_2pi]
		    fsincos

			sub  esp,4*4

			fld  st1
		       fmul  dword[.a]
		       fadd  dword[esi+BLACKHOLE.py]
		       fstp  dword[esp+4*3]

			fld  st0
		       fmul  dword[.a]
		       fadd  dword[esi+BLACKHOLE.px]
		       fstp  dword[esp+4*2]

			fld  st1
		       fmul  dword[.b]
		       fstp  dword[esp+4*1]

			fld  st0
		       fmul  dword[.b]
		       fstp  dword[esp+4*0]

		       fstp  st0
		       fstp  st0

		       call  DrawPatch

			mov  eax,dword[esp]
			add  eax,1
			mov  dword[esp],eax
			cmp  eax,dword[esp+4]

			 jb  .5

			add  esp,8

			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


align 16
.c  dd 1.5,1.5,1.5,1.5
.a: dd 48.0
.b: dd 16.0



		      align  16
BlackholeSpawn_Draw:
			ret


















