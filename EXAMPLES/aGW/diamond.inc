
struct DIAMOND
   px	   dd ?
   py	   dd ?
   vx	   dd ?
   vy	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   Dalpha  db ?
   Dbeta   db ?
   Dgamma  db ?
	   db ?

	   dd ?
	   dd ?
ends


struct DIAMONDSPAWN
   px	   dd ?
   py	   dd ?

   alpha   db ?
   beta    db ?
   gamma   db ?
	   db ?

   ct	   dd ?

ends



		      align  16
Diamond_Spawn:
			mov  esi,DiamondSpawnTable
			mov  eax,dword[FrameCount]
		       test  eax,0x0F
			jnz  .ret
			mov  eax,dword[DiamondSpawnCount]
			cmp  eax,DIAMOND_SPAWN_MAXCOUNT
			jae  .ret
			add  dword[DiamondSpawnCount],1
			shl  eax,4
			add  esi,eax
		       call  GetRand.word
			mov  dword[esi+DIAMONDSPAWN.alpha],eax
			xor  edx,edx
			mov  ecx,PLAY_WIDTH
			div  ecx
		       push  edx
		       call  GetRand.word
			xor  edx,edx
			mov  ecx,PLAY_HEIGHT
			div  ecx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+DIAMONDSPAWN.py],xmm0
			pop  edx
		   cvtsi2ss  xmm0,edx
		      movss  dword[esi+DIAMONDSPAWN.px],xmm0

			mov  dword[esi+DIAMONDSPAWN.ct],64
	    .ret:	ret

align 4
.a dd 0.00002
.b dd 3.0





		      align  16
DiamondSpawn_Update:

			mov  ebx,[DiamondSpawnCount]
			mov  esi,DiamondSpawnTable
			mov  edi,esi
			jmp  .2
	      .1:     movsd  xmm0,qword[esi+DIAMONDSPAWN.px]
			mov  eax,dword[esi+DIAMONDSPAWN.alpha]
			mov  edx,dword[esi+DIAMONDSPAWN.ct]
			sub  edx,1
			 js  .0
	      .4:     movsd  qword[edi+DIAMONDSPAWN.px],xmm0
			mov  dword[edi+DIAMONDSPAWN.alpha],eax
			mov  dword[edi+DIAMONDSPAWN.ct],edx
			add  edi,16
			jmp  .3
	      .0:	mov  ecx,dword[DiamondCount]
			 or  edx,-1
		      xorps  xmm1,xmm1
			cmp  ecx,DIAMOND_MAXCOUNT
			jae  .4
			add  dword[DiamondCount],1
			shl  ecx,5
			add  ecx,DiamondTable
		      movsd  qword[ecx+DIAMOND.px],xmm0
		      movsd  qword[ecx+DIAMOND.vx],xmm1
			mov  dword[ecx+DIAMOND.alpha],eax
		       call  GetRand.word
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+DIAMOND.Dalpha],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+DIAMOND.Dbeta],dl
			mov  edx,eax
			and  edx,0x07
			shr  eax,3
			sub  edx,3
			mov  byte[ecx+DIAMOND.Dgamma],dl
	      .3:	add  esi,16
	      .2:	sub  ebx,1
			jns  .1
			sub  edi,DiamondSpawnTable
			shr  edi,4
			mov  [DiamondSpawnCount],edi
			ret











		      align  16
Diamond_Update:

			xor  esi,esi
			mov  eax,DiamondTable
			jmp  .6
	     .5:	mov  edi,esi
			mov  ecx,esi
			shl  ecx,5
			add  ecx,DiamondTable
	     .7:
		      movsd  xmm0,qword[eax+DIAMOND.px]
		      movsd  xmm1,qword[ecx+DIAMOND.px]
		      subps  xmm0,xmm1
		     movaps  xmm2,xmm0
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		     movaps  xmm1,xmm0
		    cmpltps  xmm1,dqword[.r]
		      andps  xmm1,xmm2
		      maxps  xmm0,dqword[.e]
		    rsqrtps  xmm0,xmm0
		      mulps  xmm0,xmm1

		      movsd  xmm1,qword[eax+DIAMOND.vx]
		      movsd  xmm2,qword[ecx+DIAMOND.vx]
		      addps  xmm1,xmm0
		      subps  xmm2,xmm0
		      movsd  qword[eax+DIAMOND.vx],xmm1
		      movsd  qword[ecx+DIAMOND.vx],xmm2

			add  edi,1
			add  ecx,32
			cmp  edi,[DiamondCount]
			 jb  .7
			add  eax,32
	     .6:	add  esi,1
			cmp  esi,[DiamondCount]
			 jb  .5


		       push  DIAMOND_HR
			mov  esi,DiamondTable
			mov  edi,esi
			mov  ebx,[DiamondCount]
			jmp  .2
	    .1:       movsd  xmm4,qword[esi+DIAMOND.px]
		      movsd  xmm5,qword[esi+DIAMOND.vx]
		      xorps  xmm6,xmm6
		      movsd  xmm7,qword[Corner]
		      addps  xmm4,xmm5
		      maxps  xmm4,xmm6
		      minps  xmm4,xmm7
		       call  CheckShotIntersection
			 jc  .ex
		      movsd  xmm0,qword[Player.px]
		      subps  xmm0,xmm4
		     movaps  xmm2,xmm0
		      mulps  xmm2,dqword[.s]
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		      maxss  xmm0,dword[.e]
		    rsqrtss  xmm0,xmm0
		     shufps  xmm0,xmm0,0
		      mulps  xmm0,xmm2
		      mulps  xmm5,dqword[.a]
		      mulps  xmm0,dqword[.b]
		      addps  xmm5,xmm0
		       movd  xmm0,dword[esi+DIAMOND.alpha]
		       movd  xmm1,dword[esi+DIAMOND.Dalpha]
		      paddb  xmm0,xmm1
		      movsd  qword[edi+DIAMOND.px],xmm4
		      movsd  qword[edi+DIAMOND.vx],xmm5
		       movd  dword[edi+DIAMOND.alpha],xmm0
		       movd  dword[edi+DIAMOND.Dalpha],xmm1
			add  edi,32
			jmp  .0
	    .ex:
		       push  dword[DiamondParColor]
		       push  DiamondExpCount
		       push  DiamondExpTable
		       push  DIAMOND_EXP_MAXCOUNT
		       push  DIAMOND_EXP_MINS
		       push  DIAMOND_EXP_DELTAS
		       push  DIAMOND_EXP_COUNT
		       push  DIAMOND_PAR_MINR
		       push  DIAMOND_PAR_DELTAR
		       push  DIAMOND_PAR_MINS
		       push  DIAMOND_PAR_DELTAS
		       push  DIAMOND_PAR_COUNT
		       call  ReleaseExplosion

		       call  ReleaseGeom
		       fild  qword[Score]
		       fild  qword[Multiplier]
		       push  DIAMOND_POINTS
		       fild  dword[esp]
		      fmulp  st1,st0
		      faddp  st1,st0
		      fistp  qword[Score]
			add  esp,4

	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,DiamondTable
			shr  edi,5
			mov  [DiamondCount],edi
			add  esp,4*1


		       call  GetRand.word
			xor  ecx,ecx
		       test  eax,0x03F
		       setz  cl
			add  ecx,dword[DiamondParColor]
			mov  eax,DIAMOND_COLOR_AVOID
			sub  eax,ecx
			and  eax,63
			mov  edx,DIAMOND_COLOR_AVOID+DIAMOND_COLOR_AVOID_WIDTH
			cmp  eax,16
		      cmovb  ecx,edx
			cmp  eax,64-DIAMOND_COLOR_AVOID_WIDTH
		      cmova  ecx,edx
			and  ecx,63
			mov  dword[DiamondParColor],ecx

			ret


align 16
.a: dd 0.9375,0.9375,0.9375,0.9375
.b: dd 0.0625,0.0625,0.0625,0.0625
.e: dd 1.00,1.00,1.00,1.00
.s: dd DIAMOND_SPEED,DIAMOND_SPEED,DIAMOND_SPEED,DIAMOND_SPEED
.r: dd 800.0,800.0,800.0,800.0














		      align  16
Diamond_Draw:

		     invoke  glColor4fv,addr ColorTable2+16*DIAMOND_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[DiamondCount+CopyOffset]
			mov  esi,DiamondTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+DIAMOND.alpha]
		      movzx  edx,[esi+DIAMOND.beta]
		      movzx  ecx,[esi+DIAMOND.gamma]
		       call  SetRotationMatrix

			mov  ebp,12
			mov  edi,DiamondModel
	       .5:    movsd  xmm1,qword[esi+DIAMOND.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,32
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


		      align  16
DiamondModel:
		 dd +25.0,+00.0,+00.0,0   ,   +00.0,+25.0,+00.0,0
		 dd +00.0,+25.0,+00.0,0   ,   -25.0,+00.0,+00.0,0
		 dd -25.0,+00.0,+00.0,0   ,   +00.0,-25.0,+00.0,0
		 dd +00.0,-25.0,+00.0,0   ,   +25.0,+00.0,+00.0,0

		 dd +25.0,+00.0,+00.0,0   ,   +00.0,+00.0,+20.0,0
		 dd +00.0,+25.0,+00.0,0   ,   +00.0,+00.0,+20.0,0
		 dd -25.0,+00.0,+00.0,0   ,   +00.0,+00.0,+20.0,0
		 dd +00.0,-25.0,+00.0,0   ,   +00.0,+00.0,+20.0,0

		 dd +25.0,+00.0,+00.0,0   ,   +00.0,+00.0,-20.0,0
		 dd +00.0,+25.0,+00.0,0   ,   +00.0,+00.0,-20.0,0
		 dd -25.0,+00.0,+00.0,0   ,   +00.0,+00.0,-20.0,0
		 dd +00.0,-25.0,+00.0,0   ,   +00.0,+00.0,-20.0,0



		      align  16
DiamondSpawn_Draw:

		     invoke  glColor4fv,addr ColorTable2+16*DIAMOND_COLOR
		     invoke  glBegin,GL_QUADS
			mov  ebx,[DiamondSpawnCount+CopyOffset]
			mov  esi,DiamondSpawnTable+CopyOffset
			jmp  .4
	      .3:
		      movzx  eax,[esi+DIAMONDSPAWN.alpha]
		      movzx  edx,[esi+DIAMONDSPAWN.beta]
		      movzx  ecx,[esi+DIAMONDSPAWN.gamma]
		       call  SetRotationMatrix

			mov  eax,dword[esi+DIAMONDSPAWN.ct]
			and  eax,0x0F
		   cvtsi2ss  xmm0,eax
		      mulss  xmm0,dword[.a]
		      addss  xmm0,dword[.b]
		     shufps  xmm0,xmm0,0
		     movaps  xmm1,xmm0
		      mulps  xmm0,dqword[RotationMatrix.x]
		      mulps  xmm1,dqword[RotationMatrix.y]
		     movaps  dqword[RotationMatrix.x],xmm0
		     movaps  dqword[RotationMatrix.y],xmm1


			mov  ebp,12
			mov  edi,DiamondModel
	       .5:    movsd  xmm1,qword[esi+DIAMONDSPAWN.px]
		     movaps  xmm6,dqword[edi+16*0]
		     movaps  xmm0,dqword[edi+16*0]
		       dpps  xmm6,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm6,xmm0
		     movaps  xmm7,dqword[edi+16*1]
		     movaps  xmm0,dqword[edi+16*1]
		       dpps  xmm7,dqword[RotationMatrix.x],01110001b
		       dpps  xmm0,dqword[RotationMatrix.y],01110010b
		       orps  xmm7,xmm0
		      addps  xmm6,xmm1
		      addps  xmm7,xmm1
		       call  LineDrawNoEnd
			add  edi,32
			sub  ebp,1
			jnz  .5

			add  esi,16
	      .4:	sub  ebx,1
			jns  .3
		     invoke  glEnd
			ret


align 4

.a:	dd 0.125
.b	dd 1.0









		      align  16
Diamond_DestroyRadius:
			; xmm6 = center
			; xmm5 = radius
			mov  esi,DiamondTable
			mov  edi,esi
			mov  ebx,dword[DiamondCount]
			jmp  .2
	    .1:       movsd  xmm0,qword[esi+DIAMOND.px]
		      subps  xmm0,xmm6
		      mulps  xmm0,xmm0
		     haddps  xmm0,xmm0
		     comiss  xmm0,xmm5
		     movaps  xmm0,dqword[esi+16*0]
		     movaps  xmm1,dqword[esi+16*1]
			 jb  .ex
		     movaps  dqword[edi+16*0],xmm0
		     movaps  dqword[edi+16*1],xmm1
			add  edi,32
			jmp  .0
	    .ex:      movsd  xmm4,qword[esi+DIAMOND.px]
		      xorps  xmm7,xmm7
		       push  dword[DiamondParColor]
		       push  DiamondExpCount
		       push  DiamondExpTable
		       push  DIAMOND_EXP_MAXCOUNT
		       push  DIAMOND_EXP_MINS
		       push  DIAMOND_EXP_DELTAS
		       push  DIAMOND_EXP_COUNT/2
		       push  DIAMOND_PAR_MINR
		       push  DIAMOND_PAR_DELTAR
		       push  DIAMOND_PAR_MINS
		       push  DIAMOND_PAR_DELTAS
		       push  DIAMOND_PAR_COUNT/2
		       call  ReleaseExplosion
		       call  ReleaseGeom
	    .0: 	add  esi,32
	    .2: 	sub  ebx,1
			jns  .1
			sub  edi,DiamondTable
			shr  edi,5
			mov  [DiamondCount],edi
			ret




