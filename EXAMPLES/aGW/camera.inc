; camera modes:    activated by pressing corresponding number on keyboard
;
;
; 2 FREE:  camera position and angle is controled by keys TYFGHN and UIOJKL
;    mouse input:      screen clicks are projected onto grid
;                      left click shoots in that direction
;    keyboard input:   wasd: move player relative to camera direction
;                      arrows: shoot in a direction relative to camera direction
;    controller input: left stick: move player relative to camera direction
;                      shoot in a direction relative to camera direction
;
; 1 ABOVE: camera is above the grid and player and square to the grid (1+0I+0J+0K);  move closer to and farther from grid with keys TG
;  input is the same as in FREE; simpler because camera direction in ABOVE mode is always 1+0I+0J+0K
;
;
; 3 FIRSTPERSON: camera is essentially on top of the player with view direction perpendicular to z axis. keys TG move closer/father from player  and keys IK tilt the camera
;  mouse input:  move mouse left/right: swivel player(and camera) left/right
;                right click: shoot in direction that camera is pointing
;  keyboard input:  wasd: move player relative to camera direction
;                   up arrow: shoot in camera direction
;                   left/right arrow: rotate camera left/right
;  controller input: left stick: move player relative to camera direction
;                    right stick: rotate camera left/right
;                    right trigger: shoot in direction of camera
;

; the camera position is mantained as three floats (x,y,z)
; the camera angle mainted as flour floats (w,x,y,z) subject to w^2+x^2+y^2+z^2=1
;   after updating camera angle (w,x,y,z), it should be diveded by Sqrt[w^2+x^2+y^2+z^2] to maintain normality
;   in this way, the camera angle system is totaly free while at the same time perfectly stable and not subject to accumulated roundoff errors
;   the SO(3) matrix R(w,x,y,z) cooresponding to (w,x,y,z) is
;
;     w^2+x^2-y^2-z^2    2 (x y-w z)        2 (w y+x z)
;     2 (x y+w z)        w^2-x^2+y^2-z^2    2 (y z-w x)
;     2 (x z-w y)        2 (w x+y z)        w^2-x^2-y^2+z^2
;
;   the first  col is a unit vector pointing to the right of the camera
;   the second col is a unit vector pointing in the up direction of the the camera
;   the third  col is a unit vector pointing in the reverse direction of the camera look direction

; in first person mode, if the player view direction is (x,y) then the camera quat should be set to the normalization of
;       (Y + Sqrt[x^2 + y^2]) * (1 + I) - X * (J + K)




CameraDirToGridDirection:
		       ; in:  (esp+4*1, esp+4*2, esp+4*3) vector (x,y,z)
		       ; out: xmm0 = {x,y,0,0} unit vector    or zero if eax is zero on return
		       ;      eax = 0 if vector could not be determined, otherwise -1

; vector is multiplied by camera matrix then z coordinate is forgotten and then it is normalized

virtual at esp
.R11 dq ?
.R12 dq ?
.R13 dq ?
.R21 dq ?
.R22 dq ?
.R23 dq ?
.R31 dq ?
.R32 dq ?
.R33 dq ?
.retaddr dd ?
.x dd ?
.y dd ?
.z dd ?
end virtual
			sub  esp,8*9

			mov  eax,esp
			mov  ebx,CameraQuat
		       call  QuatToMatrixd

			fld  dword[.x]
			fld  dword[.y]
			fld  dword[.z]

			fld  qword[.R11]
		       fmul  st0,st3
			fld  qword[.R12]
		       fmul  st0,st3
			fld  qword[.R13]
		       fmul  st0,st3
		      faddp  st1,st0
		      faddp  st1,st0

			fld  qword[.R21]
		       fmul  st0,st4
			fld  qword[.R22]
		       fmul  st0,st4
			fld  qword[.R23]
		       fmul  st0,st4
		      faddp  st1,st0
		      faddp  st1,st0

			fld  st1
		       fmul  st0,st0
			fld  st1
		       fmul  st0,st0
		      faddp  st1,st0

			fld  [const_f4_1d2p10]
		     fcomip  st1
			 ja  @f 		    ; if vector is too short, just return (0,0)
		      fsqrt
		       fdiv  st1,st0
		      fdivp  st2,st0

		       fstp  dword[esp+4*1]
		       fstp  dword[esp+4*0]
		       fstp  st0
		       fstp  st0
		       fstp  st0
			 or  eax,-1
		      movsd  xmm0,qword[esp]
			add  esp,8*9
			ret  4*3


@@:		       fstp  st0
		       fstp  st0
		       fstp  st0
		       fstp  st0
		       fstp  st0
		       fstp  st0
			xor  eax,eax
		      xorps  xmm0,xmm0
			add  esp,8*9
			ret  4*3







MouseCoordToGridCoor:  ; in:  (esp+4*1, esp+4*2) integer mouse windowz screen coordinates (upper left is origin)
		       ; out: xmm0 = {x,y,0,0} mouse click projected onto grid coodinates


; set             mx = + (2 * [esp+4*1] - WindowWidth) / WindowWidth
; set             my = - (2 * [esp+4*2] - WindowHeight) / WindowHeight
; set     { fx, fy } = { CameraField.x, CameraField.y }
; set { px, py, pz } = CameraPosition
; set              R = R(CameraQuat)
; set              t = { fy * mx, fx * my, -fx * fy }
; set              v = R.t
; then the gird coodinates (x,y) of the click are
;    x = px - pz * v[1] / v[3]
;    y = py - pz * v[2] / v[3]

virtual at esp
.R11 dq ?
.R12 dq ?
.R13 dq ?
.R21 dq ?
.R22 dq ?
.R23 dq ?
.R31 dq ?
.R32 dq ?
.R33 dq ?
.retaddr dd ?
.msx dd ?
.msy dd ?
end virtual
			sub  esp,8*9

			mov  eax,esp
			mov  ebx,CameraQuat
		       call  QuatToMatrixd


			fld  dword[CameraField.y]
		       fild  dword[.msx]
		       fadd  st0,st0
			fld  dword[WindowWidth]
		       fsub  st1,st0
		      fdivp  st1,st0
		      fmulp  st1,st0

			fld  dword[CameraField.x]
		       fild  dword[.msy]
		       fadd  st0,st0
			fld  dword[WindowHeight]
		      fsubr  st1,st0
		      fdivp  st1,st0
		      fmulp  st1,st0

			fld  dword[CameraField.x]
			fld  dword[CameraField.y]
		      fmulp  st1,st0
		       fchs

			fld  qword[.R11]
		       fmul  st0,st3
			fld  qword[.R12]
		       fmul  st0,st3
			fld  qword[.R13]
		       fmul  st0,st3
		      faddp  st1,st0
		      faddp  st1,st0

			fld  qword[.R21]
		       fmul  st0,st4
			fld  qword[.R22]
		       fmul  st0,st4
			fld  qword[.R23]
		       fmul  st0,st4
		      faddp  st1,st0
		      faddp  st1,st0

			fld  qword[.R31]
		       fmul  st0,st5
			fld  qword[.R32]
		       fmul  st0,st5
			fld  qword[.R33]
		       fmul  st0,st5
		      faddp  st1,st0
		      faddp  st1,st0

			fld  dword[CameraPosition.z]
		     fdivrp  st1,st0
		       fmul  st1,st0
		      fmulp  st2,st0

			fld  dword[CameraPosition.y]
		     fsubrp  st1,st0

fist  dword[GridClick.y]

		       fstp  dword[esp+4*1]
			fld  dword[CameraPosition.x]
		     fsubrp  st1,st0

fist  dword[GridClick.x]

		       fstp  dword[esp+4*0]

		       fstp  st0
		       fstp  st0
		       fstp  st0

		      movsd  xmm0,qword[esp]

			add  esp,8*9
			ret  4*2






RotateCamera:

		; in:  esp + 4*1 : th ; half the angle of rotation
		;      esp + 4*2 : x  ; vector about which to rotate
		;      esp + 4*3 : y  ;
		;      esp + 4*4 : z  ;

       ; vector of rotation is relative to camera
       ; so rotating about 1,0,0 will tilt up and down
       ;    rotating about 0,1,0 will swivel side to side
       ;    rotating about 0,0,1 will rotate the camera up-side-down without changing the look direction

			sub  esp,4*4
			mov  eax,esp
		       push  dword[eax+4*8]
		       push  dword[eax+4*7]
		       push  dword[eax+4*6]
		       push  dword[eax+4*5]
		       call  QuatRotate
			mov  eax,CameraQuat
			mov  ebx,CameraQuat
			mov  ecx,esp
		       call  QuatMul
		       call  QuatNormalize
			add  esp,4*4
			ret  4*4



MoveCamera:
		; in:  esp + 4*1 : x  ; movement vector
		;      esp + 4*2 : y  ;
		;      esp + 4*3 : z  ;

       ;  vector is again relative to camera as in RotateCamera
       ;   0,0,-1 forward
       ;   1,0,0  to the right
       ;   0,1,0  up

			sub  esp,8*12

			mov  eax,esp
			mov  ebx,CameraQuat
		       call  QuatToMatrixd

			fld  qword[eax+8*0]
		       fmul  dword[esp+8*12+4*1]
			fld  qword[eax+8*1]
		       fmul  dword[esp+8*12+4*2]
			fld  qword[eax+8*2]
		       fmul  dword[esp+8*12+4*3]
		      faddp  st1,st0
		      faddp  st1,st0
		       fadd  dword[CameraPosition.x]
		       fstp  dword[CameraPosition.x]

			fld  qword[eax+8*3]
		       fmul  dword[esp+8*12+4*1]
			fld  qword[eax+8*4]
		       fmul  dword[esp+8*12+4*2]
			fld  qword[eax+8*5]
		       fmul  dword[esp+8*12+4*3]
		      faddp  st1,st0
		      faddp  st1,st0
		       fadd  dword[CameraPosition.y]
		       fstp  dword[CameraPosition.y]

			fld  qword[eax+8*6]
		       fmul  dword[esp+8*12+4*1]
			fld  qword[eax+8*7]
		       fmul  dword[esp+8*12+4*2]
			fld  qword[eax+8*8]
		       fmul  dword[esp+8*12+4*3]
		      faddp  st1,st0
		      faddp  st1,st0
		       fadd  dword[CameraPosition.z]
		       fstp  dword[CameraPosition.z]

			add  esp,8*12
			ret  4*3





stuff:
.d dd 350.0
.f  dd 500.0


		      align  16
SetCameraMatrices:
			cmp  dword[PausedQ],0
			jne  .free

			cmp  dword[CameraMode],CAMERA_MODE_FREE
			 je  .free
			cmp  dword[CameraMode],CAMERA_MODE_FIRST
			 je  .first

	.above: 	fld  dword[CameraModeAbove.d]
			mov  eax,dword[KeyboardState.Buttons]
			shr  eax,20
			and  eax,3
			fld  dword[Lookup+4*eax]
		      faddp  st1,st0
			fst  dword[CameraModeAbove.d]
		       fstp  dword[CameraPosition.z]
			fld  dword[Player.px]
			fld  dword[Corner.x]
		      fdivr  st0,st1
		       fadd  st0,st0
		       fld1
		      fsubp  st1,st0
		       fmul  dword[CameraModeAbove.d]
		       fmul  dword[CameraField.y]
		      fsubp  st1,st0
		       fstp  dword[CameraPosition.x]
			fld  dword[Player.py]
			fld  dword[Corner.y]
		      fdivr  st0,st1
		       fadd  st0,st0
		       fld1
		      fsubp  st1,st0
		       fmul  dword[CameraModeAbove.d]
		       fmul  dword[CameraField.x]
		      fsubp  st1,st0
		       fstp  dword[CameraPosition.y]
			mov  dword[CameraQuat.w],1.0
			mov  dword[CameraQuat.x],0.0
			mov  dword[CameraQuat.y],0.0
			mov  dword[CameraQuat.z],0.0
			mov  eax,CameraQuat
			mov  ebx,CameraQuat
		       call  QuatNormalize
			jmp  .set

	.first: 	fld  dword[CameraModeFirst.d]
			mov  eax,dword[KeyboardState.Buttons]
			shr  eax,20
			and  eax,3
			fld  dword[Lookup+4*eax]
		      faddp  st1,st0
		       fstp  dword[CameraModeFirst.d]
			fld  dword[CameraModeFirst.a]
			mov  eax,dword[KeyboardState.Buttons]
			shr  eax,24
			and  eax,3
			fld  dword[Lookup+4*eax]
		       push  PLAYER_CAMERA_FIRST_D
			fld  dword[esp]
			add  esp,4
		      fmulp  st1,st0
		      faddp  st1,st0
		       fstp  dword[CameraModeFirst.a]
			fld  dword[Player.px]
			fld  dword[Player.py]
			fld  dword[CameraModeFirst.d]
			fld  dword[CameraModeFirst.x]
		       fmul  st0,st1
		      fsubp  st3,st0
			fld  dword[CameraModeFirst.y]
		       fmul  st0,st1
		      fsubp  st2,st0
		       fstp  dword[CameraPosition.z]
		       fstp  dword[CameraPosition.y]
		       fstp  dword[CameraPosition.x]
			fld  dword[CameraModeFirst.x]
			fld  dword[CameraModeFirst.y]
			fld  st1
		       fmul  st0,st0
			fld  st1
		       fmul  st0,st0
		      faddp  st1,st0
		      fsqrt
		      faddp  st1,st0
			fst  dword[CameraQuat.w]
		       fstp  dword[CameraQuat.x]
		       fchs
			fst  dword[CameraQuat.y]
		       fstp  dword[CameraQuat.z]
		       push  0.0
		       push  0.0
		       push  1.0
		       push  dword[CameraModeFirst.a]
		       call  RotateCamera
			jmp  .set

	.free:		sub  esp,4*3
			mov  eax,dword[KeyboardState.Buttons]
			shr  eax,16
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
			shr  eax,2
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
			shr  eax,2
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
		       fstp  dword[esp+4*2]
		       fstp  dword[esp+4*1]
		       fstp  dword[esp+4*0]
		       call  MoveCamera
			sub  esp,4*4
			mov  eax,dword[KeyboardState.Buttons]
			shr  eax,24
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
			shr  eax,2
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
			shr  eax,2
			mov  ecx,eax
			and  ecx,3
			fld  dword[Lookup+4*ecx]
		       fstp  dword[esp+4*3]
		       fstp  dword[esp+4*2]
		       fstp  dword[esp+4*1]
			mov  dword[esp+4*0],0.005
		       call  RotateCamera

	.set:
		; set the projection matrix
			mov  ecx,ProjectionMatrix
		       call  SetProjectionMatrix
		     invoke  glMatrixMode, GL_PROJECTION
		     invoke  glLoadMatrixf,ProjectionMatrix

		; set the view matrix
			mov  ecx,ViewMatrix
		       call  SetViewMatrix
		     invoke  glMatrixMode,GL_MODELVIEW
		     invoke  glLoadMatrixf,ViewMatrix

			mov  eax,dword[CameraMode]
			mov  edx,dword[KeyboardState.Numbers]
			mov  ecx,CAMERA_MODE_ABOVE
		       test  edx,KEYBOARD_KEY_1
		     cmovnz  eax,ecx
			mov  ecx,CAMERA_MODE_FREE
		       test  edx,KEYBOARD_KEY_2
		     cmovnz  eax,ecx
			mov  ecx,CAMERA_MODE_FIRST
		       test  edx,KEYBOARD_KEY_3
		     cmovnz  eax,ecx
			mov  dword[CameraMode],eax
			ret





		      align  16
SetProjectionMatrix:	mov  dword[ecx+4*1],0.0
			mov  dword[ecx+4*2],0.0
			mov  dword[ecx+4*3],0.0
			mov  dword[ecx+4*4],0.0
			mov  dword[ecx+4*6],0.0
			mov  dword[ecx+4*7],0.0
			mov  dword[ecx+4*8],0.0
			mov  dword[ecx+4*9],0.0
			mov  dword[ecx+4*11],-1.0
			mov  dword[ecx+4*12],0.0
			mov  dword[ecx+4*13],0.0
			mov  dword[ecx+4*15],0.0
			fld  dword[CameraField.x]
		       fstp  dword[ecx+4*0]	   ; x
			fld  dword[CameraField.y]
		       fstp  dword[ecx+4*5]	   ; y
			fld  dword[CameraField.n]
			fld  dword[CameraField.f]
			fld  st1
		       fsub  st0,st1
			fld  st2
		       fadd  st0,st2
		       fdiv  st0,st1
		       fstp  dword[ecx+4*10]	; (n+f)/(n-f)
		       fxch  st2
		      fmulp  st1,st0
		       fadd  st0,st0
		     fdivrp  st1,st0
		       fstp  dword[ecx+4*14]	; (2*n*f)/(n-f)
			ret


		      align  16
SetViewMatrix:
;the matrix is set to Inverse[Camera4DMatrx] . Inverse[gltranslate3f matrix]


			sub  esp,8*12

			mov  eax,esp
			mov  ebx,CameraQuat
		       call  QuatToMatrixd

			fld  qword[eax+8*0]
			fst  dword[ecx+4*0]
		       fmul  dword[CameraPosition.x]
			fld  qword[eax+8*3]
			fst  dword[ecx+4*4]
		       fmul  dword[CameraPosition.y]
			fld  qword[eax+8*6]
			fst  dword[ecx+4*8]
		       fmul  dword[CameraPosition.z]
		      faddp  st1,st0
		      faddp  st1,st0
		       fchs
		       fstp  dword[ecx+4*12]

			fld  qword[eax+8*1]
			fst  dword[ecx+4*1]
		       fmul  dword[CameraPosition.x]
			fld  qword[eax+8*4]
			fst  dword[ecx+4*5]
		       fmul  dword[CameraPosition.y]
			fld  qword[eax+8*7]
			fst  dword[ecx+4*9]
		       fmul  dword[CameraPosition.z]
		      faddp  st1,st0
		      faddp  st1,st0
		       fchs
		       fstp  dword[ecx+4*13]

			fld  qword[eax+8*2]
			fst  dword[ecx+4*2]
		       fmul  dword[CameraPosition.x]
			fld  qword[eax+8*5]
			fst  dword[ecx+4*6]
		       fmul  dword[CameraPosition.y]
			fld  qword[eax+8*8]
			fst  dword[ecx+4*10]
		       fmul  dword[CameraPosition.z]
		      faddp  st1,st0
		      faddp  st1,st0
		       fchs
		       fstp  dword[ecx+4*14]

		       fldz
			fst  dword[ecx+4*3]
			fst  dword[ecx+4*7]
		       fstp  dword[ecx+4*11]
		       fld1
		       fstp  dword[ecx+4*15]

			add  esp,8*12
			ret


		      align  16
QuatNormalize:	       ; in:  ebx: address of quaternion   q
		       ; out: eax: address of q/|q|

			fld  dword[ebx+4*0]
			fld  dword[ebx+4*1]
			fld  dword[ebx+4*2]
			fld  dword[ebx+4*3]
			fld  st3
		       fmul  st0,st0
			fld  st3
		       fmul  st0,st0
		      faddp  st1,st0
			fld  st2
		       fmul  st0,st0
			fld  st2
		       fmul  st0,st0
		      faddp  st1,st0
		      faddp  st1,st0
		      fsqrt
		       fdiv  st1,st0
		       fdiv  st2,st0
		       fdiv  st3,st0
		      fdivp  st4,st0
		       fstp  dword[eax+4*3]
		       fstp  dword[eax+4*2]
		       fstp  dword[eax+4*1]
		       fstp  dword[eax+4*0]
			ret



		      align  16
QuatToMatrixd:	       ; in:  ebx: address of unit quaternion q
		       ; out: eax: address of 3x3 row major, double, orthogonal matrix A parameterized by q
; If the quaternion is w + x I + y J + z K, this matrix is
;{{   w^2+x^2-y^2-z^2, 2 (x y-w z)     ,  2 (w y+x z)       },
; {   2 (x y+w z)    , w^2-x^2+y^2-z^2 ,  2 (y z-w x)       },
; {   2 (x z-w y)    , 2 (w x+y z)     ,  w^2-x^2-y^2+z^2   }}

virtual at ebx
.w dd ?
.x dd ?
.y dd ?
.z dd ?
end virtual


		       fld  dword[.w]
		      fmul  st0,st0
		       fld  dword[.x]
		      fmul  st0,st0
		     faddp  st1,st0
		       fld  dword[.y]
		      fmul  st0,st0
		       fld  dword[.z]
		      fmul  st0,st0
		     faddp  st1,st0
		     fsubp  st1,st0
		      fstp  qword[eax+8*0]

		       fld  dword[.x]
		       fld  dword[.y]
		     fmulp  st1,st0
		       fld  dword[.w]
		       fld  dword[.z]
		     fmulp  st1,st0
		     fsubp  st1,st0
		      fadd  st0,st0
		      fstp  qword[eax+8*1]

		       fld  dword[.w]
		       fld  dword[.y]
		     fmulp  st1,st0
		       fld  dword[.x]
		       fld  dword[.z]
		     fmulp  st1,st0
		     faddp  st1,st0
		      fadd  st0,st0
		      fstp  qword[eax+8*2]

		       fld  dword[.x]
		       fld  dword[.y]
		     fmulp  st1,st0
		       fld  dword[.w]
		       fld  dword[.z]
		     fmulp  st1,st0
		     faddp  st1,st0
		      fadd  st0,st0
		      fstp  qword[eax+8*3]

		       fld  dword[.w]
		      fmul  st0,st0
		       fld  dword[.y]
		      fmul  st0,st0
		     faddp  st1,st0
		       fld  dword[.x]
		      fmul  st0,st0
		       fld  dword[.z]
		      fmul  st0,st0
		     faddp  st1,st0
		     fsubp  st1,st0
		      fstp  qword[eax+8*4]

		       fld  dword[.y]
		       fld  dword[.z]
		     fmulp  st1,st0
		       fld  dword[.w]
		       fld  dword[.x]
		     fmulp  st1,st0
		     fsubp  st1,st0
		      fadd  st0,st0
		      fstp  qword[eax+8*5]

		       fld  dword[.x]
		       fld  dword[.z]
		     fmulp  st1,st0
		       fld  dword[.w]
		       fld  dword[.y]
		     fmulp  st1,st0
		     fsubp  st1,st0
		      fadd  st0,st0
		      fstp  qword[eax+8*6]

		       fld  dword[.w]
		       fld  dword[.x]
		     fmulp  st1,st0
		       fld  dword[.y]
		       fld  dword[.z]
		     fmulp  st1,st0
		     faddp  st1,st0
		      fadd  st0,st0
		      fstp  qword[eax+8*7]

		       fld  dword[.w]
		      fmul  st0,st0
		       fld  dword[.z]
		      fmul  st0,st0
		     faddp  st1,st0
		       fld  dword[.x]
		      fmul  st0,st0
		       fld  dword[.y]
		      fmul  st0,st0
		     faddp  st1,st0
		     fsubp  st1,st0
		      fstp  qword[eax+8*8]

		       ret



		      align  16
QuatRotate:	; in:  esp + 4*1 : th
		;      esp + 4*2 : x  ; vector about which to rotate
		;      esp + 4*3 : y  ;
		;      esp + 4*4 : z  ;
		; out: eax:  address of q = Cos[th] + (x I + y J + z K)*Sin[th]/Sqrt[x*x+y*y+z*z]

			fld  dword[esp+4*2]
			fld  dword[esp+4*3]
			fld  dword[esp+4*4]
			fld  st2
		       fmul  st0,st0
			fld  st2
		       fmul  st0,st0
			fld  st2
		       fmul  st0,st0
		      faddp  st1,st0
		      faddp  st1,st0
			fld  dword[const_f4_1d2p10]
		     fcomip  st1
			 ja  @f 		    ; if vector is too short, just return 1
		      fsqrt
			fld  dword[esp+4*1]
		    fsincos
		       fstp  dword[eax+4*0]
		     fdivrp  st1,st0
		       fmul  st1,st0
		       fmul  st2,st0
		      fmulp  st3,st0
		       fstp  dword[eax+4*3]
		       fstp  dword[eax+4*2]
		       fstp  dword[eax+4*1]
			ret  4*4

@@:		       fstp  st0
		       fstp  st0
		       fstp  st0
		       fstp  st0
			mov  dword[eax+4*0],1.0
			mov  dword[eax+4*1],0.0
			mov  dword[eax+4*2],0.0
			mov  dword[eax+4*3],0.0
			ret  4*4



		      align  16
QuatMul:	       ; in:  ebx: address of q1
		       ;      ecx: address of q2
		       ; out: eax: address of q1*q2
virtual at ebx
.w1 dd ?
.x1 dd ?
.y1 dd ?
.z1 dd ?
end virtual

virtual at ecx
.w2 dd ?
.x2 dd ?
.y2 dd ?
.z2 dd ?
end virtual



			 fld  dword[.w1]
			 fld  dword[.w2]
		       fmulp  st1,st0
			 fld  dword[.x1]
			 fld  dword[.x2]
		       fmulp  st1,st0
		       fsubp  st1,st0
			 fld  dword[.y1]
			 fld  dword[.y2]
		       fmulp  st1,st0
		       fsubp  st1,st0
			 fld  dword[.z1]
			 fld  dword[.z2]
		       fmulp  st1,st0
		       fsubp  st1,st0

			 fld  dword[.w2]
			 fld  dword[.x1]
		       fmulp  st1,st0
			 fld  dword[.w1]
			 fld  dword[.x2]
		       fmulp  st1,st0
		       faddp  st1,st0
			 fld  dword[.y2]
			 fld  dword[.z1]
		       fmulp  st1,st0
		       fsubp  st1,st0
			 fld  dword[.y1]
			 fld  dword[.z2]
		       fmulp  st1,st0
		       faddp  st1,st0

			 fld  dword[.w2]
			 fld  dword[.y1]
		       fmulp  st1,st0
			 fld  dword[.w1]
			 fld  dword[.y2]
		       fmulp  st1,st0
		       faddp  st1,st0
			 fld  dword[.x2]
			 fld  dword[.z1]
		       fmulp  st1,st0
		       faddp  st1,st0
			 fld  dword[.x1]
			 fld  dword[.z2]
		       fmulp  st1,st0
		       fsubp  st1,st0

			 fld  dword[.x1]
			 fld  dword[.y2]
		       fmulp  st1,st0
			 fld  dword[.x2]
			 fld  dword[.y1]
		       fmulp  st1,st0
		       fsubp  st1,st0
			 fld  dword[.w2]
			 fld  dword[.z1]
		       fmulp  st1,st0
		       faddp  st1,st0
			 fld  dword[.w1]
			 fld  dword[.z2]
		       fmulp  st1,st0
		       faddp  st1,st0

			fstp  dword[eax+4*3]
			fstp  dword[eax+4*2]
			fstp  dword[eax+4*1]
			fstp  dword[eax+4*0]
			 ret






